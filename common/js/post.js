var SERVER_CONTEXT_PATH = "";
var CONTEXT_PATH = "";
function post(option) {
	if(option.url.indexOf("?") > -1) {
		option.url = option.url + "&";
	} else {
		option.url = option.url + "?";
	}
	option.url = option.url + "_=" + new Date().getTime();
	$.ajax({
		url: SERVER_CONTEXT_PATH + option.url,
		cache: false,
		method: option.method ? option.method : "post",
		contentType: 'application/json',
		data: JSON.stringify(option.data),
		dataType: 'json', // this is for return type
		beforeSend: function (request) {
			var csrfInfo = getCsrfInfo();
			if(csrfInfo.name) {
				request.setRequestHeader(csrfInfo.name, csrfInfo.token);
			}
        },
		success: function(result) {
			if(result.status == '1') {
				if(typeof option.success === 'function') {
                    option.success(result.data);
				}
			} else {
				if(typeof option.error === 'function'){
					option.error(result.error.msg);
				} else {
					console.log(result.error.msg);
				}
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			if(typeof option.requestError === 'function') {
				option.requestError(jqXHR, textStatus, errorThrown);
				return;
			}
			if(jqXHR.responseJSON && jqXHR.responseJSON.message) {
				console.log(jqXHR.responseJSON.message);
			} else {
                console.log(textStatus);
			}
		}
	})
}

function getCsrfInfo() { 
   var metas = document.getElementsByTagName('meta'); 
   
   var info = {};

   for (var i=0; i<metas.length; i++) { 
	   if (metas[i].getAttribute("name") == "_csrf") { 
		   info.token = metas[i].getAttribute("content"); 
	   } 
	   if (metas[i].getAttribute("name") == "_csrf_header") { 
		   info.name = metas[i].getAttribute("content"); 
	   } 
   } 

   return info;
}

function go(uri) {
	if(uri.indexOf('http') != 0 || uri.indexOf('https') != 0 ) {
		var idx = uri.indexOf(".html");
		if(idx > -1) {
			var markIdx = uri.indexOf("?");
			if(markIdx > -1 && idx < markIdx || markIdx == -1 && idx > -1) {
				window.location.href = CONTEXT_PATH + uri;
				return;
			} 
		} 
		window.location.href = SERVER_CONTEXT_PATH + uri;
	} else {
		window.location.href = uri;
	}
}

$(document).ready(function() {
	setTimeout(function() {
		$("body").css("visibility", "visible");
	}, 1000);
});
