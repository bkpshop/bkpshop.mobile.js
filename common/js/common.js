// 判断是否登录
var isLogin = false;

function goLogin() {
    post({
        url: "/isLogin",
        success: function (data) {
            isLogin = data;
            if (!data) {
                go("/login/index.html");
            }
        }
    });
}

//登录后跳转页面
loginGoBack();

function loginGoBack() {
    var _href = window.location.href;
    if (_href.indexOf("/login/index.html") == -1 && _href.indexOf("/register/index.html") == -1) {
        setCookie("loginBack", encodeURIComponent(_href));
    }
}

//跳转到购物车的页面
cartGoBack();
function cartGoBack(){
    var _href = window.location.href;
    var a = _href.indexOf("/mobileorder/order_confirm.html");
    var b = _href.indexOf("/cart/index.html");
    var c = _href.indexOf("/selectaddress.html");
    var d = _href.indexOf("/addaddressorder.html");
    if( a == -1 && b == -1 && c == -1&& d == -1){
        localStorage.setItem("cart_return",_href);
    }
}


shortcut();
function shortcut(){

    var _html = '<div class="tab" id="msg"><ul>';
    _html += '<li class="message"><a href="javascript:;">消息</a><i class="tab"></i></li>';
    _html += '<li class="cart"><a href="/customize/cart/index.html">购物车</a><i class="tab"></i></li>';
    _html += '<li class="index"><a href="/index.html">首页</a></li>';
    _html += '</ul></div>';
    $("body").append(_html);

    $("#reg").click(function(){
        $("#msg").removeClass("tab");
    })
    $("#msg").click(function(){
        $(this).addClass("tab");
    })
    //跳转消息
    $(".message").on("click", function () {
        post({
            url: "/isLogin",
            success: function(data){
                if (data) {
                    go("/message/index.html");
                } else {
                    go("/login/index.html");
                }
            },
            error:function(msg){
                console.log(msg)
            }
        })
    });
}

// 头像判断
function getImg(url) {
    var Regtest = /.*(jpg|jpeg|JPEG|JPG|PNG|png)$/;
    return Regtest.test(url);
    // console.log(Regtest.test(url))
}

function returnFloat(value) {
    var value = Math.round(parseFloat(value) * 100) / 100;
    var xsd = value.toString().split(".");
    if (xsd.length == 1) {
        value = value.toString() + ".00";
        return value;
    }
    if (xsd.length > 1) {
        if (xsd[1].length < 2) {
            value = value.toString() + "0";
        }
        return value;
    }
}

//流量统计代码
var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");
document.write(unescape("%3Cspan style='font-size:0.16rem;display: none;' id='cnzz_stat_icon_1261796657'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol + "s11.cnzz.com/z_stat.php%3Fid%3D1261796657' type='text/javascript'%3E%3C/script%3E"));

//修剪字符串前后空格
String.prototype.trimSpace = function () {
    return this.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
};

//设置cookie
function setCookie(c_name, value) {
    document.cookie = c_name + "=" + value + "; path=/";
}
//获取cookie
function getCookie(c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return document.cookie.substring(c_start, c_end);
        }
    }
    return ""
}

// 获取url参数
(function ($) {
    $.getUrlParam = function (name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if (r != null) return unescape(r[2]);
        return null;
    }
}(jQuery));

// 回到顶部
function showGoTop(topData) {
    if (topData < 300) {
        $('.goTop').addClass('tab');
    } else {
        $('.goTop').removeClass('tab');
    }
}
$('.goTop').click(function () {
    $('html,body').animate({
        scrollTop: '0px'
    }, 200);
});

if(localStorage.getItem('logIn') == 'success'){
    setTimeout(function() {
            post({
                url: '/member/phone',
                success: function(data){
                    if (/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)) {
                        window.webkit.messageHandlers.logInPhone.postMessage(data);
                    } else if (/(Android)/i.test(navigator.userAgent)) {
                        contact.logInPhone(data);
                    }
                    console.log(data);
                }
            })
            localStorage.setItem('logIn','')
    }, 100);
    
}


//分享判断
function invit(){
    post({
        url: "/isLogin",
        success: function (data) {
            // console.log(data)
            if(data){
                post({
                    url:'/member/uid',
                    success:function(data){
                        console.log(data);
                        inviter = data
                    }
                })
            }
        }
    });
}

post({
        url: "/isLogin",
        success: function (data) {
            if(data){
                post({
                    url:'/message/unRead/count',
                    success:function(data){
                        console.log(data)
                        if((data.memberMsgNum + data.systemMsgNum) > 0 ){
                            $('.alertDot').removeClass('tab');
                            $('.message>i').removeClass('tab');
                        }else{
                            $('.alertDot').addClass('tab');
                            $('.message>i').addClass('tab');
                        }
                    }
                });
                post({
                    url:'/customizeCart/list',
                    success:function(data){
                        if(data.length > 0 && data.length <100){
                            $('.cartNum').text(data.length).removeClass('tab');
                            // $('#msg .cart>i').text(data.length).removeClass('tab');
                            $('#msg .cart>i').removeClass('tab');
                        }else if(data.length >=100){
                            $('.cartNum').text('··').removeClass('tab');
                            $('#msg .cart>i').removeClass('tab');
                        }
                    }
                })
            }
        }
    });

$('#bottom').append(
    '<ul>'
        +'<li data-key="0" class="red_back">'
            +'<a>'
                +'<img src="/common/img/shangdian.png" alt="">'
                +'<span>首页</span>'
            +'</a>'
        +'</li>'
        +'<li data-key="1">'
            +'<a>'
                +'<img src="/common/img/dinzhi.png" alt="">'
                +'<span>定制</span>'
            +'</a>'
        +'</li>'
        +'<li data-key="2">'
            +'<a>'
                +'<img src="/common/img/shenghuo.png" alt="">'
                +'<span>生活</span>'
            +'</a>'
        +'</li>'
        +'<li data-key="3" class="">'
            +'<a class="tab_mind">'
                +'<img src="/common/img/gouwuche-01@2x.png" alt="">'
                +'<span>购物车</span>'
                +'<i class="cartNum tab"></i>'
            +'</a>'
        +'</li>'
        +'<li data-key="4">'
            +'<a class="tab_mind">'
                +'<img src="/common/img/wode.png" alt="">'
                +'<span>我的</span>'
                +'<i class="alertDot tab"></i>'
            +'</a>'
        +'</li>'
    +'</ul>'
);
var switchKey = (window.location.href).split('/').splice(3,2).toString();
console.log(switchKey)
switch (switchKey) {
    case 'index.html':
        $('#bottom li').eq(0).addClass('red_back').siblings().removeClass('red_back');
        $('#bottom li').eq(0).find('img').attr('src','/common/img/shangdian-01.png');
        break;
    case '':
        $('#bottom li').eq(0).addClass('red_back').siblings().removeClass('red_back');
        $('#bottom li').eq(0).find('img').attr('src','/common/img/shangdian-01.png');
        break;
    case 'customize,index.html':
        $('#bottom li').eq(1).addClass('red_back').siblings().removeClass('red_back');
        $('#bottom li').eq(1).find('img').attr('src','/common/img/dinzhi-01.png');
        break;
    case 'life,index.html':
        $('#bottom li').eq(2).addClass('red_back').siblings().removeClass('red_back');
        $('#bottom li').eq(2).find('img').attr('src','/common/img/shenghuo-01.png');
        break;
    case 'customize,cart':
        $('#bottom li').eq(3).addClass('red_back').siblings().removeClass('red_back');
        $('#bottom li').eq(3).find('img').attr('src','/common/img/gouwuche-02@2x.png');
        break;
    case 'memberindex.html':
        $('#bottom li').eq(4).addClass('red_back').siblings().removeClass('red_back');
        $('#bottom li').eq(4).find('img').attr('src','/common/img/wode-01.png');
        break;
    default:
        $('#bottom li').eq(1).addClass('red_back').siblings().removeClass('red_back');
        $('#bottom li').eq(1).find('img').attr('src','/common/img/dinzhi-01.png');
        break;
};
$('#bottom').delegate('li','click',function(){
    var switchKey = $(this).data('key')
    console.log(switchKey)
    switch (switchKey) {
        case 0:
            go('/index.html')
            break;
        case 1:
            go('/customize/index.html')
            break;
        case 2:
            go('/life/index.html')
            break;
        case 3:
            go('/customize/cart/index.html')
            break;
        case 4:
            go('/memberindex.html')
            break;
        default:
            break;
    }
})


/*
try{
    isApp = contact.ifApp();
}catch(e){
    browserReturns();//监听浏览器返回事件
}
*/

function browserReturns(){
    !function(pkg, undefined){
        var STATE = 'x-back';
        var element;

        var onPopState = function(event){
            event.state === STATE && fire();
            record(STATE);  //初始化事件时，push一下
        };

        var record = function(state){
            history.pushState(state, null, location.href);
        };

        var fire = function(){
            var event = document.createEvent('Events');
            event.initEvent(STATE, false, false);
            element.dispatchEvent(event);
        };

        var listen = function(listener){
            element.addEventListener(STATE, listener, false);
        };

        !function(){
            element = document.createElement('span');
            window.addEventListener('popstate', onPopState);
            this.listen = listen;
            record(STATE);
        }.call(window[pkg] = window[pkg] || {});

    }('XBack');

    /*XBack.listen(function(){
        //alert(document.referrer)
        if (document.referrer === '') {
            window.location.href = "/index.html";
        }
    });*/

    /*window.addEventListener("popstate", function(e) {
        //alert(document.referrer)
        if (document.referrer === '') {
            window.location.href = "/index.html";
        }else{
            //go(document.referrer)
        }
    }, false);*/

    if (window.history && window.history.pushState) {

        $(window).on('popstate', function () {
            //alert(document.referrer+'2')
            if (document.referrer === '') {
                window.location.href = "/index.html";
            }
        });
    }


}

