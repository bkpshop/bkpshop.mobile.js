$(function(){
    //登录验证
    goLogin();

    var _type = $.getUrlParam('type');
    //console.log(_type)
    //var newCustomize = $.getUrlParam('newCustomize');
    //var orderId = $.getUrlParam('orderId');
    //var imprest = $.getUrlParam("imprest");


    /*if(customized){  //定制订单详情
        $('.goBack').attr('href','/customized/customizedorder/orderdetail/orderdetail.html?orderId='+orderId);
    }else if(imprest){ //定制预付款页面
        $('.goBack').attr('href','/mobileorder/confirmCustom.html');
    }else if(newCustomize){
        $('.goBack').attr('href','/mobileorder/order_confirm.html');
    }else{ //普通商品确认下单页
        $('.goBack').attr('href','/mobileorder/index.html');
    }*/

    // 返回
    /*$('.goBack').click(function(){
        go("../../order/index.html");
    });*/

    // 获取数据
    post({
        url:'/member/address/list',
        success:function(data){
            console.log(data);
            if(!data.length){
                $('.d_nolist,.d_nolistFoot').removeClass('tab');
                $('.d_list,.d_footer').addClass('tab');
            }else{
                $('.d_list,.d_footer').removeClass('tab');
                $('.d_nolist,.d_nolistFoot').addClass('tab');
                addressListFn(data);
            }
        }
    });

    function addressListFn(data){
        for (var i = 0; i<data.length; i++){
            $('._addresslist').append(
                '<aside><div class="up" data-id="'+data[i]["id"]+'"><span>'+data[i]["consignee"]+'</span><span>'+data[i]["mobile"]+'</span>'
                +'<p>'+data[i]["addressDetail"]+'</p></div><div class="down">'
                +'<a href="/member/addressmanagement/addaddressorder.html?type='+_type+'&id='+data[i]["id"]+'">编辑</a><a name="'+data[i]["id"]+'" class="_delete">删除</a></div></aside>'
            )
        }

        var detele = null,_this=null;
        $('._delete').click(function () {
            $('.gat_background').removeClass('tab');
            $('.gat_alert').addClass('animated slideInDown');
            detele = $(this).attr("name");
            _this = $(this);
        });
        $('.gat_alert_cancel').click(function () {
            $('.gat_background').addClass('tab');
        });
        $('.gat_alert_affirm').click(function () {
            post({
                url: '/member/address/' + detele + '/delete',
                success: function () {
                    _this.parent().parent().remove();
                    $('.gat_background').addClass('tab');
                }
            });
        });

        $(".up").click(function(){
            var addressId = $(this).data("id");
            post({
                url:"/member/address/"+addressId+"/checkMemberAddress",
                success:function(data){
                    if(_type = "newCustomize"){ //新版定制
                        go('/mobileorder/order_confirm.html');
                    }else if(_type = "store"){
                        go("/mobileorder/index.html");
                    }else{
                        go('/mobileorder/order_confirm.html');
                    }
                }
            });
        });
    };


    $('.addaddress,.d_nolistFoot>span').click(function(){
        if(window.location.href.indexOf("?")>-1){
            go('./addaddressorder.html?type='+_type);
        }else{
            go('./addaddressorder.html');
        }
    });


    // FOOTER
    footerFn();
    function footerFn(){
        post({
            url:'/member/phone/blur',
            success:function(data){
                $('.foo_infor_name').text(data);
            }
        });
        $('.foo_infor_exit').click(function(){
            $.post({
                url: SERVER_CONTEXT_PATH+'/logout',
                success:function(){
                    go('/index.html');
                }
            });
        });
        $('.foo_infor_up').click(function(){
            $('html,body').animate({scrollTop: '0px'}, 200);
        })
        
    };
})