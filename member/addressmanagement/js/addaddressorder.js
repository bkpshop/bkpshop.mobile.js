$(function(){

    goLogin();

    var _url = window.location.href.substring(window.location.href.indexOf("?")+1);
    var _type = $.getUrlParam("type");

    $('.goBack').click(function(){
        window.history.go(-1);
    });

    var hashId = $.getUrlParam("id");
    console.log(hashId)
    if(hashId){
        //var hashId = hash.substring(1);
        post({
            url:'/member/address/'+hashId+'/findMemberAddressById',
            success: function(data){
                $('.id').val(data["id"]);
                $('aside:nth-child(1)>input').val(data["consignee"]);
                $('aside:nth-child(2)>input').val(data["mobile"]);
                $('.addressAId').val(data["provinceId"]);
                $('.addressBId').val(data["cityId"]);
                $('.addressCId').val(data["districtId"]);
                iniAddressFn(data["provinceId"],data["cityId"],data["districtId"]);
                $('textarea').val(data["address"]);
                $('.isDefaultId').val(data["isDefault"]);
                if(data["isDefault"]==1){
                    $('.chooseSelected>span').first().addClass('addressSelected');
                }else{
                    $('.chooseSelected>span').first().removeClass('addressSelected');
                };
            }
        });
    }

    $('aside:nth-child(2)>input').blur(function(){
        var number = parseInt($('aside:nth-child(2)>input').val());
        var regStr = /^1[34578]\d{9}$/;
        if (!regStr.test(number)) {
            $(".adrees_alert p").text("请填写正确的手机号！");
            $(".message_alert").removeClass("tab");
            //$('aside:nth-child(2)>input').val('');
        }
    });

    $('section>aside:nth-child(3)').click(function(){
        $('.choose_addressB').empty();
        $('.choose_addressC').empty();
        $('.choose_addressA').empty();
        $('.bg_choose').removeClass('tab');
        addressAFn();
    });

    function addressAFn(){
        post({
            url:'/region/provinces',
            success: function(data) {
                console.log(data);
                for(var i = 0; i<data.length; i++){
                    $('.choose_addressA').append(
                        '<li name="'+data[i]["id"]+'">'+data[i]["name"]+'</li>'
                    )
                };
                $('.choose_addressA>li').click(function(){
                    $('.choose_addressA>li').css('background','#fff');
                    $('.choose_addressB').empty();
                    $('.choose_addressC').empty();
                    $(this).css('background','#f0bf00');
                    var _indix = $(this).attr('name');
                    $('.addressA').text($(this).text());
                    $('.addressAId').val(_indix);
                    addressBFn(_indix);
                });
            }
        });
    };
    function addressBFn(_indix){
        post({
            url:'/region/' + _indix + '/children',
            success: function(data) {
                console.log(data);
                for(var i = 0; i<data.length; i++){
                    $('.choose_addressB').append(
                        '<li name="'+data[i]["id"]+'">'+data[i]["name"]+'</li>'
                    )
                };
                $('.choose_addressB>li').click(function(){
                    $('.choose_addressB>li').css('background','#fff');
                    $('.choose_addressC').empty();
                    $(this).css('background','#f0bf00');
                    var _indix = $(this).attr('name');
                    $('.addressB').text($(this).text());
                    $('.addressBId').val(_indix);
                    addressCFn(_indix);
                });
            }
        });
    }
    function addressCFn(_indix){
        post({
            url:'/region/' + _indix + '/children',
            success: function(data) {
                console.log(data);
                for(var i = 0; i<data.length; i++){
                    $('.choose_addressC').append(
                        '<li name="'+data[i]["id"]+'">'+data[i]["name"]+'</li>'
                    )
                };
                $('.choose_addressC>li').click(function(){
                    $('.choose_addressC>li').css('background','#fff');
                    $(this).css('background','#f0bf00');
                    $('.bg_choose').addClass('tab');
                    $('.addressCId').val($(this).attr('name'));
                    $('.addressC').text($(this).text());
                });
            }
        });
    }
    function iniAddressForFn(data,id,dom){
        for(var i=0; i<data.length; i++){
            if(data[i]["id"]==id){
                $(dom).text(data[i]["name"]);
            }
        }
    }
    function iniAddressFn(provinceId,cityId,districtId){
        post({
            url:'/region/provinces',
            success:function(data){
                iniAddressForFn(data,provinceId,'.addressA');
            }
        });
        post({
            url:'/region/' + provinceId + '/children',
            success:function(data){
                iniAddressForFn(data,cityId,'.addressB');
            }
        });
        post({
            url:'/region/' + cityId + '/children',
            success:function(data){
                iniAddressForFn(data,districtId,'.addressC');
            }
        })
    }

    $('.chooseSelected').click(function(){
        $('.chooseSelected>span').first().toggleClass('addressSelected');
        if($('.chooseSelected>span').hasClass('addressSelected')){
            $('.isDefaultId').val(1);
        }else{
            $('.isDefaultId').val(0);
        }
    });

    $(".message_alert_affirm").click(function(){
        $(".message_alert").addClass("tab");
    });

    $('.submitData').click(function () {
        var name = $('aside:nth-child(1)>input').val(),
            phone = $('aside:nth-child(2)>input').val(),
            provinceId = $('.addressAId').val(),
            cityId = $('.addressBId').val(),
            districtId = $('.addressCId').val(),
            address = $('textarea').val(),
            defaultId = $('.isDefaultId').val();

        if (name == '') {
            $(".adrees_alert p").text("请填写收件人姓名！");
            $(".message_alert").removeClass("tab");
            return false;
        };

        var regStr = /^1[34578]\d{9}$/;
        if (!regStr.test(phone)) {
            $(".adrees_alert p").text("请填写正确的手机号！");
            $(".message_alert").removeClass("tab");
            return false;
        };

        if (name != '' && phone != '' && provinceId != '' && cityId != '' && districtId != '' && address != '') {
            var _address = $('.addressA').text() + $('.addressB').text() + $('.addressC').text() + $('textarea').val();
            $.ajax({
                url: 'https://restapi.amap.com/v3/geocode/geo?address=' + _address + '&output=json&key=6bebc89f5b80d427c3de3e462a7533e9',
                success: function (data) {
                    // console.log(data["geocodes"][0]["location"]);
                    $.ajax({
                        url: 'https://restapi.amap.com/v3/geocode/regeo?location=' + data["geocodes"][0]["location"] + '&key=6bebc89f5b80d427c3de3e462a7533e9',
                        success: function (data) {
                            if(data["regeocode"]["addressComponent"]["district"] == "龙华区"){
                                var street = '龙华街道'
                            }else{
                                var street = data["regeocode"]["addressComponent"]["township"];
                            }
                            var data = {
                                id: $('.id').val(),
                                consignee: name,
                                provinceId: provinceId,
                                cityId: cityId,
                                districtId: districtId,
                                address: address,
                                mobile: phone,
                                isDefault: defaultId,
                                street: street
                            };

                            post({
                                url: '/member/address/save',
                                data: data,
                                success: function (data) {

                                    if(window.location.href.indexOf("?")>-1){
                                        go('./selectaddress.html?type='+ _type);
                                    }else{
                                        go('./selectaddress.html');
                                    }
                                },
                                error: function (data) {
                                    console.log(data);
                                }
                            })
                        }
                    });
                }
            });
        } else {
            $(".adrees_alert p").text("请将资料填写完整！");
            $(".message_alert").removeClass("tab");
            //alert('请将资料填写完整。');
        }
    });
});