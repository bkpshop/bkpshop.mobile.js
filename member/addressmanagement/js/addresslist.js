$(function () {
    //登录验证
    goLogin();

    // 返回
    $('.goBack').click(function () {
        go('/memberindex.html');
    })

    // 获取数据
    post({
        url: '/member/address/list',
        success: function (data) {
            console.log(data.length);
            if (!data.length) {
                $('.d_nolist,.d_nolistFoot').removeClass('tab');
                $('.d_list,.d_footer').addClass('tab');
            } else {
                $('.d_list,.d_footer').removeClass('tab');
                $('.d_nolist,.d_nolistFoot').addClass('tab');
                addressListFn(data);
            }
        }
    });

    function addressListFn(data) {
        for (var i = 0; i < data.length; i++) {
            $('._addresslist').append(
                '<aside><div class="up"><span>' + data[i]["consignee"] + '</span><span>' + data[i]["mobile"] + '</span>' +
                '<p>' + data[i]["addressDetail"] + '</p></div><div class="down">' +
                '<a href="/member/addressmanagement/addaddress.html#' + data[i]["id"] + '">编辑</a><a name="' + data[i]["id"] + '" class="_delete">删除</a></div></aside>'
            )
        };
        $('._delete').click(function () {
            $('.gat_background').removeClass('tab');
            $('.gat_alert').addClass('animated slideInDown');
            var detele = $(this).attr("name");
            $('.gat_alert_cancel').click(function () {
                $('.gat_background').addClass('tab');
            });
            $('.gat_alert_affirm').click(function () {
                post({
                    url: '/member/address/' + detele + '/delete',
                    success: function () {
                        go('./addresslist.html');
                    }
                });
            });
        });
    };
    $('.addaddress,.d_nolistFoot>span').click(function () {
        go('./addaddress.html')
    });


    // FOOTER
    footerFn();

    function footerFn() {
        post({
            url: '/member/phone/blur',
            success: function (data) {
                $('.foo_infor_name').text(data);
            }
        });
        $('.foo_infor_exit').click(function () {
            $.post({
                url: SERVER_CONTEXT_PATH+'/logout',
                success:function(){
                    go('/index.html');
                }
            });
        });
        $('.foo_infor_up').click(function () {
            $('html,body').animate({
                scrollTop: '0px'
            }, 200);
        });
    };
})