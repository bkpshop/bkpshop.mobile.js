var regPhone = /^1[34578]\d{9}$/;   //手机号码正则
var regMail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
var _id = '';
//修剪字符串前后空格
String.prototype.trimSpace = function () {
    return this.replace(/^\s\s*/,'').replace(/\s\s*$/,'');
};

$(function(){
    post({
        url : "/isLogin",
        success : function(data){
            if(!data){
                go("/login/index.html");
            }else{
                $(".name i").text(data);
            }
        }
    });
    getAlipay();
    footerFn();
    // 修改
    $(".change").on("click",function(){
        $(".binding,.account-input").removeClass("tab");
        $(".account").addClass("tab");
    });
    $(".binding").click(function(){
        var val = $(".account-input").val().trimSpace();
        if(!regPhone.test(val) && !regMail.test(val)){
            $(".error-tip").removeClass("tab");
            return false;
        }else{
            post({
                url:"/member/finance/account/alipay/save",
                data:{id:_id,account:val},
                success:function(data){
                    $(".error-tip").addClass("tab");
                    getAlipay();
                },
                error:function(msg){
                    $(".error-tip").removeClass("tab");
                }
            })
        }
    });
});

function getAlipay(){
    post({
        url:"/member/finance/account/alipay/info",
        success:function(data){
            //console.log(data)
            if(!!data.account){
                _id = data.id;
                $(".alipay .account").text(data.account);
                $(".binding,.account-input").addClass("tab");
                $(".account,.change,.change").removeClass("tab");
            }else{
                $(".binding,.account-input").removeClass("tab");
                $(".account,.change").addClass("tab");
            }
        }
    });
}

// FOOTER
function footerFn() {
    post({
        url: '/member/phone/blur',
        success: function (data) {
            $('.foo_infor_name').text(data);
        }
    });
    $('.foo_infor_exit').click(function () {
        $.post({
            url: SERVER_CONTEXT_PATH+'/logout',
            success:function(){
                go('/index.html');
            }
        });
    });
    $('.foo_infor_up').click(function () {
        $('html,body').animate({
            scrollTop: '0px'
        }, 200);
    });
}