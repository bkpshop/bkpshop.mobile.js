
var regPhone = /^1[34578]\d{9}$/;   //手机号码正则
var regBankCard = /^\d{16}|\d{19}$/;    //银行卡号正则
var bankId = '';
var bankPhone='';
var bankCard='';

$(function(){

    post({
        url : "/isLogin",
        success : function(data){
            if(!data){
                go("/login/index.html");
            }else{
                $(".name i").text(data);
            }
        }
    });

    //获取银行名称
    post({
        url:"/bank/list",
        success:function(data){
            var _option = "<option selected disabled >请选择开户银行</option>";
            for(var i=0;i<data.length;i++){
                _option += "<option data-id='"+data[i]["id"]+"'>"+data[i]["name"]+"</option>";
            }
            $(".bank select").html(_option);
        }
    });

    //选择银行
    $(".bank select").change(function(){

        bankId = $(".bank select option:selected").data("id");
    });

});

function tipMessage(message){
    $(".gat_background").removeClass("tab");
    $(".gat_alert p").text(message);
}

//点击绑定   提交事件
$("#banding").click(function(){

    if(bankId == ''){
        tipMessage("请选择开户银行！");
        return false;
    }

    bankCard = $(".bank_code input").val().trimSpace();
    if(!regBankCard.test(bankCard)){
        tipMessage("银行卡号格式不正确！");
        return false;
    }

    bankPhone = $(".tel input").val().trimSpace();
    if(!regPhone.test(bankPhone)){
        tipMessage("手机号码格式不正确！");
        return false;
    }

    var msgCode = $(".msg_code input").val().trimSpace();
    if(msgCode == ''){
        tipMessage("请输入短信验证码！");
        return false;
    }

    var _data = {
        id:"",
        bankId:bankId,
        bankCard:bankCard,
        phone:bankPhone,
        code:msgCode
    };

    post({
        url:"/member/finance/account/bank/save",
        data:_data,
        success:function(data){
            go("/member/myaccount/bank_bind/bank_bind.html");
        },
        error:function(msg){
            tipMessage(msg);
        }
    })
});



//点击发送验证码
var flag = true;    //防止连续点击发送验证码标记
$("#send").click(function(){
    
    bankPhone = $(".tel input").val().trimSpace();
    if(!regPhone.test(bankPhone)){
        tipMessage("手机号码格式不正确！");
        return false;
    }

    var code = $(".img_code input").val().trimSpace();
    if(code == ""){
        tipMessage("图片验证码不正确！");
        return false;
    }

    if(flag){
        post({
            url:"/captcha/"+code,
            success:function(data){
                console.log(data)
                if(data ==false){
                    tipMessage("图片验证码不正确！");

                }else{
                    var count = 59; //定时器，防止连续发送短信
                    if(flag) {
                        flag = false;
                        post({
                            url: "/member/finance/account/phone/" + bankPhone + "/verificationCode",
                            success: function (data) {
                                //console.log(data)
                            },
                            error:function(msg){
                                tipMessage(msg);
                                flag =true;
                            }
                        });

                        timer = setInterval(function () {
                            if (count <= 0) {
                                $("#send").val("获取短信验证码");
                                clearInterval(timer);
                                flag = true;
                                return;
                            }

                            $("#send").val(count + "s重新获取");
                            count--;
                        }, 1000);
                    }
                }
            }
        });
    }

});

$(".gat_background").click(function(){
    $(".gat_background").addClass("tab");
});



//获取图片验证码
createCode();
$(".img_code img").click(createCode);

function createCode(){
    $(".img_code img").attr("src",SERVER_CONTEXT_PATH + "/captcha.jpg");
}

