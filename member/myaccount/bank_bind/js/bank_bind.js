

$(function(){
    post({
        url : "/isLogin",
        success : function(data){
            if(!data){
                go("/login/index.html");
            }
        }
    });

    // 跳转添加银行卡页面
    $("header span").on("click",function(){
        go("./bank_add.html");
    });
    var id = null;
    var _this = null;
    $(".delete").on("click",function(){
        _this = $(".list input[type='radio']:checked").parent().parent();
        id = $(".list input[type='radio']:checked").val();
        if(id){
            $(".withdraw,.tips").removeClass("tab");
        }
    });
    // 确定解绑
    $(".btn .true").on("click",function(){
        post({
            url:"/member/finance/account/bank/"+id+"/delete",
            success:function(data){
                _this.remove();
                $(".withdraw,.tips").addClass("tab");
            }
        })
    });

    // 取消解绑
    $(".btn .exit").on("click",function(){
        $(".withdraw,.tips").addClass("tab");
    });

    //读取已绑定银行卡列表数据
    post({
        url:"/member/finance/account/bank/list",
        success:function(data){
            console.log(data)
            dealBankList(data);
            //$("input[name='radio_name'][checked]");
        }
    });

});

function dealBankList(data){
    var list = '';
    var num1 = '';
    var num2 = '';
    for(var i=0;i<data.length;i++){
        num1 = (data[i]["bankCard"]).substring(0,4);
        num2 = (data[i]["bankCard"]).substring(data[i]["bankCard"].length-6,data[i]["bankCard"].length);
        list += '<ul class="clear">';
        list += '<li class="userName single">'+data[i]["account"]+'</li>';
        list += '<li class="single">'+data[i]["bankName"]+'</li>';
        list += '<li class="bank_num">'+num1+'****'+num2+'</li>';
        list += '<li class="select">';
        list += '<input type="radio" value="'+data[i]["id"]+'" name="del"></li></ul>';
    }
    $(".list").html(list);
}



