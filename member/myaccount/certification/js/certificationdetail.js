$(function () {
    // 是否登陆
    goLogin();

    // 判断认证状态
    post({
        url: '/member/isAuth',
        success: function (data) {
            console.log(data["status"]);
            if(data["status"] == null){
                $(".alipay,.bank").addClass("tab");
                $(".status").append('<i class="right">未认证</i>');
                return;
            }
            if (data["status"]["name"] == 'AUDIT_SUCCESS') {
                $(".alipay,.bank").removeClass("tab");
                $(".status").append('<i class="right">已认证</i>');
            }else if(data["status"]["name"]=='AUDITING'){
                $(".alipay,.bank").addClass("tab");
                $(".status").append('<i class="right">认证中</i>');
            }else if(data["status"]["name"]=='AUDIT_FAILURE'){
                $(".alipay,.bank").addClass("tab");
                $(".status").append('<i class="right">认证失败</i>');
            }else{
                $(".alipay,.bank").addClass("tab");
                $(".status").append('<i class="right">未认证</i>');
            };
            // if(data.status == ''){}
        }
    });
    post({
        url:'/member/isRealAuth',
        success:function(data){
            console.log(data)
            if(data){
                $('.tishi').addClass('tab');

            }else{
                $('.tishi').removeClass('tab')
            }
        }
    })
});