$(function () {
    // 是否登陆
    goLogin();

    // 判断认证状态
    post({
        url: '/member/isAuth',
        success: function (data) {
            console.log(data["status"]);
            if(data["status"] == null){
                $('.d_uncertification,.d_certificationed,.d_certificationing').addClass('tab');
                $('.d_certification').removeClass('tab');
                certificationRuleFn();
                certificationFn();
                return;
            }else if (data["status"]["name"] == 'AUDIT_SUCCESS') {
                $('.d_certification,.d_certificationing,.d_uncertification').addClass('tab');
                $('.d_certificationed').removeClass('tab');
            }else if(data["status"]["name"]=='AUDITING'){
                $('.d_certification,.d_certificationed,.d_uncertification').addClass('tab');
                $('.d_certificationing').removeClass('tab');
            }else if(data["status"]["name"]=='AUDIT_FAILURE'){
                $('.d_certification,.d_certificationed,.d_certificationing').addClass('tab');
                $('.d_uncertification').removeClass('tab');
                console.log('shibai');
                $('.d_uncertification>aside>div').click(function(){
                    $('.d_uncertification,.d_certificationed,.d_certificationing').addClass('tab');
                    $('.d_certification').removeClass('tab');
                    certificationRuleFn();
                    certificationFn();
                });
            }else{
                $('.d_uncertification,.d_certificationed,.d_certificationing').addClass('tab');
                $('.d_certification').removeClass('tab');
                certificationRuleFn();
                certificationFn();
            };
        }
    });

    function certificationRuleFn() {
        $('.card_id>input').change(function () {
            var RegStr = /^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$/;
            if (!RegStr.test($('.card_id>input').val())) {
                $('.card_id>input').val('');
            };
        });
        $('.true_name>input').change(function () {
            var RegStr = /^(?!_)(?!.*?_$)[a-zA-Z0-9_\u4e00-\u9fa5]{2,4}$/;
            if (!RegStr.test($('.true_name>input').val())) {
                $('.true_name>input').val('');
            };
        });
        $('.front_card>input').change(function(){
            uploadPic(".front_card>input",'/upload/member/identity',".front_card>img:nth-child(4)",".hiddenA");
        });
        $('.reverse_card>input').change(function(){
            uploadPic(".reverse_card>input",'/upload/member/identity',".reverse_card>img:nth-child(4)",".hiddenB");
        })
    };

    //图片上传函数
    function uploadPic(dom, url, target,hidden) {
        var fd = new FormData();
        fd.append("file", 1);
        fd.append("file", $(dom).get(0).files[0]);
        var size = $(dom)[0].files[0].size;
        var type = $(dom)[0].files[0].type;
        console.log(size);
        console.log(type);
        if(size<=1024*1024){
            if(type == 'image/jpeg' || type == 'image/png'){
                $.ajax({
                    url: SERVER_CONTEXT_PATH + url,
                    type: 'POST',
                    data: fd,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        $(target).attr("src", SERVER_CONTEXT_PATH + "/load/picture/" + data.data);
                        $(hidden).val(data.data);
                    }
                });
            }else{
                alert('请上传图片格式。');
            };
        }else{
            alert('上传单个附件大小为1M。');
        };
    };


    function certificationFn() {
        $('.certificationSubmit').click(function () {
            var realName = $('.true_name>input').val(),
            identityCardNumber = $('.card_id>input').val(),
            identityCardFrontImage = $('.front_card>img:nth-child(4)').attr('src'),
            identityCardBackImage = $('.reverse_card>img:nth-child(4)').attr('src');

            if(realName != '' && identityCardNumber != '' && identityCardFrontImage.indexOf('/img/null.png') == -1 && identityCardBackImage.indexOf('/img/null.png') == -1 ){
                var data = {
                    realName:realName,
                    identityCardNumber:identityCardNumber,
                    identityCardFrontImage:$('.hiddenA').val(),
                    identityCardBackImage:$('.hiddenB').val()
                };

                post({
                    url:'/member/certification',
                    data: data,
                    success:function(){
                        alert('资料提交成功！');
                        window.location.reload();
                    }
                });
            }else{
                alert('请将信息填写完整。');
            }
        });
    };

})