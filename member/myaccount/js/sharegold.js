
$(function(){
// 是否登陆
    goLogin();
     
    var  pageNum = 1;
       
    var tradeDay = null;
    var tradeCondition = [
        {name:"null",text:"全部"},
        {name:3,text:"最近三天"},
        {name:7,text:"最近一周"},
        {name:30,text:"最近一月"},
        {name:180,text:"最近半年"},
        {name:365,text:"最近一年"}
    ];
    for(var j in tradeCondition){
        $('.times').append('<option value="'+tradeCondition[j].name+'">'+tradeCondition[j].text+'</option>')
    }
    $('.times').change(function(){
        if($(this).val() == "null"){
            tradeDay = null;
        }else{
            tradeDay = parseInt($(this).val());
        }
        $('.list').empty();
        pageNum = 1;
        listdata(tradeDay,pageNum);
    })
    listdata(tradeDay,pageNum)
    function listdata(tradeDay,pageNum){  
        console.log(tradeDay,pageNum)
        post({
            url:'/member/trade/share',
            data:{
                    tradeDay:tradeDay,
                    page:{pageNo:pageNum,pageSize:20}
                },
            success:function(data){
                console.log(data);
                // $('.list').html('');
               $('.all_gold>i').html('￥：'+returnFloat(data.totalPrice));
               var lists = data.pageResult.list;
               var html_l = ''
                for(var i in lists){
                    html_l += '<div class="list_li" style=""><span class="list_t left" style="">'+getLocalTime(lists[i].tradeTime)+'</span><span class="list_m right" style="">￥'+returnFloat(lists[i].amount)+'</span></div>'
                    // $('.list').append('<div class="list_li" style=""><span class="list_t left" style="">'+getLocalTime(lists[i].tradeTime)+'</span><span class="list_m right" style="">￥'+returnFloat(lists[i].amount)+'</span></div>')
                }
                $('.list').append(html_l)
            },
            error:function(msg){
                console.log(msg)
            }
        })
    }
iScroll();
// 上拉加载
// 上拉刷新函数
    function iScroll() {
        function iScrollA() {
            var scrollTop = 0,
                bodyScrollTop = 0,
                documentScrollTop = 0;
            if (document.body) {
                bodyScrollTop = document.body.scrollTop;
            }
            if (document.documentElement) {
                documentScrollTop = document.documentElement.scrollTop;
            }
            scrollTop = (bodyScrollTop - documentScrollTop > 0) ? bodyScrollTop : documentScrollTop;
            return scrollTop;
        }

        function iScrollB() {
            var windowHeight = 0;
            if (document.compatMode == "CSS1Compat") {
                windowHeight = document.documentElement.clientHeight;
            } else {
                windowHeight = document.body.clientHeight;
            }
            return windowHeight;
        }

        function iScrollC() {
            var scrollHeight = 0,
                bodyScrollHeight = 0,
                documentScrollHeight = 0;
            if (document.body) {
                bodyScrollHeight = document.body.scrollHeight;
            }
            if (document.documentElement) {
                documentScrollHeight = document.documentElement.scrollHeight;
            }
            scrollHeight = (bodyScrollHeight - documentScrollHeight > 0) ? bodyScrollHeight : documentScrollHeight;
            return scrollHeight;
        }
        window.onscroll = function () {
            showGoTop($('body').scrollTop())
            if (iScrollC() == iScrollB() + iScrollA()) {
                pageNum ++;
                listdata(tradeDay,pageNum);
                return pageNum;
            }
        }
    };
    // 时间轴处理
    function getLocalTime(nS) {     
        Date.prototype.toLocaleString = function() {
          return this.getFullYear() + "." + (this.getMonth() + 1) + "." + this.getDate();
        };
       return new Date(parseInt(nS)).toLocaleString();      
    }  
}) 