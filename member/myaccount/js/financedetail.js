
var action = null,
    tradeDay = null,
    type = null,
    pageNum = 1;
    var tradeCondition = [
        {name:"null",text:"全部"},
        {name:3,text:"最近三天"},
        {name:7,text:"最近一周"},
        {name:30,text:"最近一月"},
        {name:180,text:"最近半年"},
        {name:365,text:"最近一年"}
    ];
$(function(){
    goLogin();

    getCondition();
    listFunc();

    $('.all_select>input').click(function(){
        action = null;
        tradeDay = null;
        type = null;
        pageNum = 1;
        getCondition();
        listFunc();
        //window.location.reload();
    });


//查询条件
function getCondition(){
    post({
        url:'/member/trade/condition',
        success:function(data){

            var _t = '<option value="null">类型</option>';
            for(var i in data.tradeMode){
                _t += '<option value="'+data.tradeMode[i].name+'">'+data.tradeMode[i].text+'</option>';
            }
            //$('.type').append();

            var _a = '<option value="null">收支</option>';
            for(var j in data.action){
                _a += '<option value="'+data.action[j].name+'">'+data.action[j].text+'</option>';
                //$('.action').append()
            }

            var _time = '<option value="null">时间</option>';
            for(var k in tradeCondition){
                _time += '<option value="'+tradeCondition[k].name+'">'+tradeCondition[k].text+'</option>';
                //$('.time').append('<option value="'+tradeCondition[k].name+'">'+tradeCondition[k].text+'</option>')
            }

            $('.type').html(_t).change(function(){
                if($(this).val() == "null"){
                    type = null;
                }else{
                    type = $(this).val();
                }
                pageNum = 1;
                listFunc();
            });

            $('.action').html(_a).change(function(){
                if($(this).val() == "null"){
                    action = null;
                }else{
                    action = $(this).val();
                }
                pageNum = 1;
                listFunc();
            });

            $('.time').html(_time).change(function(){
                if($(this).val() == "null"){
                    tradeDay = null;
                }else{
                    tradeDay = parseInt($(this).val());
                }

                pageNum = 1;
                listFunc();
            })
        }
    });
}

//列表
function listFunc(){

    post({
        url:"/member/trade/list",
        data:{
            tradeMode:type,
            action:action,
            tradeDay:tradeDay,
            page:{
                pageNo:pageNum,
                pageSize:20
            }
        },
        success:function(data){
            console.log(data);
            $('.num').text(data.total);
            var _html = '';
            if(pageNum == 1){
                $('.record_list').empty();
            }

            for (var i in data.list) {

                _html += '<ul class="clear">';
                _html += '<li header="'+data.list[i].code+'">'+data.list[i].code+'</li>';
                if(data.list[i].mode){
                    _html += '<li>'+data.list[i].mode.text+'</li>';
                }else{
                    _html += '<li>'+""+'</li>';
                }

                if(data.list[i].action.name == "PAYMENT"){
                    _html += '<li>-'+returnFloat(data.list[i].amount)+'</li>';
                }else{
                    _html += '<li>'+returnFloat(data.list[i].amount)+'</li>';
                }
                _html += '<li>'+timeFn(data.list[i]["tradeTime"])+'</li></ul>';
            }
            $('.record_list').append(_html);
        }
    });
    /*post({
        url:'/member/finance/trade/list',
        data:list,
        success:function(data){
            $('.num').text(data.total);
            var _html = '';
            if(referType != null||action != null||tradeDay!=0||type!=null){
                $('.record_list').empty();
            }
            $('.record_list').empty();
            for (var i in data.list) {
                _html += '<ul class="clear">' 
                      + '<li header="'+data.list[i].code+'">'+data.list[i].code+'</li>'
                      + '<li>'+data.list[i].tradeReferType["text"]+'</li>'
                      + '<li>'+data.list[i].tradeType["text"]+'</li>';
                if(data.list[i].action == "RECEIPT"){
                    _html += '<li>'+returnFloat(data.list[i].amount)+'</li></ul>';
                }else{
                    _html += '<li>-'+returnFloat(data.list[i].amount)+'</li></ul>';
                }
            }
            $('.record_list').append(_html);
            //console.log(data)
        }
    })*/
}  
// 上拉加载
//文档高度 
function getDocumentTop() { 
    var scrollTop = 0, bodyScrollTop = 0, documentScrollTop = 0; 
    if (document.body) { 
        bodyScrollTop = document.body.scrollTop; 
    } 
    if (document.documentElement) { 
        documentScrollTop = document.documentElement.scrollTop; 
    } 
    scrollTop = (bodyScrollTop - documentScrollTop > 0) ? bodyScrollTop : documentScrollTop; 
    return scrollTop; 
} 

//可视窗口高度 
function getWindowHeight() { 
    var windowHeight = 0; 
    if (document.compatMode == "CSS1Compat") { 
        windowHeight = document.documentElement.clientHeight; 
    } else { 
        windowHeight = document.body.clientHeight; 
    } 
    return windowHeight; 
} 

//滚动条滚动高度 
function getScrollHeight() { 
    var scrollHeight = 0, bodyScrollHeight = 0, documentScrollHeight = 0; 
    if (document.body) { 
        bodyScrollHeight = document.body.scrollHeight; 
    } 
    if (document.documentElement) { 
        documentScrollHeight = document.documentElement.scrollHeight; 
    } 
    scrollHeight = (bodyScrollHeight - documentScrollHeight > 0) ? bodyScrollHeight : documentScrollHeight; 
    return scrollHeight; 
} 
window.onscroll = function () {  
    //监听事件内容 
    if(getScrollHeight() == getWindowHeight() + getDocumentTop()){ 
            showGoTop($('body').scrollTop())
        pageNum++;
        setTimeout(function() {
            listFunc();
        }, 1000);
        
    } 
}  
});

function timeFn(data) { //时间戳转时间
    var date = new Date(data);
    Y = date.getFullYear() + '-';
    M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
    D = date.getDate() + ' ';
    h = date.getHours() + ':';
    m = date.getMinutes() + ':';
    s = date.getSeconds();
    return (Y + M + D)
};