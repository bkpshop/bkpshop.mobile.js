
$(function(){
// 是否登陆
    goLogin();
     var status = null,
        pageNum = 1;
        day= 0;
    // FOOTER
    footerFn();
    listFunc(status,day,pageNum);
    $('.all_select>input').click(function(){
        location.reload()
    });
    function footerFn(){
        post({
            url:'/member/phone/blur',
            success:function(data){
                $('.foo_infor_name').text(data);
            }
        });
        $('.foo_infor_exit').click(function(){
        	$("#logout").attr("action", SERVER_CONTEXT_PATH + "/logout").submit();
        });
        $('.foo_infor_up').click(function(){
            $('html,body').animate({scrollTop: '0px'}, 200);
        });
    };

    // 条件
    post({
        url: '/member/finance/withdraw/conditions',
        success:function(data){
            console.log(data)
            for(var i in data.statuses){
                $('.state').append('<option value="'+data.statuses[i].status+'">'+data.statuses[i].text+'</option>')
            }
            for(var j in data.times){
                $('.times').append('<option value="'+data.times[j].day+'">'+data.times[j].text+'</option>')
            }
            $('.state').change(function(){
                $('.withdraw_to').empty();
                if($(this).val() == "null"){
                    status = null;
                }else{
                    status = $(this).val();
                }
                pageNum = 1;
                listFunc(status,day,pageNum)
            });
            $('.times').change(function(){
                $('.withdraw_to').empty();
                day = parseInt($(this).val());
                pageNum = 1;
                listFunc(status,day,pageNum)
            }) 
        }
    })
    //列表
      
function listFunc(status,day,pageNum){
    var financialList = {
        status: status,
        day: day,
        page: {
            "pageNo": pageNum,
            "pageSize": "10",
            "sorts": []
            }
    };console.log(financialList)
    post({
        url: '/member/finance/withdraw/list',
        data: financialList,
        success: function(data){
            console.log(data)
            $('.numb').text(data.total);
            var _html ='';
            // $('.withdraw_to').empty();
            var html_status = '';
            for (var i in data.list) {
                var srcimg = '';
                if(data.list[i].modeName == '支付宝'){
                    srcimg = "img/alipay.png"
                }else{
                    srcimg = "img/bank.png"
                };
                if(data.list[i].status == 'APPLYING'){
                    html_status = '申请中'
                }else if(data.list[i].status == 'SUCCESS'){
                    html_status = '提现成功'
                }else if(data.list[i].status == 'FAILURE'){
                    html_status = '提现失败'
                };
                _html += '<div class="to_title clear">' 
                       + '<p class="num left"><i>'+data.list[i].account+'</i><span>交易号：'+data.list[i].code+'</span></p>'
                       + '<p class="time right"><i>'+data.list[i].createTime+'</i><span>提现：￥'+data.list[i].amount+'</span></p></div>'
                       + '<div class="to_list"><ul class="clear"><li class="left"><img src='+srcimg+' alt=""></li>'
                       + '<li class="left"><span>提现-到'+data.list[i].modeName+'</span><i>'+data.list[i].createTime+'</i></li>'
                       + '<li class="right">￥'+data.list[i].amount+'<i>'+html_status+'</i></li></ul></div>'
                     
            }
            $('.withdraw_to').append(_html)
        }
    })
}
// 上拉加载
//文档高度 
function getDocumentTop() { 
    var scrollTop = 0, bodyScrollTop = 0, documentScrollTop = 0; 
    if (document.body) { 
        bodyScrollTop = document.body.scrollTop; 
    } 
    if (document.documentElement) { 
        documentScrollTop = document.documentElement.scrollTop; 
    } 
    scrollTop = (bodyScrollTop - documentScrollTop > 0) ? bodyScrollTop : documentScrollTop; 
    return scrollTop; 
} 

//可视窗口高度 
function getWindowHeight() { 
    var windowHeight = 0; 
    if (document.compatMode == "CSS1Compat") { 
        windowHeight = document.documentElement.clientHeight; 
    } else { 
        windowHeight = document.body.clientHeight; 
    } 
    return windowHeight; 
} 

//滚动条滚动高度 
function getScrollHeight() { 
    var scrollHeight = 0, bodyScrollHeight = 0, documentScrollHeight = 0; 
    if (document.body) { 
        bodyScrollHeight = document.body.scrollHeight; 
    } 
    if (document.documentElement) { 
        documentScrollHeight = document.documentElement.scrollHeight; 
    } 
    scrollHeight = (bodyScrollHeight - documentScrollHeight > 0) ? bodyScrollHeight : documentScrollHeight; 
    return scrollHeight; 
} 
window.onscroll = function () {  
    //监听事件内容 
    if(getScrollHeight() == getWindowHeight() + getDocumentTop()){ 
        //当滚动条到底时,这里是触发内容 
        //异步请求数据,局部刷新dom 
        pageNum++;
        //console.log(pageNum);
        setTimeout(function() {
            listFunc(status,day,pageNum);
        }, 1000);
        
    } 
}  
}) 