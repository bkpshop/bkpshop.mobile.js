$(function () {
    // audio
    // var ua = window.navigator.userAgent.toLowerCase();
    // if (ua.match(/MicroMessenger/i) == 'micromessenger') {
    //     function audioAutoPlay(id) {
    //         var audio = document.getElementById(id);
    //         audio.play();
    //         document.addEventListener("WeixinJSBridgeReady", function () {
    //             audio.play();
    //         }, false);
    //     }
    //     audioAutoPlay('_audio');
    // } else {
    //     var audio = document.getElementById("_audio");
    //     audio.play();
    // }

// 是否登陆
    goLogin();

    var accountId;
    //账户余额
    post({
        url: "/member/finance/balance",
        success: function (data) {
            // console.log(data)
            if (data) {
                $('.balance').html(data + '元')
            } else {
                $('.balance').html(0)
            }
        }
    })
    //最小余额
    var minAmountData = 10;
    post({
        url: '/member/finance/withdraw/minAmount',
        success: function (data) {
            //console.log(data)
            $('.min_money').html(data);
            // minAmountData = 10
        }
    })
    $('.cwgl_withdrawalAmount').blur(function () {
        console.log(minAmountData)
        regStr = /^[0-9]{0,}$/;
        if (!regStr.test($(this).val()) || isNaN($(this).val()) || $(this).val() < minAmountData || $(this).val() > parseFloat($('.balance').text())) {
            $(this).val('')
        };
    })
    //更多
    $("footer .exit").on("click", function () {
        // $(".acc_gray").addClass("tab");
        $("footer").addClass("tab");
    });

    // $("header span").on("click", function () {
        // $(".acc_gray").removeClass("tab");
    //     $("footer").removeClass("tab");
    // });
    // 提现
    $("#withdraw h3 span").on("click", function () {
        $(".acc_gray").addClass("tab");
        $("#withdraw").addClass("tab");
    });
    $(".gat_alert_cancel").on("click", function () {
        $(".acc_gray").addClass("tab");
        $(".gat_background").addClass("tab");
    });
    $(".gat_alert_affirm").on("click", function () {
        $(".acc_gray").addClass("tab");
        $(".gat_background").addClass("tab");
        go('/member/myaccount/certification/certification.html')
    });
    $(".my-account input[type='button']").on("click", function () {
        post({
            url:'/member/account/isBinding',
            success:function(data){
                if(data){
                    $('.cwgl_withdrawalAmount,.withdrawInputB,.withdrawInputA').val('');
                    $('.withdrawPhone').html('');
                    $('.z_zfb,.l_bank').removeClass('checked').addClass('normal');
                    $('.bank,.zfb_account').empty();
                    $(".acc_gray").removeClass("tab");
                    
                    $("#withdraw").removeClass("tab");
                }else{
                    // $(".acc_gray").removeClass('tab');
                    $(".gat_background").removeClass('tab')
                }

            }
        })
        
    });

    //银行和支付宝切换
    $('.acc_detail label').on('click', function () {
        $(this).addClass('checked').siblings('label').removeClass('checked');
        $(this).removeClass('normal').siblings('label').addClass('normal');
    });
    //选择银行
    var accountId;
    $('.acc_detail .l_bank').on('click', function (event) {
        event.stopPropagation();
        $('.bank').removeClass('tab');
        $('.zfb,.notbind_pay').addClass('tab');
        post({
            url: '/member/finance/account/bank/list',
            success: function (data) {
                console.log(data)
                if (data.length > 0) {
                    $('.bank').removeClass('tab');
                    $('.bank').empty()
                    for (var i = 0; i < data.length; i++) {
                        $('.bank').append('<li class="clear"><span name="' + data[i]["id"] + '" class="left bankChoose"></span><b class="left">' + data[i]["bankName"] + ' 尾号:' + data[i]["bankCard"].substring(data[i]["bankCard"].length - 4) + '</b></li>')
                    }
                } else {
                    $('.notbank').removeClass('tab');
                }
                $('.bankChoose').click(function () {
                    $('.bankChoose').removeClass('red_circle');
                    $(this).addClass('red_circle');
                    post({
                        url: '/member/finance/withdraw/' + $(this).attr('name') + '/phone',
                        success: function (data) {
                            console.log(data);
                            $('.withdrawPhone').text(data);
                        }
                    });
                    return accountId = $(this).attr('name');
                })
                return accountId = 'null';
            }
        })
    });
    //选择支付宝
    $('.acc_detail .z_zfb').on('click', function (event) {
        event.stopPropagation();
        $('.bank,.notbank').addClass('tab');
        $('.zfb').removeClass('tab');
        post({
            url: '/member/finance/account/alipay/info',
            success: function (data) {
                if (data["id"] != null) {
                    $('.zfb_account').removeClass('tab');
                    $('.zfb_account').text(data["account"]);
                    $('.zfb_account').attr('name', data["id"]);
                    post({
                        url: '/member/finance/withdraw/' + data["id"] + '/phone',
                        success: function (data) {
                            console.log(data);
                            $('.withdrawPhone').text(data);
                        }
                    });
                    return accountId = data["id"];
                } else {

                    $('.notbind_pay').removeClass('tab');
                }
            }
        })
    });
    //验证码
    $("#captchaImg").attr("src", "" + SERVER_CONTEXT_PATH + "/captcha.jpg?t=" + new Date().getTime());
    $("#captchaImg").click(function () {
        $("#captchaImg").attr("src", "" + SERVER_CONTEXT_PATH + "/captcha.jpg?t=" + new Date().getTime());
    });

    //验证码按钮的点击事件
    var flag = true;
    var authCode = '';
    $(".click").on("click", function (event) {
        event.preventDefault();

        if ($('.withdrawInputA').val() == '' || isNaN(accountId) || $('.cwgl_withdrawalAmount').val() == '') {
            return false;
        }

        post({
            url: '/captcha/' + $('.withdrawInputA').val(),

            success: function (data) {
                console.log(data)
                if (data == true) {
                    if (flag) {
                        flag = false;
                        var count = 50;
                        $(".click").val(count + "s重新获取");
                        var phoneData = {
                            accountId: accountId,
                            phone: $('.withdrawPhone').text()
                        };
                        console.log(phoneData)
                        //第三方接口
                        post({
                            url: "/member/finance/withdraw/phone/verificationCode",
                            data: phoneData,
                            success: function (data) {
                                console.log(data)
                            }
                        });
                        //定时器，防止连续发送短信
                        var timer = setInterval(function () {
                            count--;
                            if (count == 0) {
                                clearInterval(timer);
                                flag = true;
                                $(".click").val("点击获取验证码");
                                return;
                            }
                            $(".click").val(count + "s重新获取");
                        }, 1000);
                    }
                } else {
                    $('.withdrawInputA').val('');
                    $('.withdrawInputA').attr('placeholder', '验证码错误')

                }
            },
            error: function () {
                console.log(11)
            }
        })

    });


    // 提交

    $('.tijiao').click(function () {

        if ($('.cwgl_withdrawalAmount').val() != '' && !isNaN(accountId) && $('.withdrawInputB').val() != '') {
            var withdrawRequest = {
                amount: $('.cwgl_withdrawalAmount').val(),
                accountId: accountId,
                code: $('.withdrawInputB').val()
            };
            post({
                url: '/member/finance/doWithdraw',
                data: withdrawRequest,
                success: function (data) {
                    console.log(data);
                    $('#withdraw,.acc_gray').addClass('tab');
                    //账户余额
                    post({
                        url: "/member/finance/balance",
                        success: function (data) {
                            // console.log(data)
                            if (data) {
                                $('.balance').html(data + '元')
                            } else {
                                $('.balance').html(0)
                            }
                        }
                    });
                    $('.over').fadeIn(500);
                    $('.over').fadeOut(4000);
                },
                error: function (data) {
                    console.log(data)

                    $('.msg').text(data)
                }
            })
        } else {

            $('.msg').text('请填写完善后再提交!');
        }
    })

})