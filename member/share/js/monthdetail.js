
var pageNum = 1;
var searchDate = '2017-03';
$(function(){
    var date = new Date();
    var y = date.getFullYear();
    var m = date.getMonth()+1;
    m = m>10?m:("0"+m);
    searchDate = y+'-'+m;
    $(".now-time").text(y+"年"+m+"月");
    getData(true);
    iScroll();

    $(".month").change(function(){
        searchDate = $(this).val();
        y = searchDate.split("-")[0];
        m = searchDate.split("-")[1];
        $(".now-time").text(y+"年"+m+"月");
        $(".main ul").html("");
        pageNum=1;
        getData(true);
    })
});

function getData(flag){
    var page = {
        "pageNo": pageNum,
        "pageSize": "20",
        "sorts": []
    };
    post({
        url:"/member/share/details/month",
        data:{
            searchDate:searchDate,
            page:page
        },
        success:function(data){
            //console.log(data);
            if(flag){
                if(data.list.length>0){
                    dealData(data);
                    $(".main").css("display","block");
                    $(".list-none").css("display","none");
                }else{
                    $(".main").css("display","none");
                    $(".list-none").css("display","block");
                }
            }else{
                dealData(data);
                $(".main").css("display","block");
                $(".list-none").css("display","none");
            }

        }
    })
}


function dealData(data){
    var list = data.list;
    var _html = '';
    for(var i=0;i<list.length;i++){
        _html += '<li><div class="up"><span>'+list[i]["phone"]+'</span>';
        _html += '<span>'+list[i]["createTime"]+'</span></div>';
        _html += '<div class="down"><aside><p>消费了</p><p>'+(list[i]["tradeAmount"]).toFixed(2)+'</p>';
        _html += '</aside><aside><p>获得了分享金</p><p style="color: #ea540a;">+'+(list[i]["amount"]).toFixed(2)+'</p>';
        _html += '</aside><aside><img src="./img/cg.png" alt=""></aside></div></li>';
    }
    $(".main ul").append(_html);
}


// 上拉刷新函数
function iScroll() {
    function iScrollA() {
        var scrollTop = 0,
            bodyScrollTop = 0,
            documentScrollTop = 0;
        if (document.body) {
            bodyScrollTop = document.body.scrollTop;
        }
        if (document.documentElement) {
            documentScrollTop = document.documentElement.scrollTop;
        }
        scrollTop = (bodyScrollTop - documentScrollTop > 0) ? bodyScrollTop : documentScrollTop;
        return scrollTop;
    }

    function iScrollB() {
        var windowHeight = 0;
        if (document.compatMode == "CSS1Compat") {
            windowHeight = document.documentElement.clientHeight;
        } else {
            windowHeight = document.body.clientHeight;
        }
        return windowHeight;
    }

    function iScrollC() {
        var scrollHeight = 0,
            bodyScrollHeight = 0,
            documentScrollHeight = 0;
        if (document.body) {
            bodyScrollHeight = document.body.scrollHeight;
        }
        if (document.documentElement) {
            documentScrollHeight = document.documentElement.scrollHeight;
        }
        scrollHeight = (bodyScrollHeight - documentScrollHeight > 0) ? bodyScrollHeight : documentScrollHeight;
        return scrollHeight;
    }
    window.onscroll = function () {
        if (iScrollC() == iScrollB() + iScrollA()) {
            pageNum ++;
            getData(false);
        }
    }
}