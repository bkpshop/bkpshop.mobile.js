
var pageNum = 1;
$(function(){
    // 是否登陆
    goLogin();
    $('.d_header>span:nth-child(3)').click(function(){
        go('./monthdetail.html');
    });
    getData(true);
    iScroll();
});

function getData(flag){
    var page = {
        "pageNo": pageNum,
        "pageSize": "20",
        "sorts": []
    };
    post({
        url:'/member/share/list',
        data:page,
        success:function(data){
            if(flag){
                if(data.list.length>0){
                    dealData(data);
                    $(".main").css("display","block");
                    $(".list-none").css("display","none");
                }else{
                    $(".main").css("display","none");
                    $(".list-none").css("display","block");
                }
            }else{
                dealData(data);
                $(".main").css("display","block");
                $(".list-none").css("display","none");
            }
        }
    })
}

function dealData(data){
    var list = data.list;
    var _html = '';
    for(var i=0;i<list.length;i++){
        _html += '<li data-userid="'+list[i]["userId"]+'"><span>'+list[i]["phone"]+'</span><a href="javascript:;">明细&nbsp;></a></li>'
    }

    $(".main ul").append(_html);

    $(".main li").click(function(){
        var _id = $(this).data("userid");
        go("userdetail.html#"+_id);
    });
}


// 上拉刷新函数
function iScroll() {
    function iScrollA() {
        var scrollTop = 0,
            bodyScrollTop = 0,
            documentScrollTop = 0;
        if (document.body) {
            bodyScrollTop = document.body.scrollTop;
        }
        if (document.documentElement) {
            documentScrollTop = document.documentElement.scrollTop;
        }
        scrollTop = (bodyScrollTop - documentScrollTop > 0) ? bodyScrollTop : documentScrollTop;
        return scrollTop;
    }

    function iScrollB() {
        var windowHeight = 0;
        if (document.compatMode == "CSS1Compat") {
            windowHeight = document.documentElement.clientHeight;
        } else {
            windowHeight = document.body.clientHeight;
        }
        return windowHeight;
    }

    function iScrollC() {
        var scrollHeight = 0,
            bodyScrollHeight = 0,
            documentScrollHeight = 0;
        if (document.body) {
            bodyScrollHeight = document.body.scrollHeight;
        }
        if (document.documentElement) {
            documentScrollHeight = document.documentElement.scrollHeight;
        }
        scrollHeight = (bodyScrollHeight - documentScrollHeight > 0) ? bodyScrollHeight : documentScrollHeight;
        return scrollHeight;
    }
    window.onscroll = function () {
        if (iScrollC() == iScrollB() + iScrollA()) {
            pageNum ++;
            getData(false);
        }
    }
}

