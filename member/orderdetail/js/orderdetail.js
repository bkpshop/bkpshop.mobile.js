$(function () {
    // 是否登陆
    goLogin();

    footerFn();

    // goback
    $('.d_header>a').click(function () {
        go(localStorage.getItem('orderUrl'));
    });

    // 获取数据
    var hash = window.location.hash;
    if (hash) {
        var hashId = hash.substring(1);
        post({
            url: '/member/order/' + hashId + '/info',
            success: function (data) {
                console.log(data);
                orderFn(data); // 订单函数
                var dataList = data["items"];
                if (data["status"]["status"] == "WAIT_DELIVERY") {
                    WAIT_DELIVERY(dataList);
                } else if (data["status"]["status"] == "CLOSED") {
                    CLOSED(dataList);
                } else if (data["status"]["status"] == "WAIT_PAYING") {
                    WAIT_PAYING(dataList);
                } else {
                    post({
                        url: '/member/order/' + data["code"] + '/canRefund',
                        success: function (argument) {
                            console.log(argument)
                            // 判断是否过7天
                            // if (data == true) {
                            //     refundable(dataList);
                            // } else {
                            //     unRefundable(dataList);
                            // };
                            ifRefund(dataList,argument);

                            btnGo();
                        }
                    });
                    
                };
                btnGo();
                logistics();
            }
        });
    } else {
        go('/member/myorders/allorder.html');
    };

    // 待收货
    function WAIT_DELIVERY(data) {
        for (var i = 0; i < data.length; i++) {
            $('aside').append(
                '<div class="aside_center"><p>' + data[i]["orderStatusDTO"]["text"] + '</p><a href="/productdetail/index.html#' + data[i]["productId"] + '"><img class="aside_center_pic" src="' + SERVER_CONTEXT_PATH + '/load/picture/' + data[i]["productUrl"] + '" alt=""></a>' +
                '<div class="aside_center_text"><p>' + data[i]["productName"] + '</p><p>' + data[i]["productRemark"] + '</p><div>' +
                '<p class="aside_center_text_price">￥<span>' + returnFloat(data[i]["price"]) + '</span></p><p class="aside_center_text_num">×<span>' + data[i]["num"] + '</span></p></div>' +
                '<span class="order_' + data[i]["id"] + '"></span></div></div>' +
                '<div class="aside_operation"><div name="' + data[i]["orderStatusDTO"]["status"] + '" nameId="' + data[i]["id"] + '" class="oper_return">退款/退货</div><div name="' + data[i]["id"] + '" class="oper_logistics">查看物流</div></div>'
            );
            for (var k = 0; k < data[i]["specifications"].length; k++) {
                $('.order_' + data[i]["id"] + '').append('' + data[i]["specifications"][k]["specValue"] + '&nbsp;&nbsp;&nbsp;&nbsp')
            };
        };
    };
    // 订单关闭
    function CLOSED(data) {
        for (var i = 0; i < data.length; i++) {
            $('aside').append(
                '<div class="aside_center"><p>' + data[i]["orderStatusDTO"]["text"] + '</p><a href="/productdetail/index.html#' + data[i]["productId"] + '"><img class="aside_center_pic" src="' + SERVER_CONTEXT_PATH + '/load/picture/' + data[i]["productUrl"] + '" alt=""></a>' +
                '<div class="aside_center_text"><p>' + data[i]["productName"] + '</p><p>' + data[i]["productRemark"] + '</p><div>' +
                '<p class="aside_center_text_price">￥<span>' + returnFloat(data[i]["price"]) + '</span></p><p class="aside_center_text_num">×<span>' + data[i]["num"] + '</span></p></div>' +
                '<span class="order_' + data[i]["id"] + '"></span></div></div>'
            );
            for (var k = 0; k < data[i]["specifications"].length; k++) {
                $('.order_' + data[i]["id"] + '').append('' + data[i]["specifications"][k]["specValue"] + '&nbsp;&nbsp;&nbsp;&nbsp')
            };
        };
    };
    // 待付款
    function WAIT_PAYING(data) {
        for (var i = 0; i < data.length; i++) {
            $('aside').append(
                '<div class="aside_center"><p>' + data[i]["orderStatusDTO"]["text"] + '</p><a href="/productdetail/index.html#' + data[i]["productId"] + '"><img class="aside_center_pic" src="' + SERVER_CONTEXT_PATH + '/load/picture/' + data[i]["productUrl"] + '" alt=""></a>' +
                '<div class="aside_center_text"><p>' + data[i]["productName"] + '</p><p>' + data[i]["productRemark"] + '</p><div>' +
                '<p class="aside_center_text_price">￥<span>' + returnFloat(data[i]["price"]) + '</span></p><p class="aside_center_text_num">×<span>' + data[i]["num"] + '</span></p></div>' +
                '<span class="order_' + data[i]["id"] + '"></span></div></div>'
            );
            for (var k = 0; k < data[i]["specifications"].length; k++) {
                $('.order_' + data[i]["id"] + '').append('' + data[i]["specifications"][k]["specValue"] + '&nbsp;&nbsp;&nbsp;&nbsp')
            };
        };
    };
    // 七天之内可退款
    function refundable(data) {
        for (var i = 0; i < data.length; i++) {
            $('aside').append(
                '<div class="aside_center"><p>' + data[i]["orderStatusDTO"]["text"] + '</p><a href="/productdetail/index.html#' + data[i]["productId"] + '"><img class="aside_center_pic" src="' + SERVER_CONTEXT_PATH + '/load/picture/' + data[i]["productUrl"] + '" alt=""></a>' +
                '<div class="aside_center_text"><p>' + data[i]["productName"] + '</p><p>' + data[i]["productRemark"] + '</p><div>' +
                '<p class="aside_center_text_price">￥<span>' + returnFloat(data[i]["price"]) + '</span></p><p class="aside_center_text_num">×<span>' + data[i]["num"] + '</span></p></div>' +
                '<span class="order_' + data[i]["id"] + '"></span></div></div>' +
                '<div class="aside_operation"><div name="' + data[i]["orderStatusDTO"]["status"] + '"  nameId="' + data[i]["id"] + '" class="oper_return">退款/退货</div></div>'
            );
            for (var k = 0; k < data[i]["specifications"].length; k++) {
                $('.order_' + data[i]["id"] + '').append('' + data[i]["specifications"][k]["specValue"] + '&nbsp;&nbsp;&nbsp;&nbsp')
            };
        };
    };
    // 七天之外不可退款
    function unRefundable(data) {
        for (var i = 0; i < data.length; i++) {
            $('aside').append(
                '<div class="aside_center"><p>' + data[i]["orderStatusDTO"]["text"] + '</p><a href="/productdetail/index.html#' + data[i]["productId"] + '"><img class="aside_center_pic" src="' + SERVER_CONTEXT_PATH + '/load/picture/' + data[i]["productUrl"] + '" alt=""></a>' +
                '<div class="aside_center_text"><p>' + data[i]["productName"] + '</p><p>' + data[i]["productRemark"] + '</p><div>' +
                '<p class="aside_center_text_price">￥<span>' + returnFloat(data[i]["price"]) + '</span></p><p class="aside_center_text_num">×<span>' + data[i]["num"] + '</span></p></div>' +
                '<span class="order_' + data[i]["id"] + '"></span></div></div>'
            );
            for (var k = 0; k < data[i]["specifications"].length; k++) {
                $('.order_' + data[i]["id"] + '').append('' + data[i]["specifications"][k]["specValue"] + '&nbsp;&nbsp;&nbsp;&nbsp')
            };
        };
    };
    //交易成功
    function ifRefund(data,argument) {
        var html_ref;
        // var html_comment='';
        var ids = [];
        for (var i = 0; i < data.length; i++) {
            //是否能退款
            if(argument){
                html_ref = '<div class="aside_operation"><div name="' + data[i]["orderStatusDTO"]["status"] + '"  nameId="' + data[i]["id"] + '" class="oper_return">退款/退货</div></div>'
            }else{
                html_ref = ''
            };
            ids.push(data[i].id)

            $('aside').append(
                '<div class="aside_center"><p>' + data[i]["orderStatusDTO"]["text"] + '</p><a href="/productdetail/index.html#' + data[i]["productId"] + '"><img class="aside_center_pic" src="' + SERVER_CONTEXT_PATH + '/load/picture/' + data[i]["productUrl"] + '" alt=""></a>' +
                '<div class="aside_center_text"><p>' + data[i]["productName"] + '</p><p>' + data[i]["productRemark"] + '</p><div>' +
                '<p class="aside_center_text_price">￥<span>' + returnFloat(data[i]["price"]) + '</span></p><p class="aside_center_text_num">×<span>' + data[i]["num"] + '</span></p></div>' +
                '<span class="order_' + data[i]["id"] + '"></span></div></div>' + html_ref
            );
            for (var k = 0; k < data[i]["specifications"].length; k++) {
                $('.order_' + data[i]["id"] + '').append('' + data[i]["specifications"][k]["specValue"] + '&nbsp;&nbsp;&nbsp;&nbsp')
            };
        };

    //评价入口
        $('.aside_operation').each(function(index){
            var _this = $(this);
            post({
                url:'/product/'+ids[index]+'/findReviews',
                success:function(obj){
                    console.log(obj)
                    var flag = obj.isAdditional;console.log(flag);
                    if(flag == null){
                        
                        _this.append('<div data-id="'+ids[index]+'" class="go_comment fir_com">评论</div>');
                        
                    }else if(flag == true){
                       
                        _this.append('<div data-id="'+obj.id+'" class="go_comment sec_com">追评</div>');
                        
                    }
                }
            })

        })
       
         comment();
    }
    function comment(){
        $('.aside_operation').delegate('.fir_com','click',function(){
            
            go('/usercomment/publish.html#'+$(this).data('id'))
        });
        $('.aside_operation').delegate('.sec_com','click',function(){
            
            go('/usercomment/addition.html#'+$(this).data('id'))
        });
    }
    // 订单函数
    function orderFn(data) {
        // 地址
        $('.ord_info_info>span:nth-child(1)').text(data["address"]["consignee"]);
        $('.ord_info_info>span:nth-child(2)').text(data["address"]["mobile"]);
        $('.ord_info_address>span:nth-child(1)').text(data["address"]["address"]);
        // $('.ord_info_address>span:nth-child(2)').text(data["address"]["address"]);
        // 订单
        $('.ord_state_code').text(data["code"]);
        $('.ord_state_state').text(data["status"]["text"]);
        $('.ord_state_time').text(data["createTime"]);
        // 费用
        $('.feiA').text(returnFloat(data["productAmount"]));
        $('.feiB').text(returnFloat(data["shippingFee"]));
        $('.feiC').text(returnFloat(data["orderAmount"]));
    };

    // 退款退货跳转
    function btnGo() {
        $('.oper_return').on('click', function () {
            console.log($(this).attr('name'))
            // go('/member/myorders/refund/chooserefund.html#' + $(this).attr('nameId'))
            if ($(this).attr('name') == 'REFUND') {
                go('/member/myorders/refund/refunding.html#' + $(this).attr('nameId'));
            } else if ($(this).attr('name') == 'CLOSED') {
                alert('您的退款已到账，请勿重复提交。');
            } else {
                go('/member/myorders/refund/chooserefund.html#' + $(this).attr('nameId'));
            }
        })
    };
    // FOOTER函数
    function footerFn() {
        post({
            url: '/member/phone/blur',
            success: function (data) {
                $('.foo_infor_name').text(data);
            }
        });
        $('.foo_infor_exit').click(function () {
            $.post({
                url: SERVER_CONTEXT_PATH + '/logout',
                success: function () {

                    go('/index.html');
                }
            });
        });
        $('.foo_infor_up').click(function () {
            $('html,body').animate({
                scrollTop: '0px'
            }, 200);
        });
    };
    //确认收货按钮（弹框）
    function logistics() {
        $('.oper_logistics').click(function () {
            console.log($(this).attr('name'));
            post({
                url: '/member/order/' + $(this).attr('name') + '/logistical',
                success: function (data) {
                    // console.log(data);
                    if (data["shippingMode"] == null) {
                        $('.gat_alert>p').empty();
                        $('.gat_alert>p').text('您的订购的商品暂未发货，请耐心等待。');
                    } else if (data["shippingMode"]["mode"] == "DELIVER") {
                        $('.gat_alert>p').empty();
                        $('.gat_alert>p').append(
                            '<span>配送方式：' + data["shippingMode"]["text"] + '</span>' +
                            '<span>送货员：' + data["shippingCourier"] + '</span>' +
                            '<span>联系方式：' + data["shippingPhone"] + '</span>'
                        );
                    } else if (data["shippingMode"]["mode"] == "EXPRESS") {
                        $('.gat_alert>p').empty();
                        $('.gat_alert>p').append(
                            '<span>配送方式：' + data["shippingMode"]["text"] + '</span>' +
                            '<span>快递公司：' + data["shippingName"] + '</span>' +
                            '<span>快递单号：' + data["shippingCode"] + '</span>'
                        );
                    };
                }
            });
            // $('.gat_background').removeClass('tab');
            $('.gat_background').removeClass('tab');
            $('.gat_alert').addClass('animated slideInDown');
        });
        $('.gat_alert_affirm').click(function () {
            $('.gat_background').addClass('tab');
        });
    };
})