$(function(){
    // 获取哈希值；
    var hash = window.location.hash,
    _hash = hash.substring(1);
    
    // 获取数据1
    post({
        url:'/order/'+_hash+'/findRefundInfo',
        success:function(data){
            if(data["type"][0]["text"] == "REFUND"){
                go('/member/myorders/refund/onlyrefund.html#'+_hash);
            }else{
                for(var i = 0; i<data["type"].length; i++){
                    $('.ref_reason>select').append(
                        '<option value="'+data["type"][i]["value"]+'#'+data["type"][i]["text"]+'">'+data["type"][i]["value"]+'</option>'
                    )
                }
            };
        }
    });

    // 获取数据2
    post({
        url:'/order/' + _hash + '/findOrderProduct',
        success:function(data){
            console.log(data);
            $('.proPic').attr('src', SERVER_CONTEXT_PATH + '/load/picture/' + data["productUrl"]);
            $('.productName').text(data["productName"]);
            $('.productSpecification').text(data["productSpecification"]);
            $('.price').text('￥'+returnFloat(data["price"]));
            $('.num').text('x'+data["num"]);
            $('.supplyName').text(data["supplyName"]);
            $('.shippingFee').text('￥'+returnFloat(data["shippingFee"]));
            $('.orderAmount').text('￥'+returnFloat(data["orderAmount"]));
            $('.productCode').text(data["productCode"]);
        }
    })


    $('.ref_reason select').on("change",function(){
        var value = $(this).val();
        console.log(value.split('#')[1]);
        $('.ref_reason span.right').text(value.split('#')[0]);
        if(value.split('#')[1] == 'ONLY_REFUND'){
            go('/member/myorders/refund/onlyrefundb.html#'+value.split('#')[1]+'?'+_hash);
        }else if(value.split('#')[1] == 'REFUND_RETURNS'){
            go('/member/myorders/refund/onlyrefundc.html#'+value.split('#')[1]+'?'+_hash);
        }
        
        
    });


    // FOOTER
    footerFn();

    function footerFn() {
        post({
            url: '/member/phone/blur',
            success: function (data) {
                $('.foo_infor_name').text(data);
            }
        });
        $('.foo_infor_exit').click(function () {
            $.post({
                url: SERVER_CONTEXT_PATH+'/logout',
                success:function(){
                    
                    go('/index.html');
                }
            });
        });
        $('.foo_infor_up').click(function () {
            $('html,body').animate({
                scrollTop: '0px'
            }, 200);
        });
    };

});