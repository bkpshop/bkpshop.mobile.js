$(function () {
    // 是否登陆
    goLogin();

    // 获取哈希值；
    var hash = window.location.hash,
        _hash = hash.split('?')[1];

    // 回退
    $('.goBack').click(function () {
        go('/member/orderdetail/orderdetail.html#' + $('.onlyRef_productCode').text());
    });

    // 获取数据1
    post({
        url: '/order/' + _hash + '/findOrderProduct',
        success: function (data) {
            console.log(data);
            $('.onlyRef_img').attr('src', SERVER_CONTEXT_PATH + '/load/picture/' + data["productUrl"]);
            $('.onlyRef_name').text(data["productName"]);
            $('.onlyRef_classify').text(data["productSpecification"]);
            $('.onlyRef_fei').text('￥' + returnFloat(data["price"]));
            $('.onlyRef_num').text('x' + data["num"]);
            $('.refundNum').val(data["num"]);
            $('.onlyRef_supplyName').text(data["supplyName"]);
            $('.onlyRef_shippingFee').text('￥' + returnFloat(data["shippingFee"]));
            $('.onlyRef_orderAmount').text('￥' + returnFloat(data["orderAmount"]));
            $('.onlyRef_productCode').text(data["productCode"]);
        }
    });

    refundNumFn();
    // 退款数量与金额
    function refundNumFn(){
        $('.ref_num>input').blur(function(){
            if(parseFloat($(this).val()) > parseFloat($('.refundAmount').val()) || parseFloat($(this).val())<=0 ){
                $(this).val('');
            };
        });
        $('.ref_number>input').blur(function(){
            if(parseInt($(this).val()) > parseInt($('.refundNum').val()) || parseInt($(this).val())<=0 ){
                $(this).val('');
            };
        });
    };

    // 获取数据2
    post({
        url: '/order/' + _hash + '/findRefundInfo',
        success: function (data) {
            console.log(data);
            // $('.ref_num>input').val(returnFloat(data["refundAmount"]));
            $('.refundAmount').val(data["refundAmount"]);
            for (var i = 0; data["refundReason"].length > i; i++) {
                $('.ref_reason>select').append(
                    '<option value="' + data["refundReason"][i]["title"] + '#' + data["refundReason"][i]["id"] + '">' + data["refundReason"][i]["title"] + '</option>'
                )
            };
            $('.ref_reason>select').change(function () {
                var value = $(this).val().split('#');
                $('.ref_reason span.right').text(value[0]);
                $('.ref_reason span.right').attr('name', value[1]);
            });
        }
    });

    // 凭证上传
    $('.ref_uploadPic').change(function(){
        uploadDoc('.ref_uploadPic', '/upload/picture', '.ref_img>div');
    });

    // 提交
    $('.ref_submit').click(function () {
        if($('.ref_reason span.right').text() == '请选择' || $('.ref_num>input').val() =='' || $('.ref_number>input').val() == ''){
            alert('请将信息填写完整。')
            return;
        };
        var data = {
            id: _hash,
            type: hash.split('?')[0].substring(1),
            refundAmount: $('.ref_num>input').val(),
            num: $('.ref_number>input').val(),
            refundReason: $('.ref_reason span.right').text(),
            refundExplain: $('.ref_exp>textarea').val(),
            voucher: attachments.toString()
        };
        console.log(data);
        post({
            url: '/order/saveRefund',
            data: data,
            success: function (data) {
                alert('您的申请已经提交，请耐心等待。');
                go('/member/orderdetail/orderdetail.html#' + $('.onlyRef_productCode').text());
            }
        });
    });

    // 文件上传功能函数
    var attachments = []

    function uploadDoc(dom1, url, dom2) {
        console.log(attachments);
        if(attachments.length ==3){
            alert('上传图片已达上限。');
            return;
        };
        var fd = new FormData();
        fd.append("file", 1);
        for (var i = 0; i < $(dom1).get(0).files.length; i++) {
            var type = $(dom1).get(0).files[i].type;
            if (type == 'image/jpeg' || type == 'image/png') {
                fd.append("file", $(dom1).get(0).files[i]);
            } else {
                alert('格式不支持');
            }
        };
        $.ajax({
            url: SERVER_CONTEXT_PATH + url,
            type: 'POST',
            data: fd,
            processData: false,
            contentType: false,
            success: function (data) {
                console.log(data);
                $(dom2).append('<img src="'+SERVER_CONTEXT_PATH+'/load/picture/'+data+'" alt="">');
                var attachmentData =data;
                    attachments.push(attachmentData);
                return attachments
            }
        })
    }

    // FOOTER
    footerFn();

    function footerFn() {
        post({
            url: '/member/phone/blur',
            success: function (data) {
                $('.foo_infor_name').text(data);
            }
        });
        $('.foo_infor_exit').click(function () {
            $.post({
                url: SERVER_CONTEXT_PATH+'/logout',
                success:function(){
                    
                    go('/index.html');
                }
            });
        });
        $('.foo_infor_up').click(function () {
            $('html,body').animate({
                scrollTop: '0px'
            }, 200);
        });
    };

})