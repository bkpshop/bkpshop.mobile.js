$(function () {
    // 是否登陆
    goLogin();

    // 获取哈希值；
    var hash = window.location.hash,
        _hash = hash.substring(1);

    // 回退
    $('.goBack').click(function(){
       go('/member/orderdetail/orderdetail.html#'+$('.onlyRef_productCode').text());
    });

    // 获取数据1
    post({
        url: '/order/' + _hash + '/findOrderProduct',
        success: function (data) {
            console.log(data);
            $('.onlyRef_img').attr('src', SERVER_CONTEXT_PATH + '/load/picture/' + data["productUrl"]);
            $('.onlyRef_name').text(data["productName"]);
            $('.onlyRef_classify').text(data["productSpecification"]);
            $('.onlyRef_fei').text('￥' + returnFloat(data["price"]));
            $('.onlyRef_num').text('x' + data["num"]);
            $('.onlyRef_supplyName').text(data["supplyName"]);
            $('.onlyRef_shippingFee').text('￥' + returnFloat(data["shippingFee"]));
            $('.onlyRef_orderAmount').text('￥' + returnFloat(data["orderAmount"]));
            $('.onlyRef_productCode').text(data["productCode"]);
        }
    });

    // 获取数据2
    post({
        url: '/order/' + _hash + '/findRefundInfo',
        success: function (data) {
            console.log(data);
            $('.ref_num>input').val(returnFloat(data["refundAmount"]));  
            for (var i = 0; data["refundReason"].length > i; i++) {
                $('.ref_reason>select').append(
                    '<option value="' + data["refundReason"][i]["title"] + '#' + data["refundReason"][i]["id"] + '">' + data["refundReason"][i]["title"] + '</option>'
                )
            };
            $('.ref_reason>select').change(function () {
                var value = $(this).val().split('#');
                $('.ref_reason span.right').text(value[0]);
                $('.ref_reason span.right').attr('name', value[1]);
            });
        }
    });

    // 提交
    $('.ref_submit').click(function(){
        if($('.ref_reason span.right').text() == '请选择'){
            alert('请将信息填写完整。');
            return;
        };
        var data = {
            id: _hash,
            refundAmount: $('.ref_num>input').val(),
            refundReason: $('.ref_reason span.right').text(),
            refundExplain: $('.ref_exp>textarea').val()
        };
        post({
            url:'/order/saveRefund',
            data:data,
            success:function(data){
                alert('您的申请已经提交，请耐心等待。');
                go('/member/orderdetail/orderdetail.html#'+$('.onlyRef_productCode').text());
            }
        });
    })

    // FOOTER
    footerFn();

    function footerFn() {
        post({
            url: '/member/phone/blur',
            success: function (data) {
                $('.foo_infor_name').text(data);
            }
        });
        $('.foo_infor_exit').click(function () {
            $.post({
                url: SERVER_CONTEXT_PATH+'/logout',
                success:function(){
                    
                    go('/index.html');
                }
            });
        });
        $('.foo_infor_up').click(function () {
            $('html,body').animate({
                scrollTop: '0px'
            }, 200);
        });
    };

})