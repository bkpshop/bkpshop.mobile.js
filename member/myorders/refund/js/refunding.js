$(function(){
    // 是否登陆
    goLogin();

    // 获取哈希值；
    var hash = window.location.hash,
        _hash = hash.substring(1);

    // 回退
    $('.goBack').click(function(){
       go('/member/orderdetail/orderdetail.html#'+$('.productCode').text());
    });

    // 获取信息1
    post({
        url:'/order/'+_hash+'/findMobileRefundInfo',
        success:function(data){
            // console.log(data);
            if(data["type"] == '仅退款'){
                console.log(1111)
                $('.ref_img').removeClass('tab');
                var picList = data["voucher"].split(',');
                if(data["voucher"] == ''){
                    $('.ref_img>ul').append(
                        '<li>无</li>'
                    );
                }else{
                    for(var i = 0; i<picList.length; i++){
                        $('.ref_img>ul').append(
                            '<li><img src="'+SERVER_CONTEXT_PATH+'/load/picture/'+picList[i]+'" alt=""></li>'
                        )
                    };
                };
            };
            if(data["type"] == '退款退货'){
                console.log(222)
                $('.ref_img').removeClass('tab');
                var picList = data["voucher"].split(',');
                $('.ref_number').removeClass('tab');
                $('.ref_number>span>i').text(data["num"]);
                if(data["voucher"] == ''){
                    $('.ref_img>ul').append(
                        '<li>无</li>'
                    )
                }else{
                    for(var i = 0; i<picList.length; i++){
                        $('.ref_img>ul').append(
                            '<li><img src="'+SERVER_CONTEXT_PATH+'/load/picture/'+picList[i]+'" alt=""></li>'
                        )
                    };
                };
            };
            $('.type').text(data["type"]);
            $('.refundReason').text(data["refundReason"]);
            $('.refundAmount').text(returnFloat(data["refundAmount"]));
            $('.refundExplain').text(data["refundExplain"]);
            $('.refund_').text(data["createTime"]);
            // 判断完成时间是可否为空
            if(data["merchantAuditTime"] != null){
                $('.refund_ed').text(data["merchantAuditTime"]);
            };
            // 判断审核状态是否为空
            if(data["merchantAuditStatus"] == null){
                if(data["type"] == '退款'){
                    go('/member/myorders/refund/onlyrefund.html#' + _hash);
                }else{
                    go('/member/myorders/refund/chooserefund.html#' + _hash);
                }
            };
            // 退款退货订单
            if(data["type"] == '退款退货'){
                if(data["merchantAuditStatus"] == '审核成功'){
                    $('.consignee_').text(data["address"]["consignee"]);
                    $('.phone_').text(data["address"]["phone"]);
                    $('.address_').text(data["address"]["address"]);
                    $('.consigneeId').val(data["address"]["id"]);
                };
            };
            

            // 货是否送到
            $('.confirmStatus').val(data["confirmStatus"]);

            refundStatusFn(data["merchantAuditStatus"]);

            // 交易投诉状态
            $('.complaintData').val(data["serviceType"]);
            $('.complaintId').val(data["id"]);


        }
    });

    // 获取信息2
    post({
        url:'/order/' + _hash + '/findOrderProduct',
        success:function(data){
            console.log(data);
            $('.productUrl').attr('src',SERVER_CONTEXT_PATH + '/load/picture/' + data["productUrl"]);
            $('.productName').text(data["productName"]);
            $('.productSpecification').text(data["productSpecification"]);
            $('.price').text('￥'+returnFloat(data["price"]));
            $('.num').text('x'+data["num"]);
            $('.supplyName').text(data["supplyName"]);
            $('.shippingFee').text('￥'+returnFloat(data["shippingFee"]));
            $('.orderAmount').text('￥'+returnFloat(data["orderAmount"]));
            $('.productCode').text(data["productCode"]);
        }
    });

    // 退款申请
    $('.refundBtn').click(function(){
        if($('.type').text() == '退款'){
            post({
                url:'/order/'+_hash+'/saveAgainRefund',
                success:function(){
                    go('/member/myorders/refund/onlyrefund.html#' + _hash);
                }
            })
        }else{
            post({
                url:'/order/'+_hash+'/saveAgainRefund',
                success:function(){
                    go('/member/myorders/refund/chooserefund.html#' + _hash);
                }
            })
        };
    });

    // 状态判断
    function refundStatusFn(data){
        if(data == '审核中'){
            console.log(33333)            
            $('.refund_progress').attr('src','img/dian2.png');
            $('.ref_').addClass('tab');
            $('.ref_ing').removeClass('tab');
            $('.ref_suc').addClass('tab');
            $('.ref_ifGet').addClass('tab');
        }else if(data == '审核成功'){
            console.log(2222222)            
            if($('.confirmStatus').val == '审核中'){
                $('.ref_ifGet').removeClass('tab');
                $('.progress li').addClass('redcolor');
                $('.ref_').addClass('tab');
                $('.ref_success').addClass('tab');
                $('.ref_suc').addClass('tab');
            }
            if($('.type').text() == '退款退货'){
                if($('.confirmStatus').val() == '审核中'){
                    $('.ref_ifGet').removeClass('tab');
                    $('.progress li').addClass('redcolor');
                    $('.ref_').addClass('tab');
                    $('.ref_success').addClass('tab');
                    $('.ref_suc').addClass('tab');
                }else{
                    $('.progress li').addClass('redcolor');
                    $('.ref_').addClass('tab');
                    $('.ref_success').addClass('tab');
                    $('.ref_suc').removeClass('tab');
                    $('.ref_ifGet').addClass('tab');
                }
            }else{
                $('.progress li').addClass('redcolor');
                $('.ref_').addClass('tab');
                $('.ref_success').removeClass('tab');
                $('.ref_suc').addClass('tab');
                $('.ref_ifGet').addClass('tab');
            };
        }else if(data == '审核失败'){
            console.log(11111)
            $('.ref_shibai').text('退款失败')
            $('.progress li').addClass('redcolor');
            $('.ref_').addClass('tab');
            $('.ref_fail').removeClass('tab');
            $('.ref_suc').addClass('tab');
            $('.ref_ifGet').addClass('tab');
        }
    };

    // 提交快递单
    $('.ref_submit').click(function(){
        if($('.wlgs').val() == '' || $('.yhdh').val() == ''){
            alert('请填写完整信息。');
            return;
        };
        var data ={
            id: _hash,
            logistics: $('.wlgs').val(),
            trackNumber:$('.yhdh').val()
        };
        post({
            url:'/order/saveRefund',
            data:data,
            success:function(){
                go('/member/orderdetail/orderdetail.html#'+$('.productCode').text());
            }
        });
    });

    // 交易投诉按钮
    $('.complaint_ing').click(function(e){
        e.stopPropagation();
        var complaintData ={
            referType:'SHOP',
            referId:$('.complaintId').val(),
            type:$('.complaintData').val()
        };
        post({
            url:'/member/dispute/isComplain',
            data:complaintData,
            success:function(data){
                console.log(data);
                if(data == true){
                    var _data = {
                        referType:'SHOP',
                        type:$('.complaintData').val(),
                        referId:_hash
                    };
                    //console.log(data)
                    post({
                        url:'/member/dispute/complain/referSearch',
                        data:_data,
                        success:function(_id){
                            go('/member/complaint/disputedetail.html#item='+ _id);
                        }
                    });
                }else{
                    go('/member/complaint/disputeapply.html#'+_hash);
                    return false;
                }
            }
        });
    });


    // FOOTER
    footerFn();

    function footerFn() {
        post({
            url: '/member/phone/blur',
            success: function (data) {
                $('.foo_infor_name').text(data);
            }
        });
        $('.foo_infor_exit').click(function () {
            $.post({
                url: SERVER_CONTEXT_PATH+'/logout',
                success:function(){
                    
                    go('/index.html');
                }
            });
        });
        $('.foo_infor_up').click(function () {
            $('html,body').animate({
                scrollTop: '0px'
            }, 200);
        });
    };
});