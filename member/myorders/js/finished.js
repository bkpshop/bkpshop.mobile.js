$(function () {
    var pageNum = 1;
    allorderFn(pageNum);
    iScroll();

    // 订单数据函数
    function allorderFn(pageNum) {
        var orderList = {
            status: "WAIT_REVIEW",
            page: {
                "pageNo": pageNum,
                "pageSize": "5",
                "sorts": [{"order":"DESC", "property":"createTime"}]
            }
        };
        post({
            url: '/member/order/list',
            data: orderList,
            success: function (data) {
                var list = data.list
                if (data.total == 0) {
                    $('.noOrder').removeClass('tab');
                } else {
                    $('.noOrder').addClass('tab');
                    for (var i = 0; i < list.length; i++) {
                        var listData = list[i];
                        orderListFnD(listData, pageNum);
                    };
                };
                orderBtnFn();
            }
        });
    };

    // 上拉刷新函数
    function iScroll() {
        function iScrollA() {
            var scrollTop = 0,
                bodyScrollTop = 0,
                documentScrollTop = 0;
            if (document.body) {
                bodyScrollTop = document.body.scrollTop;
            }
            if (document.documentElement) {
                documentScrollTop = document.documentElement.scrollTop;
            }
            scrollTop = (bodyScrollTop - documentScrollTop > 0) ? bodyScrollTop : documentScrollTop;
            return scrollTop;
        }

        function iScrollB() {
            var windowHeight = 0;
            if (document.compatMode == "CSS1Compat") {
                windowHeight = document.documentElement.clientHeight;
            } else {
                windowHeight = document.body.clientHeight;
            }
            return windowHeight;
        }

        function iScrollC() {
            var scrollHeight = 0,
                bodyScrollHeight = 0,
                documentScrollHeight = 0;
            if (document.body) {
                bodyScrollHeight = document.body.scrollHeight;
            }
            if (document.documentElement) {
                documentScrollHeight = document.documentElement.scrollHeight;
            }
            scrollHeight = (bodyScrollHeight - documentScrollHeight > 0) ? bodyScrollHeight : documentScrollHeight;
            return scrollHeight;
        }
        window.onscroll = function () {
            showGoTop($('body').scrollTop())
            if (iScrollC() == iScrollB() + iScrollA()) {
                pageNum += 1;
                allorderFn(pageNum);
                return pageNum;
            }
        }
    };
})