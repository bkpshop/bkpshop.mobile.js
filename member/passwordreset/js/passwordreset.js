$(function(){
    // 是否登陆
    goLogin();

    $('.cur_password>input,.new_password>input,.affirm_password>input').change(function(){
        var regStr = /^(?![\d]+$)(?![a-zA-Z]+$)(?![^\da-zA-Z]+$).{6,20}$/;
        if(!regStr.test($(this).val())){
            console.log($(this).val())
            $(this).val('');
        };
    })

    $('section>div').click(function(){
        if($('.cur_password>input').val() == ''){
            alert('请填写当前密码。');
        }else if($('.new_password>input').val() == ''){
            alert('请输入新密码。');
        }else{
            if($('.new_password>input').val()!=$('.affirm_password>input').val()){
                alert('新密码不一致，请重新输入。');
            }else{
                var data ={
                    password:Base64.encode($('.cur_password>input').val()),
                    newPassword:Base64.encode($('.new_password>input').val())
                };
                post({
                    url:'/member/resetPassword',
                    data:data,
                    success:function(data){
                        if(data == null){
                            alert('密码重置成功！');
                            go('../index.html');
                        }
                    },
                    error:function(msg){
                        alert(msg);
                    }
                });
            };
        };
    })
    
})