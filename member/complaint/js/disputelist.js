/**
 * Created by Scoot on 2017/3/15.
 */

var _referType = 'SHOP';
var _status = null;
var _type = null;
var _day = "0";
var _pageNum = 1;

$(function(){
    var hash = window.location.hash;
    $(".go-back").click(function(){
        if(hash){
            go("/member/myorders/refund/refunding.html"+hash);
        }else{
            go("/member/myorders/allorder.html");
        }
    });

    init();

});

function init(){

    getList(true);

    post({//查询条件
        url:"/member/dispute/conditions",
        success:function(data){
            condition(data);
        }
    });

    //点击查看全部
    $(".all_select .btn").click(function(){
        window.location.reload();
    });
}

function condition(data){
    post({
        url:"/member/dispute/"+_referType+"/typeList",
        success:function(data2){
            //console.log(data2)
            for(var k in data2){ //申诉类型
                $('.type').append('<option value="'+data2[k]["type"]+'">'+data2[k]["text"]+'</option>')
            }
        }
    });

    for(var i in data["statuses"]){
        $('.status').append('<option value="'+data["statuses"][i]["status"]+'">'+data["statuses"][i]["text"]+'</option>')
    }

    for(var j in data["times"]){
        $('.time').append('<option value="'+data["times"][j]["day"]+'">'+data["times"][j]["text"]+'</option>')
    }

    $('.type').change(function(){
        $('.record_list').empty();
        if($(this).val() == "null"){
            _type = null;
        }else{
            _type = $(this).val();
        }
        _pageNum = 1;
        getList(true);
    });

    $('.status').change(function(){
        $('.record_list').empty();
        if($(this).val() == "null"){
            _status = null;
        }else{
            _status = $(this).val();
        }
        _pageNum = 1;
        getList(true);
    });

    $('.time').change(function(){
        $('.record_list').empty();
        _day = $(this).val();
        _pageNum = 1;
        getList(true);
    });
}


function getList(flagLength){

    var _data = {
        referType: _referType,
        status:_status,
        day: _day,
        type: _type,
        page: {
            "pageNo": _pageNum,
            "pageSize": "20",
            "sorts": []
        }
    };

    post({
        url:'/member/dispute/list/complain',
        data:_data,
        success:function(data){
            //console.log(data);
            listFun(data.list,flagLength);
        }
    })
}

function listFun(list,flagLength){

    var _html = '';
    for(var i=0;i<list.length;i++){
        _html += '<a href="disputedetail.html#item='+list[i].id+'"><ul class="clear">';
        _html += '<li>'+list[i]["createTime"]+'</li>';
        _html += '<li>'+list[i]["code"]+'</li>';
        _html += '<li>'+list[i]["type"]["text"]+'</li>';
        _html += '<li>'+list[i]["status"]["text"]+'</li></ul></a>';
    }
    $(".record_list").append(_html);

    if(flagLength == true && list.length<=0){
        $(".record_list").addClass("tab");
        $(".none").removeClass("tab");
    }else{
        $(".none").addClass("tab");
        $(".record_list").removeClass("tab");
        scroll();
    }
}



// 上拉加载
//文档高度
function getDocumentTop() {
    var scrollTop = 0, bodyScrollTop = 0, documentScrollTop = 0;
    if (document.body) {
        bodyScrollTop = document.body.scrollTop;
    }
    if (document.documentElement) {
        documentScrollTop = document.documentElement.scrollTop;
    }
    scrollTop = (bodyScrollTop - documentScrollTop > 0) ? bodyScrollTop : documentScrollTop;
    return scrollTop;
}
//可视窗口高度
function getWindowHeight() {
    var windowHeight = 0;
    if (document.compatMode == "CSS1Compat") {
        windowHeight = document.documentElement.clientHeight;
    } else {
        windowHeight = document.body.clientHeight;
    }
    return windowHeight;
}

//滚动条滚动高度
function getScrollHeight() {
    var scrollHeight = 0, bodyScrollHeight = 0, documentScrollHeight = 0;
    if (document.body) {
        bodyScrollHeight = document.body.scrollHeight;
    }
    if (document.documentElement) {
        documentScrollHeight = document.documentElement.scrollHeight;
    }
    scrollHeight = (bodyScrollHeight - documentScrollHeight > 0) ? bodyScrollHeight : documentScrollHeight;
    return scrollHeight;
}

function scroll(){
    window.onscroll = function () {
        //监听事件内容
        if(getScrollHeight() == getWindowHeight() + getDocumentTop()){
            //当滚动条到底时,这里是触发内容
            //异步请求数据,局部刷新dom
            _pageNum++;
            console.log(_pageNum)
            getList(false);
        }
    };
}

