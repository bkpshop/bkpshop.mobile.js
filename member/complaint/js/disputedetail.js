/**
 * Created by Scoot on 2017/3/15.
 */

$(function(){
    var _hash = window.location.hash.split("item=")[1];

    post({
        url:"/member/dispute/"+_hash,
        success:function(data){
            //console.log(data);
            dealData(data);
        }
    });

    $("header .manage").click(function(){
        go("disputelist.html");
    });

    $("header .left").click(function(){
        window.history.go(-1);
    });


});

function dealData(data){

    var _li = '<li><span class="left">申诉者：</span><span class="right complainant">'+data.complainant+'</span></li>';
    _li += '<li><span class="left">申诉事件：</span><span class="right single title">'+data.title+'</span></li>';
    _li += '<li><span class="left">申诉类型：</span><span class="right type">'+data["type"]["text"]+'</span></li>';
    _li += '<li><span class="left">事件编号：</span><span class="right code">'+data.code+'</span></li>';
    _li += '<li><span class="left">申诉金额：</span><span class="right amount">'+returnFloat(data.amount)+'元</span></li>';
    _li += '<li><span class="left">订单编号：</span><span class="right referCode">'+data.referCode+'</span></li>';
    _li += '<li><span class="left">申诉内容：</span><span class="right more_single content">'+data.content+'</span></li>';

    $(".list").html(_li);
    $(".complainant").text(data.complainant);
    $(".title").text(data.title);

    if(data["status"]["status"] == "PENDING"){
        $(".dealing,.tip").removeClass("tab");
        $(".success").addClass("tab");
    }else{
        $(".dealing,.tip").addClass("tab");
        $(".success").removeClass("tab");
    }
}



