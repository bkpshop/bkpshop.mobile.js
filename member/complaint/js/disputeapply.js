var _referId = '';
var _referCode = '';
var _type = '';
var _complainedUID = '';
var _amount = '';
var _content = '';

$(function(){

    _referId = window.location.hash.split("#")[1];

    post({
        url:"/member/order/item/"+_referId+"/dispute",
        success:function(data){
            //console.log(data);
            deal(data);
        }
    });

    $(".manage").click(function(){
        go("disputelist.html#"+_referId);
    });

    $(".go-back").click(function(){
        go("/member/myorders/refund/refunding.html#"+_referId);
    });
});

function deal(data){
    _referCode = data.code;
    _amount = data.amount;
    _type = data["type"];
    _complainedUID = data.complainedUID;

    $(".title").text(data.title);
    $(".amount").text(returnFloat(_amount) + "元");

    $(".submit").click(function(){

        var _data = {
            referType:"SHOP",
            referId:_referId,
            referCode:_referCode,
            type:_type,
            event:"SHOP_REFUND_AND_RETURNS_REJECT",
            complainedUID:_complainedUID,
            amount:_amount,
            content:$(".content").val().trimSpace()
        };

        post({
            url:"/member/dispute/apply",
            data:_data,
            success:function(data){
                go("disputelist.html");
            }
        })
    });
}