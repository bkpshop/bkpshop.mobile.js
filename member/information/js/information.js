var isApp = false;
var equipmentType = "common";

if (/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)) {
    equipmentType= "ios";
} else if (/(Android)/i.test(navigator.userAgent)) {
    equipmentType = "android";
} else {
    equipmentType = "common";
}

$(function () {

    /*$(".albumWrapper").click(function(){

        try{
            ifApp();
            isApp = true;
        }catch (e){
            isApp = false;
        }
        if(equipmentType != "ios" && isApp == true){
            $(".albumWrapper").empty().attr("href","js-call://album/albumCallback");
            $(".albumWrapper input").attr("type","button");
        }

    });*/

    //登录验证
    goLogin();

    $(".message_alert_affirm").on("click",function(){
        $(".message_alert").addClass("tab");
    });

    // 数据获取
    post({
        url: '/member/info',
        success: function (data) {
            $('.sec_nickname>span:nth-child(2)').text(data["phone"]);
            if (data["avatar"]) {
                console.log(data["avatar"])
                if(getImg(data["avatar"])){
                    $('.sec_head>img').attr('src', SERVER_CONTEXT_PATH + "/load/picture/" + data["avatar"]);
                }else{
                    $('.sec_head>img').attr('src',data["avatar"]);
                };
            };
            if (data["sex"]) {
                $('.sec_gender>span').text(data["sex"]);
            };
            if (data["birthDay"]) {
                $('.sec_birthday>span').text(data["birthDay"]);
            };
        }
    });

    // 性别与日期的映射
    $('.sec_gender>select').change(function () {
        $('.sec_gender>span').text($('.sec_gender>select').val());
        if ($('.sec_gender>select').val() == '男') {
            console.log('MALE');
            var sex = 'MALE';
        } else {
            console.log('FEMALE');
            var sex = 'FEMALE';
        };
        post({
            url: '/member/updateSex/' + sex
        });

    });
    $('.sec_birthday>input').change(function () {
        $('.sec_birthday>span').text($('.sec_birthday>input').val());
        var birthday = $('.sec_birthday>input').val();
        post({
            url: '/member/updateBirthday/' + birthday
        });
    });

    $('.sec_head input').change(function () {
        uploadPic('.sec_head input', '/upload/picture', '.sec_head>img');
    });

    //照片上传函数
    function uploadPic(dom, url, target) {
        var fd = new FormData();
        fd.append("file", $(dom).get(0).files[0]);
        var size = $(dom)[0].files[0].size;
        var type = $(dom)[0].files[0].type;
        if(size<=6*1024*1024){
            if(type == 'image/jpeg' || type == 'image/png'){
                $.ajax({
                    url: SERVER_CONTEXT_PATH + url,
                    type: 'POST',
                    data: fd,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        $(target).attr("src", SERVER_CONTEXT_PATH + "/load/picture/" + data);
                        post({
                            url: '/member/uploadAvatar',
                            data:{imageCode:data},
                            success: function (data) {
                                console.log(data)
                            }
                        })
                    },
                    error:function(msg){
                        if(msg.status == 413){
                            messageAlert('抱歉，上传图片出现问题，您的图片大小超过限制，请您压缩后重新上传！');
                        }else{
                            messageAlert('抱歉，上传图片失败！');
                        }
                    }
                });
            }else{
                messageAlert('请上传jpg/png格式的照片!');
            }
        }else{
            messageAlert('上传照片不能大于6M!');
        }
    }


    function blobToDataURL(blob, callback) {
        var a = new FileReader();
        a.onload = function (e) { callback(e.target.result); };
        a.readAsDataURL(blob);
    }

    function getBase64Image(img) {
        var canvas = document.createElement("canvas");
        canvas.width = img.width;
        canvas.height = img.height;
        var ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0, img.width, img.height);
        var ext = img.src.substring(img.src.lastIndexOf(".") + 1).toLowerCase();
        var dataURL = canvas.toDataURL("image/" + ext);
        return dataURL;
    }



    // 菜单键切换
    var i = 0;
    $('.header_menu').click(function () {
        if (i == 0) {
            $('#bottom').removeClass('tab');
            i = 1;
        } else {
            $('#bottom').addClass('tab');
            i = 0;
        };
    });


     // FOOTER
    footerFn();

    function footerFn() {
        post({
            url: '/member/phone/blur',
            success: function (data) {
                $('.foo_infor_name').text(data);
            }
        });
        $('.foo_infor_exit').click(function () {
        	$("#logout").attr("action", SERVER_CONTEXT_PATH + "/logout").submit();
        });
        $('.foo_infor_up').click(function () {
            $('html,body').animate({
                scrollTop: '0px'
            }, 200);
        });
    };

})

function messageAlert(msg){
    $(".message_alert .msg").text(msg);
    $(".message_alert").removeClass("tab");
}

function albumCallback(imageData) {
    //alert(imageData);
    //var img = createImageWithBase64(imageData);
    //document.getElementById("albumWrapper").appendChild(img);
}

function iosApp(str){
    isApp = true;
    return true;
}