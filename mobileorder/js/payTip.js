/**
 * Created by Scoot on 2017/3/3.
 */

var equipmentType = "common";

if (/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)) {
    equipmentType= "ios";
} else if (/(Android)/i.test(navigator.userAgent)) {
    equipmentType = "android";
} else {
    equipmentType = "common";
}

$(function(){

    // 提示用户关闭无痕模式。
    if (typeof localStorage === 'object') {
        try {
            localStorage.setItem('localStorage', 1);
            localStorage.removeItem('localStorage');
        } catch (e) {
            Storage.prototype._setItem = Storage.prototype.setItem;
            Storage.prototype.setItem = function() {};
            alert('您处于无痕浏览，页面无法正常显示，请关闭无痕模式。');
        }
    };

    var pay = [
        "/alipay/customizeConfirmPayFor", //定制商品确认下单
        "/alipay/customizePayFor"//未支付订单
    ];
    var _url = $.getUrlParam("url");
    _url = decodeURIComponent(_url);
    //var code = $.getUrlParam("code");

    if(isWeiXin()){
         $(".pay_container").css("display","block");
    }else{
        if(_url){
            setTimeout(function(){
                go(_url);
                /*if(_url.indexOf("customizeConfirmPayFor")>-1){
                    go("/alipay/customizeConfirmPayFor?code="+code);
                }else{
                    go("/alipay/customizePayFor?code="+code);
                }*/

            },30);
        }else{
            if(equipmentType == "ios"){
                alert("请关闭浏览器无痕模式后重新支付！");
            }else{
                alert("支付失败，请重新支付！");
            }
        }

    }
});

//判断是否是微信
function isWeiXin(){
    var ua = window.navigator.userAgent.toLowerCase();
    if(ua.match(/MicroMessenger/i) == 'micromessenger'){
        return true;
    }else{
        return false;
    }
}