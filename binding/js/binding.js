$(function () {
	var url = window.location.href;
	var accountToken = url.split('?')[1];
	var accountTokenData = accountToken.split('=')[1];
	// 初始验证码
	$(".verification_code").attr("src", "" + SERVER_CONTEXT_PATH + "/captcha.jpg?t=" + new Date().getTime());
	$('.verification_code').click(function () {
		$(".verification_code").attr("src", "" + SERVER_CONTEXT_PATH + "/captcha.jpg?t=" + new Date().getTime());
	});

	$('.password>input').keyup(function () {
		post({
			url: '/captcha/' + $(this).val(),
			success: function (data) {
				if (data == true) {
					$('.getYZM').addClass('canget');
				} else {
					$('.getYZM').removeClass('canget');
				}
			}
		});
	});

	// 点击获取验证码
	$('.getYZM').on('click', function () {
		if ($(this).hasClass('canget')) {
			var ExgStr = /^1[34578]\d{9}$/;
			if (ExgStr.test($('.phone>input').val()) && $('.phone>input').val() != '') {
				console.log(1111);
				timeFn();
				$('.getYZM').removeClass('canget');
				post({
					url: '/phoneRandomCode/' + $('.phone>input').val(),
					success: function (data) {
						alert('短信已经发送到该手机号请查收！');
					}
				});
			} else {
				alert('请填写正确手机号码。');
			}
		}else{
			alert('请输入正确的验证码。');
		}
	});

	// 确认绑定按钮
	$('.d_submit').click(function () {
		var ExgStr = /^1[34578]\d{9}$/;
		if (ExgStr.test($('.phone>input').val()) && $('.phone>input').val() != '' && $('.yzm>input').val() != '') {
			post({
				url: '/weixin/bingding/' + accountTokenData + '/' + $('.phone>input').val() + '/' + $('.yzm>input').val() + '',
				success: function (data) {
					// console.log(data);
					go(data);
				}
			});
		}else{
			alert('请将信息填写正确。');
		}
	});

	// 定时器
	function timeFn() {
		var count = 59;
		var timer = setInterval(function () {
			count--;
			if (count == 0) {
				clearInterval(timer);
				$(".getYZM").text("获取验证码");
				$(".getYZM").addClass('canget');
				return;
			};
			$(".getYZM").text(count + "s重新获取");
		}, 1000);
	};

});