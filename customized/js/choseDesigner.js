
$(function(){

    var index_design = 0,index_chose = 0;
    $(".wrap_main .wrap_bottom .title li:nth-child(1)").click(function(){
        $(".wrap_main .wrap_bottom .title li").removeClass("selected");
        $(this).addClass("selected");
        $(".wrap_main .good_detail").css("display","block");
        $(".wrap_main .good_attribute,.wrap_main .life").css("display","none");
    });

    $(".wrap_main .wrap_bottom .title li:nth-child(2)").click(function(){
        $(".wrap_main .wrap_bottom .title li").removeClass("selected");
        $(this).addClass("selected");
        $(".wrap_main .good_detail,.wrap_main .life").css("display","none");
        $(".wrap_main .good_attribute").css("display","block");
    });
    //点击底部“加入购物车”
    $(".add_cart").click(function(){
        $(".container").css("position","fixed");
        $(".c_container").css("display","block");
        $(".cover").animate({"opacity":"0.5"},"500");
        $(".c_main").animate({"bottom":"0%"},"500");
    });
    //点击关闭 选择规格页面
    $(".c_main .close").click(function(){
        $(".container").css("position","static");
        $(".c_main").animate({"bottom":"-120%"},"500");
        $(".cover").animate({"opacity":"0"},"500");
        setTimeout(function(){
            $(".c_container").css("display","none");
        },500)
    });
    //返回上一级
    $(".go-back").click(function(){
        go(decodeURIComponent(getCookie("productdetailgoback")));
    });
    



    var _csid,_productid,_code,_dsgid=[],_designType='ORIGINAL',_num;
    var url = location.search;
    
    var theRequest = new Object();
    if (url.indexOf("?") != -1) {  
        var str = url.substr(1);  
        strs = str.split("&");  
        for(var i = 0; i < strs.length; i ++) {  
           theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);  
        } 
        // console.log(theRequest) 
        _csid = theRequest.csid != null?theRequest.csid:'';
        _productid = theRequest.productid != null?theRequest.productid:'';
        _code = theRequest.code != null?theRequest.code:'';
        _num = theRequest.num != null?theRequest.num:'';
    };
    console.log(_productid)  
    post({
        url:'/customizeProduct/designerList',
        data:{
            csId:parseInt(_csid),
            productId:parseInt(_productid)
        },
        success:function(data){
            console.log(data);

            $('.design_title').html(data.specificationDto[index_design].name);
            $('.good_detail').html(data.specificationDto[index_design].remark);
            desiginfo(data);
            desigList(data);
            $('.design_name li:nth-child(1)').addClass('selected').siblings().removeClass('selected');
        },
        error:function(msg){
            console.log(msg)
        }
    });
    //提交信息
    // buttonColor();
    $('.up_info').click(function(){
        var _data_up = {
            code:_code,
            dsgIds:_dsgid.toString(),
            designType:_designType
            // contact:$('.contacts input').val(),
            // contactTel:$('.tel input').val()

        };console.log(_data_up)
        if(_designType == 'DESIGN'){
            if(_dsgid){
                up_data(_data_up);
                // go('./designcontainer.html')
            }else{
                $('.gat_background').removeClass('tab');
                $('.gat_background').click(function(){
                    $('.gat_background').addClass('tab')
                })
                // alert('请将信息填写完整')
            }
        }else if(_designType == 'ORIGINAL'){
            up_data(_data_up)
        }
        
    })
     //查看评价
   $('.go-comlist').click(function(){
        go('/customized/designerComment/index.html#'+_productid)
   })
    function up_data(_data_up){
        console.log(_dsgid)
        if(_dsgid.length){
            post({
                url:'/customizeOrder/saveOrderTemporary',
                data:_data_up,
                success:function(data){
                    localStorage.setItem("_url",window.location.href);    
                    go('./designcontainer.html?code='+_code);

                },
                error:function(msg){
                    console.log(msg)
                }
            })
        }else{
            post({
                url:'/customizeOrder/saveOrderTemporary',
                data:_data_up,
                success:function(data){
                    console.log(data)
                    localStorage.setItem("choseDesigner", encodeURIComponent(window.location.href));
                    setCookie("confirmCustomCode",data);
                       go('/mobileorder/confirmCustom.html'); 
                },
                error:function(msg){
                    console.log(msg)
                }
            })
        }
        
    }
   //轮播图
   
    function desiginfo(data_1){
        // console.log(data_1)
        var data = data_1.specificationDto;
        console.log(data)
        for(var k in data){
            $('.design_name ul').append('<li class="">设计商'+parseInt(k+1)+'</li>');
            var pics = data[k].designerPictures;
            var html_img = '';
            for(var j in pics){
                html_img += '<div class="swiper-slide"><img src="'+SERVER_CONTEXT_PATH+'/load/picture/'+pics[j]+'"></div>'
            }

        };
        
        $('.swiper-wrapper').append(html_img)

        if(pics.length>1){
            var mySwiper = new Swiper ('.info .swiper-container', {
                pagination: '.swiper-pagination',
                paginationElement : 'span',
                loop: true,//循环
                autoplay: 3000//播放间隔
            });
        }

        zoomImg($('.info .swiper-container img'),data_1["pictures"]); // 查看放大图

        $('.c_price span').html(data_1.minPrice);
        $('.design_name li').click(function(){
            $(this).addClass('selected').siblings().removeClass('selected');
            index_design = $(this).index();console.log(index_design)
            // $('.swiper-slide img').attr('src',""+SERVER_CONTEXT_PATH+"/load/picture/"+data[index_design].imageUrl+"")
            $('.design_title').html(data[index_design].name);
            $('.good_detail').html(data[index_design].remark);
        });

    }
    function desigList(data_2){
        var data = data_2.specificationDto;
        $('.img_small').attr('src',""+SERVER_CONTEXT_PATH+"/load/picture/"+data_2.imageUrl+"");
        $('.pro_title').html(data_2.name);
        $('.moren').click(function(){
            $('.capacity,.contacts,._tel').addClass('tab');
            $(this).addClass('red_back');
            $('.taste li').removeClass('red_back');
            $('.capacity ul').empty();
            $('.pro_title').html($(this).text());
            $('.design_content').html('');
            $('.price span').html('0 (含制作)')
            _designType='ORIGINAL';
            _dsgid = [];
        });
        for(var i in data){
            $('.taste ul').append('<li data-id="'+data[i].id+'">'+data[i].name+'</li>')
        };
        $('.taste li').click(function(){
            $('.capacity,.contacts,._tel').removeClass('tab');
            index_chose = $(this).index();console.log(index_chose)
            $(this).addClass('red_back').siblings().removeClass('red_back');
            $('.moren').removeClass('red_back')
            $('.design').html($(this).text());
            $('.design').data('id',$(this).data('id'))
            $('.capacity ul').empty();
            _dsgid = [];
            for(var m in data[index_chose].attributes){
                $('.capacity ul').append('<li name="'+m+'" data-id="'+data[index_chose].attributes[m].id+'">'+data[index_chose].attributes[m].name+'</li>')
            };
            $('.capacity li').click(function(){
                // $(this).addClass('red_back').siblings().removeClass('red_back');
                $(this).toggleClass('red_back');
                $('.design_content').html($(this).text());
                $('.design_content').data('id',$(this).data('id'));
                console.log($(this))
                var _this = $(this)
                desigOrd(_this)
            });
           _designType='DESIGN'
        });

        
    }
    function desigOrd(_this){
        console.log($(_this).attr('name'))
        var _data = {
            designerId:$('.design').data('id'),
            attrId:$('.design_content').data('id'),
            csId:parseInt(_csid),
            productId:parseInt(_productid),
            num:parseInt(_num)
        };console.log(_data)
        post({
            url:'/customizeProduct/designerDetail',
            data:_data,
            success:function(data){
                if(_this.hasClass('red_back')){
                    let beforeNum = parseInt($('.price span').html()),
                        nextNum = parseInt(data.price)
                    $('.price span').html(beforeNum+nextNum+' (含制作)');
                    _dsgid.push(data.id)
                }else{
                    let beforeNum = parseInt($('.price span').html()),
                        nextNum = parseInt(data.price);
                        $('.price span').html(beforeNum-nextNum+' (含制作)')
                        removeAry(_dsgid,data.id)
                }
                
                   
            },
            error:function(msg){
                console.log(msg)
            }
        })
    }
    
    // 数组处理
    function removeAry(data,delet){
        for(let i=0;i<data.length;i++){
            if(data[i]==delet){
                return data.splice(i,1)
            }
        }
    };
    // function buttonColor(){
    //     if($('.pro_title').text()&&$('.design_content').text()&&$('.contacts>input').val()&&$('.tel>input').val()){
    //         $('.up_info').css('background-color','ff7900');
    //         $('.up_info').click(function(){
    //             var _data_up = {
    //                 code:_code,
    //                 dsgId:_dsgid,
    //                 designType:"DESIGN",
    //                 contact:$('.contacts input').val(),
    //                 contactTel:$('.tel input').val()
    //             };console.log(_data_up)
    //             // post({
    //             //     url:'/customizeOrder/saveOrderTemporary',
    //             //     data:_data_up,
    //             //     success:function(data){
    //             //         console.log(data)
    //             //         go('confirmCustom.html#'+data)
    //             //     },
    //             //     error:function(msg){
    //             //         console.log(msg)
    //             //     }
    //             // })
    //         })
    //     }
    // }
})
