
$(function(){
    var _href = window.location.hash;
    var _index = _href.split("#")[1];

    post({
        url : "/isLogin",
        success : function(data){
            if(data){
                isLogin = true;
            }else{
                isLogin = false;
            }
        }
    });
  
    //点击底部“选择规格”
    $(".add_cart").click(function(){
        if(isLogin == true){
            $(".container").css("position","fixed");
            $(".c_container").css("display","block");
            $(".cover").animate({"opacity":"0.5"},"500");
            $(".c_main").animate({"bottom":"0%"},"500");
        }else{
            go("/login/index.html");
        }
    });
    //点击关闭 选择规格页面
    $(".c_main .close").click(function(){
        $(".container").css("position","static");
        $(".c_main").animate({"bottom":"-120%"},"500");
        $(".cover").animate({"opacity":"0"},"500");
        setTimeout(function(){
            $(".c_container").css("display","none");
        },500)
    });
    //返回上一级
    $(".go-back").click(function(){
        go(decodeURIComponent(getCookie("productdetailgoback")));
    });
    
   //查看评价
   $('.go-comlist').click(function(){
        go('/customized/manufacturerComment/index.html#'+_index)
   })

    var index_manuf = 0;
    var index_attri = 0;
    var index_spec = 0;
    var msgid = '';
    var _moq = 0;
    post({
        url:"/customizeProduct/"+_index+"/detail",
        success:function(data){
            console.log(data);
            // if(data.status == 'SALE_LOWER'){
            //     $('.add_cart').css('background-color','#666').value('商品已下架').attr('disable','disable')
            // };
            var proData = data.product;
            var manufDate = data.manufacturers;
            var proPic = data.productPictures;
            var html_4 = '';

            if(proPic){
                for(var h in proPic){
                    html_4 += '<div class="swiper-slide"><img src="'+SERVER_CONTEXT_PATH+"/load/picture/"+proPic[h]+'"></div>'
                }
                $('.swiper-wrapper').append(html_4)
            }
            if(proPic.length>1){
                var mySwiper = new Swiper ('.info .swiper-container', {
                    pagination: '.swiper-pagination',
                    paginationElement : 'span',
                    loop: true,//循环
                    autoplay: 3000//播放间隔
                });
            }

            zoomImg($('.info .swiper-container img'),data["productPictures"]); // 查看放大图

            $('.name').html(proData.name);
            $('.img_small').attr('src',''+SERVER_CONTEXT_PATH+"/load/picture/"+proData.imageUrl+'')
            $('.c_price span').html(proData.minPrice);
            $('.price span').html(proData.minPrice+'/箱起');
            if(manufDate){
                for(var k in manufDate){
                    $('.title ul').append('<li>制造商'+(++k)+'</li>')
                }
            };
            $('.title li:nth-child(1)').addClass('selected').siblings().removeClass('selected');
            $('.good_detail').empty();
            //制造商信息
            var html_info = '';
            var html_manuf = '';
            if(manufDate){
                for(var i in manufDate){
                    html_info = '<p>'+manufDate[index_manuf].remark+'</p>';
                    // html_manuf += '<li data-id="'+manufDate[i].id+'">'+manufDate[i].name+'</li>'

                };
            }
            $('.good_detail').append(html_info);
            // $('.manufacturer ul').append(html_manuf);
            // moren(manufDate);
            choseManufacture(data)
            specifi(data);
            doit(manufDate);
            // choseManuf(manufDate);
        },
        error:function(msg){
            console.log(msg)
        }
    });
    //提交数据
    $('.submitdata').click(function(){
        var sub_data = {
            msgId:msgid,
            num:parseInt($('.numb').val()),
            csId:$('.packaging').data('id')
        };
        if(msgid&&(parseInt($('.numb').val())>_moq||parseInt($('.numb').val())==_moq)&&$('.packaging').data('id')){
            post({
                url:'/customizeOrder/saveOrderTemporary',
                data:sub_data,
                success:function(data){
                    console.log(data)
                    window.location.href = 'choseDesigner.html?code='+data+'&productid='+_index+'&csid='+$('.packaging').data('id')+'&num='+parseInt($('.numb').val())+''
                },
                error:function(msg){
                    console.log(msg)
                }
            })
        }else{
            $('.gat_background').removeClass('tab');
            $('.gat_background').click(function(){
                $('.gat_background').addClass('tab')
            });
            // return false
        }
    });
    //制造商信息函数
    function doit(manufDate){
        console.log(manufDate)
        $('.title li').click(function(){
            $(this).addClass('selected').siblings().removeClass('selected');
            index_manuf = $(this).index();console.log(index_manuf)
            $('.good_detail').empty();
            for(var i in manufDate){
                html_info = '<p>'+manufDate[index_manuf].remark+'</p>';

            };
            $('.good_detail').append(html_info);

        })
    }
    //弹窗选择制造商函数
    function choseManuf(manufDate){
       
        $(".manufacturer li").click(function(){
            $('.taste ul,.capacity ul,.d_standard ul').empty();
            $(this).addClass("red_back").siblings().removeClass("red_back");
            $(".standard>.company").text( $(this).text() );
            $(".standard>.company").data('id',$(this).data('id'))
            index_attri = $(this).index();
            allord();
            var specData = manufDate[index_attri].specification;console.log(specData)

                for(var m in specData[0].attributes){
                    $('.taste ul').append('<li data-id="'+specData[0].attributes[m].id+'">'+specData[0].attributes[m].name+'</li>')
                };
                for(var n in specData[1].attributes){
                    $('.capacity ul').append('<li data-id="'+specData[1].attributes[n].id+'">'+specData[1].attributes[n].name+'</li>')
                };
                for(var o in specData[2].attributes){
                    $('.d_standard ul').append('<li data-id="'+specData[2].attributes[o].id+'">'+specData[2].attributes[o].name+'</li>')
                }
            
            $(".taste li").click(function(){
                $(this).addClass("red_back").siblings().removeClass("red_back");
                $(".standard .fruit").text( $(this).text() )
                $(".standard .fruit").data('id',$(this).data('id'));
                allord()
            });
            $(".capacity li").click(function(){
                $(this).addClass("red_back").siblings().removeClass("red_back");
                $(".standard .percent").text( $(this).text() );
                $(".standard .percent").data('id',$(this).data('id'));
                allord();
            });
            $(".d_standard li").click(function(){
                $(this).addClass("red_back").siblings().removeClass("red_back");
                $(".standard .ml").text( $(this).text() );
                $(".standard .ml").data('id',$(this).data('id'));
                allord()
            });
        })
        // $('.')
        
    }
    //默认显示
    // function moren(manufDate){
    //     var specData = manufDate[index_attri].specification;
    //         for(var i in manufDate){
    //             if(i == 0){
    //                 $('.manufacturer ul').append('<li class="red_back" data-id="'+manufDate[i].id+'">'+manufDate[i].name+'</li>');
    //                 $(".standard>.company").text(manufDate[i].name);
    //                 $(".standard>.company").data('id',manufDate[i].id)
    //             }else{
    //                 $('.manufacturer ul').append('<li data-id="'+manufDate[i].id+'">'+manufDate[i].name+'</li>');
    //             }
    //         };
    //         for(var m in specData[0].attributes){
    //             if(m == 0){
    //                 $('.taste ul').append('<li class="red_back" data-id="'+specData[0].attributes[m].id+'">'+specData[0].attributes[m].name+'</li>');
    //                 $(".standard .fruit").text( specData[0].attributes[m].name )
    //                 $(".standard .fruit").data('id',specData[0].attributes[m].id);
    //             }else{
    //                 $('.taste ul').append('<li data-id="'+specData[0].attributes[m].id+'">'+specData[0].attributes[m].name+'</li>');
    //             }
    //         };
    //         for(var n in specData[1].attributes){
    //             if(n == 0){
    //                 $('.capacity ul').append('<li class="red_back" data-id="'+specData[1].attributes[n].id+'">'+specData[1].attributes[n].name+'</li>');
    //                 $(".standard .percent").text( specData[1].attributes[n].name );
    //                 $(".standard .percent").data('id',specData[1].attributes[n].id)
    //             }else{
    //                 $('.capacity ul').append('<li data-id="'+specData[1].attributes[n].id+'">'+specData[1].attributes[n].name+'</li>')
    //             }
    //         };
    //         for(var o in specData[2].attributes){
    //             if(o == 0){
    //                 $('.d_standard ul').append('<li class="red_back" data-id="'+specData[2].attributes[o].id+'">'+specData[2].attributes[o].name+'</li>');
    //                 $(".standard .ml").text(specData[2].attributes[o].name);
    //                 $(".standard .ml").data('id',specData[2].attributes[o].id)   
    //             }else{
    //                 $('.d_standard ul').append('<li data-id="'+specData[2].attributes[o].id+'">'+specData[2].attributes[o].name+'</li>')
    //             }    
    //         };
    //         choseManuf(manufDate)
    //         allord()
    // }
    //弹窗数据
    var _arry = [],flag = false;
    function choseManufacture(data){
        var data_manu = data.manufacturers;
        var html_1 = '';
        for(var i in data_manu){
            html_1 +=  '<li data-id="'+data_manu[i].id+'">'+data_manu[i].name+'</li>'
            var data_spec = data_manu[i].specification;

        }
        $('.manufacturer ul').append(html_1);
        $('.manufacturer li').click(function(){
            $('.datas').empty();
            _arry = [];
            $(this).addClass("red_back").siblings().removeClass("red_back");
            $(".standard>.company").text( $(this).text() );
            $(".standard>.company").data('id',$(this).data('id'))
            index_manuf = $(this).index();
            var html_2='',html_3='';
            for(var j in data_manu[index_manuf].specification){
                _arry.push('');
                html_2 += '<div class="spec_name clear"><p>'+data_manu[index_manuf].specification[j].name+'</p><ul>' 

                for(var k in data_manu[index_manuf].specification[j].attributes){
                    html_2 +='<li data-id="'+data_manu[index_manuf].specification[j].attributes[k].id+'">'+data_manu[index_manuf].specification[j].attributes[k].name+'</li>'
                }
                html_2 += '</ul></div>';
            };
            $('.datas').append(html_2);
            $('.spec_name').each(function(index){
                $(this).find('li').click(function(){
                    $(this).addClass('red_back').siblings().removeClass('red_back');
                    _arry[index] = $(this).data('id');
                    console.log(_arry)
                      for(var l in _arry){
                        if(_arry[l] != ''){
                            flag = true
                        }else{
                            flag = false
                        }
                    }
                    // console.log(flag)
                   if(flag){
                        allord();
                   }
                    
                });
            })
        });

        
    }

    //起订数量，金额
    function allord(){
        var _dataid={
            productId:parseInt(_index),
            manufacturerId:$('.company').data('id'),
            attrIds:_arry.toString()
        };console.log(_dataid)
        post({
            url:'/customizeProduct/manufacturerDetail',
            data:_dataid,
            success:function(data){
                console.log(data)
                if(data.moq){
                    $('.numb').val(data.moq);
                    $('.minnum').html("起订数量"+data.moq+"箱起").css('color','#999');
                    $('.price span').html(data.totalPrice+'/箱');
                    msgid = data.id;
                    _moq = data.moq;    
                    
                }else{
                    $('.minnum').html('无法定制').css('color','red');
                    $('.numb').val(0);
                    $('.price span').html('0')
                }
            },
            error:function(msg){
                console.log(msg)
            }
        })
    }
    //包装
    function specifi(data){
        var specifiData = data.customizeSpecifications;
        for(var i in specifiData){
            $('.pack ul').append('<li data-id="'+specifiData[i].id+'"><img src="'+SERVER_CONTEXT_PATH+'/load/picture/'+specifiData[i].imageUrl+'" alt="" class=""><span>'+specifiData[i].name+'</span></li>')
        }
        $(".pack li").click(function(){
                $(this).find('img').addClass("red_img").parent().siblings().find('img').removeClass("red_img");
                // $(".standard .packaging").text( $(this).text() );
                $(".standard .packaging").data('id',$(this).data('id'));
                
        });
    }

// var _moq = 0;
// //改变定制数量
changeNum();

function changeNum(){
    var count = 1;
    //数量加1
    $(".change>.add").click(function(){
        count = $(".numb").val() ;
        count ++;
        if(count <= 0){
            var addNum = 0;
        }else{
            var addNum =  count ;
        }        
        //console.log(addNum)
        $(".numb").val(addNum);
    })

    //数量减1
    $(".minus").click(function(){
        count = $(".numb").val() ;
        if($(".numb").val() <= _moq){
            count = _moq;
        }else{
            count --;
        }
        
        if(count <= 0){
            var minNum = 0;
        }else{
            var minNum =  count ;
        }
        //console.log(addNum)
        $(".numb").val(minNum);
    })
    //输入数量
    $(".numb").on("blur",function(){
        if($(this).val() < _moq){
            $(this).val('')
        }       
         // $(".change>.middle>input").val( $(".change>.middle>input").val() );
    })

}
var timeuot;
// longPress();
function longPress(){
    var num = 1;
    $('.add').mousedown(function(){
       setTimeout(function(){
            num ++;
       },500)
       console.log(num)
       $('.numb').val(num)
    })
}
})