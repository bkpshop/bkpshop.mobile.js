$(function(){
    setCookie("productdetailgoback",encodeURIComponent(window.location.href));
    var pageNum = 0;
    iScroll();
	goodList(pageNum);

    // post({
    //     url: "/isLogin",
    //     success: function (data) {
    //         if(data){
    //             uid()
    //         }
    //     }
    // });
    // function uid(){
    //     post({
    //         url:'/member/uid',
    //         success:function(data){
    //             console.log(data);

    //         }
    //     })
    // }
	function nextSession(){
		$(".datalist>li").on("click",function(){
			var id = $(this).attr("name");
            // go("choseManufacturer.html#"+id);
            go("/customize/newDesign.html?productid="+id+"")
			/*post({
				url: "/isLogin",
				success: function (data) {
					if (!data) {
						go("/login/index.html");
					} else {						
						go("choseManufacturer.html#"+id);
					}
				}
			})		*/
		})
	}

    //顶部nav

    var mySwiper = new Swiper('.nav-swiper', {
        slidesPerView: 5,
        centeredSlides: false
    })
    $(".nav-swiper li").on("click", function () {
        $(this).addClass("selected").siblings().removeClass("selected")
    })
	//商品列表
	function goodList(pageNum){
		post({
			url:'/customizeProduct/list',
			data:{page:pageNum,rows:10},
			success:function(data){
				console.log(data)
				var lists = data.productList;
				//console.log(lists)
				var html_list = '';
				for(var i in lists){
					html_list += '<li name="'+lists[i].id+'">';
                    html_list += '<img src="'+SERVER_CONTEXT_PATH+'/load/picture/'+lists[i].imageUrl+'" alt="">';
                    html_list += '<div class="custom-detail">';
                    html_list += '<h4 class="single">'+lists[i].name+'</h4><p>'+digitUppercase(lists[i].period)+'天发货</p><span>'+lists[i].minPrice+'元/箱起</span>';
                    html_list += '<input type="button" value="立即定制">';
                    html_list += '</div></li>'
				};						
				$('.datalist').append(html_list);

				nextSession();
			}
		})
	 
	}
	// 上拉刷新函数
    function iScroll() {
        function iScrollA() {
            var scrollTop = 0,
                bodyScrollTop = 0,
                documentScrollTop = 0;
            if (document.body) {
                bodyScrollTop = document.body.scrollTop;
            }
            if (document.documentElement) {
                documentScrollTop = document.documentElement.scrollTop;
            }
            scrollTop = (bodyScrollTop - documentScrollTop > 0) ? bodyScrollTop : documentScrollTop;
            return scrollTop;
        }

        function iScrollB() {
            var windowHeight = 0;
            if (document.compatMode == "CSS1Compat") {
                windowHeight = document.documentElement.clientHeight;
            } else {
                windowHeight = document.body.clientHeight;
            }
            return windowHeight;
        }

        function iScrollC() {
            var scrollHeight = 0,
                bodyScrollHeight = 0,
                documentScrollHeight = 0;
            if (document.body) {
                bodyScrollHeight = document.body.scrollHeight;
            }
            if (document.documentElement) {
                documentScrollHeight = document.documentElement.scrollHeight;
            }
            scrollHeight = (bodyScrollHeight - documentScrollHeight > 0) ? bodyScrollHeight : documentScrollHeight;
            return scrollHeight;
        }
        window.onscroll = function () {
            showGoTop($('body').scrollTop())
            if (iScrollC() == iScrollB() + iScrollA()) {
                pageNum += 1;
                goodList(pageNum);
                return pageNum;
            }
        }
    };
})