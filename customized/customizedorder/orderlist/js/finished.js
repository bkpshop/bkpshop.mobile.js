var pageNum = 0;
$(function () {
    
    allorderFn(pageNum);
    iScroll();
    operationFn();

    // var codes = [];
    // 订单数据函数
    function allorderFn(pageNum) {
        var orderList = {
            "orderStep": 'WAIT_REVIEW',
            "page": pageNum,
            "rows": "3"
        };
        post({
            url: '/customizeOrder/list',
            data: orderList,
            success: function (data) {
                console.log(data)
                var list = data.orderList;
                codes = [];
                if (data.total == 0) {
                    $('.noOrder').removeClass('tab');
                } else {
                    $('.noOrder').addClass('tab');
                    for (var i = 0; i < list.length; i++) {
                        var listData = list[i];
                        var statusName,orderBtn,countdown;
                        statusFn(listData);
                        orderListFn(listData);
                    };
                    // for(var j in list){
                    //     codes.push(list[j].code)
                    // };
                    // console.log(codes)
                    // commentBtnM();
                    add_com();
                    com()
                    // commentBtnD(list)
                    // orderBtnFn();
                };
            }
        });
    };
     //判断能否评价

// function commentBtnM(){
//     console.log(codes)
//        $('.wait_review_'+pageNum).each(function(index){
//             var _this = $(this).parent();
//            console.log(index)
//             post({
//                 url:'/customizeProduct/findReviews/',
//                 data:{
//                     code:codes[index],
//                     type:'manufacturer'
//                 },
//                 success:function(obj){
//                     console.log(obj)
//                     if(obj.isAdditional == null){
//                         _this.append('<a class="com_m"  href="/customized/manufacturerComment/publish.html#'+codes[index]+'">评价制造商</a>')
//                     }else if(obj.isAdditional == true){
//                         _this.append('<a class="com_m"  href="/customized/manufacturerComment/addition.html#'+codes[index]+'">追评制造商</a>')
//                     }
                    
//                 }
//             })

//         })
        
// }
    // 上拉刷新函数
    function iScroll() {

        function iScrollA() {
            var scrollTop = 0,
                bodyScrollTop = 0,
                documentScrollTop = 0;
            if (document.body) {
                bodyScrollTop = document.body.scrollTop;
            }
            if (document.documentElement) {
                documentScrollTop = document.documentElement.scrollTop;
            }
            scrollTop = (bodyScrollTop - documentScrollTop > 0) ? bodyScrollTop : documentScrollTop;
            return scrollTop;
        }

        function iScrollB() {
            var windowHeight = 0;
            if (document.compatMode == "CSS1Compat") {
                windowHeight = document.documentElement.clientHeight;
            } else {
                windowHeight = document.body.clientHeight;
            }
            return windowHeight;
        }

        function iScrollC() {
            var scrollHeight = 0,
                bodyScrollHeight = 0,
                documentScrollHeight = 0;
            if (document.body) {
                bodyScrollHeight = document.body.scrollHeight;
            }
            if (document.documentElement) {
                documentScrollHeight = document.documentElement.scrollHeight;
            }
            scrollHeight = (bodyScrollHeight - documentScrollHeight > 0) ? bodyScrollHeight : documentScrollHeight;
            return scrollHeight;
        }
        window.onscroll = function () {
            showGoTop($('body').scrollTop())
            if (iScrollC() == iScrollB() + iScrollA()) {
                pageNum += 1;
                allorderFn(pageNum);
                return pageNum;
            }
        }
        
    };
});