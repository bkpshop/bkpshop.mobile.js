$(function(){
    goLogin();
});

var num = 0;
var codes = [];
// 订单内容
// function orderListFn(listData, orderList) {
function orderListFn(listData) {
    if(listData["designer"]){
        var designer = `<p>设计商：${listData["designer"]}</p>`;
        var designerFee = `<p>包装设计费：<span class="light">￥${returnFloat(listData["packPrice"])}</span></p>`;
    }else{
        var designer = ` `;
        var designerFee = ` `;
    }
    
    $('section').append(
        `<aside>
            <div class="aside_top">
                <p class="orderNum">订单编号：<span>${listData["code"]}</span></p> 
                <p class="orderState">${statusName}</p>
            </div>
            <div class="aside_center">
                <a href="../orderdetail/orderdetail.html?orderId=${listData["code"]}" class="clear">
                    <img src="${SERVER_CONTEXT_PATH}/load/picture/${listData["productUrl"]}" class="aside_center_pic">
                    <div class="aside_center_text">
                        <div class="clear">
                            <p class="aside_center_text_price">${listData["productName"]}</p>
                            <p class="aside_center_text_num">x${listData["num"]}</p>
                        </div>
                        ${designer}
                        <p>商品金额：<span class="light">￥${returnFloat(listData["price"])}</span></p>
                        ${designerFee}
                    </div>   
                    ${countdown}           
                </a>
            </div> 
            <div class="aside_btm clear">
                <p class="aside_btm_text">
                    共<span>${listData["num"]}</span>件商品，实付¥<span>${returnFloat(listData["totalPrice"])}</span>
                    <span class="aside_btm_text_fei">（含运费¥<span>${returnFloat(listData["shippingFee"])}</span>）</span>
                </p>
                ${orderBtn}
                ${d_btn}
                ${m_btn}
                ${add_btn_d}
                ${add_btn_m}
            </div>
        </aside>`
    )
};
// var codes=[];
function statusFn(data){
    // var codes = []
    console.log(data);
    switch (data["orderStep"]) {
        case 'WAIT_PAYMENT':
            var time =parseInt(data["createTime"]) - parseInt(new Date().getTime()) + 86400000;
            if(time<=0){
                post({
                    url:'customizeOrder/timeout',
                    data:{code:data["code"]},
                    success:function(){
                        window.location.reload();
                    }
                })
            }else{
                var date = new Date(time);
                h = parseInt(time/(1000*60*60));
                // console.log((time-(h*1000*60*60))/(1000*60))
                m = date.getMinutes();  
                if(h>0){
                    var timeD = h + '小时' + m + '分钟';  
                }else{
                    var timeD = m + '分钟';
                }
            }
            statusName = '待付预付款'
            orderBtn = `<div name="${data["code"]}" data-amount="${data["realPrice"]}" data-type="${data["orderStep"]}" class="pay_order">支付预付款</div>`
            countdown = `<p class="countdownBlock">请在${timeD}内支付预付款</p>`
            d_btn = ``
            m_btn =  ``
            add_btn_d = ``
            add_btn_m = ``
            break;
        case 'WAIT_FINAL_PAYMENT':
            statusName = '待付尾款'
            orderBtn = `<div name="${data["code"]}" class="pay_orderB">支付尾款</div>`
            countdown = ` `
           d_btn = ` `
           m_btn =  ``
           add_btn_d = ``
            add_btn_m = ``
            break;
        case 'WAIT_PAYING':
            var time =parseInt(data["createTime"]) - parseInt(new Date().getTime()) + 86400000;
            console.log(time)
            if(time<=0){
                post({
                    url:'customizeOrder/timeout',
                    data:{code:data["code"]},
                    success:function(){
                        window.location.reload();
                    }
                })
            }else{
                var date = new Date(time);
                h = parseInt(time/(1000*60*60));
                m = date.getMinutes();  
                console.log(h)
                console.log(m)
                var timeD = h + '小时' + m + '分钟'; 
                // if(h>0){
                //     var timeD = h + '小时' + m + '分钟';  
                // }else{
                //     var timeD = m + '分钟';
                // }
            }
            statusName = '待付款'
            orderBtn = `<div name="${data["code"]}" data-amount="${data["realPrice"]}" data-type="${data["orderStep"]}" class="pay_order">支付</div>`
            countdown = `<p class="countdownBlock">请在${timeD}内支付全款</p>`
            d_btn = ` `
            m_btn =  ``
            add_btn_d = ``
            add_btn_m = ``
            break;
        case 'WAIT_DELIVERY':
            statusName = '待收货'
            orderBtn = `<div name="${data["code"]}" class="find_stream">查看物流</div><div name="${data["code"]}" class="confirm_receiving">确认收货</div>`
            countdown = ` `
           d_btn = ` `
           m_btn =  ``
           add_btn_d = ``
            add_btn_m = ``
            break;
        case 'WAIT_SEND':
            statusName = '待发货'
            orderBtn = ` `
            countdown = ` `
            d_btn = ` `
            m_btn =  ``
            add_btn_d = ``
            add_btn_m = ``
            break;
        case 'COMPLETE':
            statusName = '订单完成'
            orderBtn = `<a class="post_sale" href="tel:400-825-8521">申请售后</a>`
            countdown = ` `
            d_btn = ` `
            m_btn =  ``
            add_btn_d = `<a class="com_add_d tab" data-des="${data.designer}" data-code="${data.code}"  href="">追评设计商</a>`
            add_btn_m = `<a class="com_add_m tab" data-des="${data.designer}" data-code="${data.code}"  href="">追评制造商</a>`
            break;
         case 'CLOSED':
            statusName = '交易关闭'
            orderBtn = `<div name="${data["code"]}" class="del_order">删除订单</div>`
            countdown = ` `
            d_btn = ` `
            m_btn =  ``
            add_btn_d = ``
            add_btn_m = ``
            break;
        case 'WAIT_REVIEW':
            statusName = '待评论';
            
            d_btn = `<a class="com_d tab" data-des="${data.designer}" data-code="${data.code}"  href="/customized/designerComment/publish.html#${data.code}">评价设计商</a>`
            m_btn = `<a class="com_m tab" data-des="${data.designer}" data-code="${data.code}" href="/customized/manufacturerComment/publish.html#${data.code}">评价制造商</a>`
            orderBtn = `<a class="post_sale wait_review_${pageNum}" href="tel:400-825-8521">申请售后</a>`;
            
            countdown = ` `
            add_btn_d = ``
            add_btn_m = ``
            break;
        default:
            break;
    }
    
};

//已完成追评
function add_com(){
    //设计商
    $('.com_add_d').each(function() {
        var _this_add_d = $(this);
        var _code_add_d = $(this).data('code');
        var _des_add_d = $(this).data('des');
        if(_des_add_d){
            post({
                url:'/customizeProduct/findReviews/',
                data:{
                    code:_code_add_d,
                    type:'designer'
                },
                success:function(obj){
                    // console.log(obj)
                    if(obj.isAdditional == true){
                        _this_add_d.removeClass('tab').attr('href','/customized/designerComment/addition.html#'+obj.id+'')
                    }
                },
                error:function(msg){
                    console.log(msg)
                }
            })
        }
    });
    //制造商
    $('.com_add_m').each(function(){
        var _this_add_m = $(this);
        var _code_add_m = $(this).data('code');
        post({
            url:'/customizeProduct/findReviews/',
            data:{
                code:_code_add_m,
                type:'manufacturer'
            },
            success:function(obj){
                if(obj.isAdditional == true){
                    _this_add_m.removeClass('tab').attr('href','/customized/manufacturerComment/addition.html#'+obj.id+'')
                }
            },
            error:function(msg){
                console.log(msg)
            }
        })
    })
}




//待评价评价
function com(){
    // $('.com_d').each(function(){
    //     var  _this = $(this);
    //     var _code = $(this).data('code')
    //     var _des = $(this).data('des')
    //     post({
    //         url:'/customizeProduct/findReviews/',
    //         data:{
    //             code:_code,
    //             type:'designer'
    //         },
    //         success:function(obj){
    //             console.log(obj);
    //             if(_des){
    //                 if(obj.isAdditional == null){
    //                     _this.removeClass('tab')
    //                     // _this.append('<a class="com_d"  href="/customized/designerComment/publish.html#'+codes[index]+'">评价设计商</a>')
    //                 }else if(obj.isAdditional == true){
    //                     _this.removeClass('tab').text('追评设计商').attr('href','/customized/designerComment/addition.html#'+obj.id)
    //                     // _this.append('<a class="com_d"  href="/customized/designerComment/addition.html#'+codes[index]+'">追评设计商</a>')
    //                 }else if(obj.isAdditional == false){
    //                     _this.addClass('tab')
    //                 }
    //             }
    //         },
    //         error:function(msg){
    //                 console.log(msg)
    //             }
    //     })
    // })
    // 设计商
    $('.com_d').each(function(){
        var _this_d = $(this);
        var _code_d = $(this).data('code');
        var _des_d = $(this).data('des')
        if(_des_d){
            post({
                url:'/customizeProduct/findReviews/',
                data:{
                    code:_code_d,
                    type:'designer'
                },
                success:function(obj){
                    console.log(obj)
                    if(obj.isAdditional == null){
                        _this_d.removeClass('tab')
                    }else if(obj.isAdditional == true){
                        _this_d.removeClass('tab').text('追评设计商').attr('href','/customized/designerComment/addition.html#'+obj.id+'')
                    }else if(obj.isAdditional == false){
                        _this_d.addClass('tab')
                    }
                }
            })
        }
    })
    //制造商
    $('.com_m').each(function(){
        var _this_m = $(this);
        var _code_m = $(this).data('code');
        var _des_m = $(this).data('des')
        post({
            url:'/customizeProduct/findReviews/',
            data:{
                code:_code_m,
                type:'manufacturer'
            },
            success:function(obj){
                // console.log(obj)
                if(obj.isAdditional == null){
                    _this_m.removeClass('tab')
                }else if(obj.isAdditional == true){
                    _this_m.removeClass('tab').text('追评制造商').attr('href','/customized/designerComment/addition.html#'+obj.id+'')
                }else if(obj.isAdditional == false){
                    _this_m.addClass('tab')
                }
            }
        })
    })
}




 //制造商评价
// function commentBtnM(){

//     console.log(codes)
//        $('.wait_review_'+pageNum).each(function(index){
//             var _this = $(this).parent();
//            console.log(index)
//             post({
//                 url:'/customizeProduct/findReviews/',
//                 data:{
//                     code:codes[index],
//                     type:'manufacturer'
//                 },
//                 success:function(obj){
//                     console.log(obj)
//                     if(obj.isAdditional == null){
//                         _this.append('<a class="com_m"  href="/customized/manufacturerComment/publish.html#'+codes[index]+'">评价制造商</a>')
//                     }else if(obj.isAdditional == true){
//                         _this.append('<a class="com_m"  href="/customized/manufacturerComment/addition.html#'+obj.id+'">追评制造商</a>')
//                     }
                    
//                 },
//                 error:function(msg){
//                     console.log(msg)
//                 }
//             })

//         })
        
// }
//设计商评价
// function commentBtnD(list){
//     console.log(list)
//     $('.wait_review_'+pageNum).each(function(index){
//         var _this = $(this).parent();
//         post({
//             url:'/customizeProduct/findReviews/',
//             data:{
//                 code:codes[index],
//                 type:'designer'
//             },
//             success:function(obj){
//                 console.log(obj);
//                 if(list[index].designer){
//                     if(obj.isAdditional == null){
//                         _this.append('<a class="com_d"  href="/customized/designerComment/publish.html#'+codes[index]+'">评价设计商</a>')
//                     }else if(obj.isAdditional == true){
//                         _this.append('<a class="com_d"  href="/customized/designerComment/addition.html#'+codes[index]+'">追评设计商</a>')
//                     }
//                 }
//             }
//         })
//     })
// }
function timeFn(timedata,code){
    if(timedata<=0){
        post({
            url:'customizeOrder/timeout',
            data:{code:code},
            success:function(){
                window.location.reload();
            }
        })
    }else{
        var date = new Date(timedata);
        h = date.getHours() + ':';
        m = date.getMinutes();  
       var timeD = h + m;  
    }
};

function operationFn(){
    $('section').delegate('.del_order','click',function(){
        var code = $(this).attr('name');
        var _this = $(this);
        $('.gat_alert>p').text('是否确定删除该订单？');
        $('.gat_background').removeClass('tab');
        $('.gat_alert').addClass('animated slideInDown');
        $('.gat_alert_cancel').click(function () {
            $('.gat_background').addClass('tab');
        });
        $('.gat_alert_affirm').click(function () {
            post({
                url:'/customizeOrder/delete',
                data:{"code":code},
                success:function(data){
                    $('.gat_background').addClass('tab');
                    _this.parent().parent().addClass('tab');
                }
            })
        });
    });

    $('section').delegate('.confirm_receiving','click',function(){
        var code = $(this).attr('name');
        $('.gat_alert>p').text('请收到货后，再确认收货！');
        $('.gat_background').removeClass('tab');
        $('.gat_alert').addClass('animated slideInDown');
        $('.gat_alert_cancel').click(function () {
            $('.gat_background').addClass('tab');
        });
        $('.gat_alert_affirm').click(function () {
            post({
                url:'/customizeOrder/confirmationReceipt',
                data:{"code":code},
                success:function(data){
                    window.location.reload();
                }
            })
        });
    });

    $('section').delegate('.pay_order','click',function(){
        var code = $(this).attr('name');
        setCookie("pay-content","customimprest"); //支付预付款、原厂包装
        setCookie("wait-code",code);
        setCookie("wait-amount",$(this).data("amount"));
        setCookie("step", "1");
        setCookie("productdetailgoback",encodeURIComponent(window.location.href));
        go('/mobileorder/selectpaytype.html');

    });

    $('section').delegate('.pay_orderB','click',function(){
        var code = $(this).attr('name');
        go('/customized/customizedorder/orderdetail/orderdetail.html?orderId='+code);
    });
    $('section').delegate('.find_stream','click',function(){
        var code = $(this).attr('name');
        go('/customized/customizedorder/logistics/index.html?orderId='+code);
    })
}
$('section').delegate('.aside_center>a','click',function(){
    localStorage.setItem('custOrderUrl',window.location.href)
})
