$(function () {
    var trackingNum = $.getUrlParam('orderId');
    getDataFn(trackingNum);

    $('.head-back').click(function(){
        window.history.go(-1);
    });

    // Fun
    function getDataFn(trackingNum) {
        post({
            url: '/express/order/data/' + trackingNum,
            success: function (data) {
                console.log(data)
                $('.d_message_status').text(data["order"]["orderName"]);
                $('.express_num').text(data["order"]["expressCode"]);
                $('.d_message>img').attr('src',SERVER_CONTEXT_PATH+"/load/picture/"+data["order"]["productUrl"]);
                $('.express_name').text(data["order"]["expressCompany"]);
                trackingMsg(JSON.parse(data["express"]));
            }
        })
    };

    function trackingMsg(json) {
        console.log(json)
        if (json["Traces"] == []) {
            $('aside').append(`<p style="text-align: center;margin-top: 1rem;">暂无物流信息</p>`);
        } else{
            for (var i = 0; i < json["Traces"].length; i++) {
                if (i == json["Traces"].length-1) {
                    var dot = `<img class="_dian1" src="./img/dian1.png" alt="">`;
                    var color = `_green`;
                } else {
                    var dot = `<img class="_dian" src="./img/dian.png" alt="">`;
                    var color = ` `;
                };
                $('aside').prepend(
                 `
                <div>
                    <div class="progressMsg">
                        <i class="_line"></i>
                        <i class="_dot">
                            ${dot}
                        </i>
                        <p class="${color}">${json["Traces"][i]["AcceptStation"]}</p>
                        <p class="${color}">${json["Traces"][i]["AcceptTime"]}</p>
                    </div>
                </div>
                `
                )
            }
        }
    }
})
