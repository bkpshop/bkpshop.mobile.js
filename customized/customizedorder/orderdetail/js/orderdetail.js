
var orderId = $.getUrlParam('orderId');
var addressId = '';
var amount = '0.00';
var orderStep = '';

$(function(){

    $('.add_cart').attr('name',orderId);

    getDataFn(orderId);

    operationFn();

    $('.go_back').click(function(){
        go(localStorage.getItem('custOrderUrl'))
    })


    // FUN
    function getDataFn(code){
        post({
            url:'/customizeOrder/detail',
            data:{'code':code},
            success:function(data){
                console.log(data);
                orderStep = data.orderStep;
                dealwithDataFn(data);
                chooseBtnFn(data["orderStep"]);
            }
        })
    };

    function dealwithDataFn(data){

        if(data.orderStep == "WAIT_FINAL_PAYMENT"){ //待付尾款
            if(data["memberAddressDTO"] == null){
                $('._address').text('请选择地址').css('text-align','center');
            }else{
                $('._contact').text(data["memberAddressDTO"]["consignee"]);
                $('._mobile').text(data["memberAddressDTO"]["mobile"]);
                $('._address').text(data["memberAddressDTO"]["addressDetail"]);
                addressId = data.memberAddressDTO.id;
            }
        }else if(data.orderStep == "WAIT_PAYING"){ //原厂包装待支付
            if(data["orderAddress"] != null){
                $('._contact').text(data["orderAddress"]["consignee"]);
                $('._mobile').text(data["orderAddress"]["mobile"]);
                $('._address').text(data["orderAddress"]["address"]);
                addressId = data.orderAddress.id;
            }else{
                $('._address').text('请选择地址').css('text-align','center');
            }
        }else{
            if(data["orderAddress"] == null){
                if(data["memberAddressDTO"] == null){
                    $('._address').text('请选择地址').css('text-align','center');
                }else{
                    $('._contact').text(data["memberAddressDTO"]["consignee"]);
                    $('._mobile').text(data["memberAddressDTO"]["mobile"]);
                    $('._address').text(data["memberAddressDTO"]["addressDetail"]);
                    addressId = data.memberAddressDTO.id;
                }
            }else{
                $('._contact').text(data["orderAddress"]["consignee"]);
                $('._mobile').text(data["orderAddress"]["mobile"]);
                $('._address').text(data["orderAddress"]["address"]);
                addressId = data.orderAddress.id;
            }
        }

        amount = data.realPrice;
        $('._productName').html(data["productName"]+'<span style="color: #666;font-size: .14rem;">     数量 x '+data['num']+'/箱</span>');
        $('._productUrl').attr('src',SERVER_CONTEXT_PATH+"/load/picture/"+data["productUrl"]);
        $('._manufacturer').text(data["manufacturer"]);
        var arry = data["manufacturerSpecification"].split('  ');
        for(var i=0; i<arry.length; i++){
            $('._manufacturerSpecification').append('<li>'+arry[i]+'</li>')
        }

        if(data["designer"] == null){
            $('.design_detail').addClass('tab');
        }else{
            $('.design_detail').removeClass('tab');
            //$('._designerProductUrl').attr('src',SERVER_CONTEXT_PATH+"/load/picture/"+data["designerProductUrl"]);
            $('._designer').text(data["designer"]);
            $('._designerSpecification').text(data["designerSpecification"]);
            $('._designerProductUrl').attr('src',SERVER_CONTEXT_PATH+"/load/picture/"+data["designerProductUrl"]);
        }

        //$(".order_detail .period").text((data.period?data.period:7));
        $('._productPrice').text(returnFloat(data["productPrice"]));
        if(data["packPrice"] == null){
            $('._packPrice').parent().addClass('tab');
            $('._packPriceName').addClass('tab');
        }else{
            $('._packPrice').text(returnFloat(data["packPrice"]));
        }
        $('._shippingFee').text(returnFloat(data["shippingFee"]));
        $('._totalPrice').text(returnFloat(data["totalPrice"]));
        if(data["advanceMoney"] == null){
            $('._advanceMoney').parent().addClass('tab');
            $('._advanceMoneyName').addClass('tab');
        }else{
            $('._advanceMoney').text(returnFloat(data["advanceMoney"]));
        }
    };

    function chooseBtnFn(status){
        var orderText;
        switch (status) {
            case 'WAIT_PAYMENT':
                $('.address_selected').addClass('tab');
                $('._payment').removeClass('tab');
                $('.pro_a').html(
                    `
                    <li class="li_lineA">包装制作</li>
                    <li class="li_lineA">开始生产</li>
                    <li class="li_lineA">商家发货</li>
                    <li class="li_lineA">交易完成</li>
                    `
                );
                break;
            case 'WAIT_FINAL_PAYMENT':
                $('._paymentB').removeClass('tab');
                $('._advanceMoneyName').text('尾款');
                $('.paied').text('买家已支付预付款开始包装制作');
                $('.pro_a').html(
                    `
                    <li class="li_lineB">包装制作</li>
                    <li class="li_lineA">开始生产</li>
                    <li class="li_lineA">商家发货</li>
                    <li class="li_lineA">交易完成</li>
                    `
                );
                $('.address_selected').click(function(){
                    go('/member/addressmanagement/selectaddress.html?customized=true&orderId='+orderId);
                });
                break;
            case 'WAIT_PAYING':
                $('._payment').removeClass('tab');
                $('.address_selected').click(function(){
                    $('.gat_alert>p').text('此订单不可更改地址！');
                    $('.gat_background').removeClass('tab');
                    //go('/member/addressmanagement/selectaddress.html?customized=true&orderId='+orderId);
                });
                $('.gat_alert_cancel,.gat_alert_affirm').click(function () {
                    $('.gat_background').addClass('tab');
                });
                $('.pro_a').html(
                    `
                    <li style="margin-left: 0.45rem;" class="li_lineA">开始生产</li>
                    <li class="li_lineA">商家发货</li>
                    <li class="li_lineA">交易完成</li>
                    `
                );
                break;
            case 'WAIT_DELIVERY':
                $('._lookFor').removeClass('tab');
                $('.paied').text('商家已发货，请注意查收');
                if($('.design_detail').hasClass('tab')){
                    $('.pro_a').html(
                        `
                        <li style="margin-left: 0.45rem;" class="li_lineB">开始生产</li>
                        <li class="li_lineB">商家发货</li>
                        <li class="li_lineA">交易完成</li>
                        `
                    );
                }else{
                    $('.pro_a').html(
                        `
                        <li class="li_lineB">包装制作</li>
                        <li class="li_lineB">开始生产</li>
                        <li class="li_lineB">商家发货</li>
                        <li class="li_lineA">交易完成</li>
                        `
                    );
                }
                
                break;
            case 'WAIT_SEND':
                $('.paied').text('买家已支付尾款开始生产，等待商家发货');
                if($('.design_detail').hasClass('tab')){
                    $('.pro_a').html(
                        `
                        <li style="margin-left: 0.45rem;" class="li_lineB">开始生产</li>
                        <li class="li_lineA">商家发货</li>
                        <li class="li_lineA">交易完成</li>
                        `
                    );
                }else{
                    $('.pro_a').html(
                        `
                        <li class="li_lineB">包装制作</li>
                        <li class="li_lineB">开始生产</li>
                        <li class="li_lineA">商家发货</li>
                        <li class="li_lineA">交易完成</li>
                        `
                    );
                }
                break;
            case 'COMPLETE':
                $('._service').removeClass('tab');
                $('.paied').text('交易完成，如有问题请申请售后');
                if($('.design_detail').hasClass('tab')){
                    $('.pro_a').html(
                        `
                        <li style="margin-left: 0.45rem;" class="li_lineB">开始生产</li>
                        <li class="li_lineB">商家发货</li>
                        <li class="li_lineB">交易完成</li>
                        `
                    );
                }else{
                    $('.pro_a').html(
                        `
                        <li class="li_lineB">包装制作</li>
                        <li class="li_lineB">开始生产</li>
                        <li class="li_lineB">商家发货</li>
                        <li class="li_lineB">交易完成</li>
                        `
                    );
                }
                break;
            case 'CLOSED':
                break;
            default:
                break;
        }
    };
    
    function operationFn(){
        $('.confirm_receiving').click(function(){
            var code = $(this).parent().attr('name');
            $('.gat_alert>p').text('请收到货后，再确认收货！');
            $('.gat_background').removeClass('tab');
            $('.gat_alert').addClass('animated slideInDown');
            $('.gat_alert_cancel').click(function () {
                $('.gat_background').addClass('tab');
            });
            $('.gat_alert_affirm').click(function () {
                post({
                    url:'/customizeOrder/confirmationReceipt',
                    data:{"code":code},
                    success:function(data){
                        window.location.reload();
                    }
                });
            });
        });

        $('._payment>input').click(function(){
            var code = $(this).parent().attr('name');
            setCookie("pay-content","customimprest"); //支付预付款、原厂包装
            setCookie("wait-code",code);
            setCookie("wait-amount", amount);
            setCookie("step", "1");
            setCookie("productdetailgoback",encodeURIComponent(window.location.href));
            if(orderStep == "WAIT_PAYMENT"){  //预付款待支付状态
                setCookie("addressid", "");
                go('/mobileorder/selectpaytype.html');
            }else{
                if(addressId){
                    setCookie("addressid", addressId);
                    go('/mobileorder/selectpaytype.html');
                }else{
                    $(".no_adrees").removeClass("tab");
                    return false;
                }
            }
        });

        $('._paymentB>input').click(function(){
            var code = $(this).parent().attr('name');
            setCookie("pay-content","customizefinal"); //支付尾款
            setCookie("wait-code",code);
            setCookie("wait-amount", amount);
            setCookie("step", "1");
            setCookie("productdetailgoback",encodeURIComponent(window.location.href));
            if(addressId){
                setCookie("addressid", addressId);
                go('/mobileorder/selectpaytype.html');
            }else{
                $(".no_adrees").removeClass("tab");
                return false;
            }
        });

        $('.find_stream').click(function(){
            var code = $(this).parent().attr('name');
            go('/customized/customizedorder/logistics/index.html?orderId='+code);
        });

        $(".adrees_affirm").click(function(){
            go('/member/addressmanagement/selectaddress.html?customized=true&orderId='+orderId);
        });

        $(".adrees_cancel").click(function(){
            $(".no_adrees").addClass("tab");
        });
    }
});