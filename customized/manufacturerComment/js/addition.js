$(function(){
	var _href = window.location.hash;
    var _index = _href.split("#")[1];console.log(_index)
    var _grade = 0;
    var _imgs = [];
    var len = 0
    $('.up_img').change(function(){
        $('.img_show').each(function(){
           len =  $(this).find('.img_1').length;

        })
        console.log(len)
        if(len <= 2 ){
           uploadDoc('.up_img','/upload/member/review','.img_show');
        }else{
            alertText('最多添加三张图片')
            _imgs = []
        }
    })
    // 返回
    $(".go-back").click(function(){
        window.history.back();
    });
    // grade();
    publish();
    
	  // 上传图片
    function uploadDoc(dom1, url,dom2) {
        var fd = new FormData();
        fd.append("file", 1);
        var type = $(dom1).get(0).files[0].type;
        var size = $(dom1).get(0).files[0].size;
        fd.append("file", $(dom1).get(0).files[0]);
        if(size<=6*1024*1024){
            if(type == 'image/jpeg' || type == 'image/png'){
                $.ajax({
                    url: SERVER_CONTEXT_PATH + url,
                    type: 'POST',
                    data: fd,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        // _imgs = _imgs.concat(data.data) 
                        // console.log(_imgs.toString())
                        $(dom2).append('<div class="img_1"><img data-src="'+data.data+'" src="'+SERVER_CONTEXT_PATH + "/load/picture/" + data.data+'"/><span></span></div>')
                        
                    },
                    error:function(msg){
                        alertText('上传失败！')
                    }
                })
            }else{
                alertText('请上传图片格式。')
            }
        }else{
            alertText('图片大小限制为3M。')
        }
        
    };
   function alertText(mesg){
         $('.gat_background').removeClass('tab');
          $('.gat_info').text(mesg)
          $('.gat_alert_affirm').click(function(){
              $('.gat_background').addClass('tab')
          })
    }

    // 发布追评
    function publish(){
    	$('.menu').click(function(){
            $('.img_1').each(function(){
                _imgs = _imgs.concat($(this).find('img').data('src'))
            });console.log(_imgs)
        	var _data ={
        			id:_index,
        			imageUrl:_imgs.toString()?_imgs.toString():null,
        			description:$('.text_content').val()
        		};console.log(_data)
            if($('.text_content').val()){
          		post({
          			url:'/customizeOrder/saveManufacturerReviews',
          			data:_data,
          			success:function(data){
          				console.log(data)

                 			 go('/customize/orderlist/index.html')

          			},
          			error:function(msg){
          				console.log(msg)
          			}
    		      })
            }else{
                alertText('追评内容不能为空！')
                _imgs = []
            }
        })
    }
    //图片删除
    $('.img_show').delegate('span','click',function(){
        $(this).parent().remove();
        _imgs= []
    })
})