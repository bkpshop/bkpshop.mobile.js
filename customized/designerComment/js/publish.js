$(function(){
	var _href = window.location.hash;
    var _index = _href.split("#")[1];
    var _grade = 0;
    var _imgs = [];
    
    $('.up_img').change(function(){
        if(_imgs.length <3){
    	   uploadDoc('.up_img','/upload/member/review','.img_show');
        }else{
            alertText('最多添加三张图片')
        }
    })
    grade();
    publish();
    // 返回
    $(".go-back").click(function(){
        window.history.back();
    });
      // 上传图片
    function uploadDoc(dom1, url,dom2) {
        var fd = new FormData();
        fd.append("file", 1);
        var type = $(dom1).get(0).files[0].type;
        var size = $(dom1).get(0).files[0].size;
        fd.append("file", $(dom1).get(0).files[0]);
        if(size<=6*1024*1024){
            if(type == 'image/jpeg' || type == 'image/png'){
                $.ajax({
                    url: SERVER_CONTEXT_PATH + url,
                    type: 'POST',
                    data: fd,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        _imgs = _imgs.concat(data.data) 
                        console.log(_imgs.toString())
                        $(dom2).append('<img src="'+SERVER_CONTEXT_PATH + "/load/picture/" + data.data+'"/>')
                        
                    },
                    error:function(msg){
                        alertText('上传失败！')
                    }
                })
            }else{
                alertText('请上传图片格式。')
            }
        }else{
            alertText('图片大小限制为3M。')
        }
        
    };
   function alertText(mesg){
         $('.gat_background').removeClass('tab');
          $('.gat_info').text(mesg)
          $('.gat_alert_affirm').click(function(){
              $('.gat_background').addClass('tab')
          })
    }
    //评分
    function grade(){
    	$('.grade li').click(function(){
    		var index_g = $(this).index();
    			_grade = index_g +1;
    		$('.grade li').each(function(index){
    			if(index<=index_g){
    				$(this).find("img").attr('src','img/star.png')
    			}else{
    				$(this).find("img").attr('src','img/star1.png')
    			}
    		});
    		if(_grade == 1){$('.text_g').text('1分失望')}
			else if(_grade == 2){$('.text_g').text('2分不满')}
			else if(_grade == 3){$('.text_g').text('3分一般')}
			else if(_grade == 4){$('.text_g').text('4分不错')}
			else if(_grade == 5){$('.text_g').text('5分惊喜')}
    	})
    }
    // 发布评价
    function publish(){
    	$('.menu').click(function(){
    	var _data ={
    			code:_index,
    			grade:_grade,
    			imageUrl:_imgs.toString()?_imgs.toString():null,
    			description:$('.text_content').val()
    		};console.log(_data)
        if($('.text_content').val()&&_grade != 0){
    		post({
    			url:'/customizeOrder/saveDesignerReviews',
    			data:_data,
    			success:function(data){
    				console.log(data)

           			 window.history.back();
        

    				// window.location.href = '/usercomment/index.html#';
    			},
    			error:function(msg){
    				console.log(msg)

    			}
    		})
        }else{
            if($('.text_content').val() == ''){
                alertText('评论内容不能为空！')
                
            }else{
                alertText('请选择评分！')
               
            }
        }

    })
    }
})