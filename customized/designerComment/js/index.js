$(function(){
	var _isImage = null,_isAdditional = null;
	var pageNum = 1;
	var _href = window.location.hash;
    var _index = _href.split("#")[1];
	commentList(pageNum);
	condition();
	iScroll();
	// 返回
	$(".go-back").click(function(){
        window.history.back();
    });


	function commentList(pageNum){
		console.log(_isImage,_isAdditional)
		post({
		url:'/customizeProduct/reviewsPage',
		data:{
			page:pageNum,
			rows:5,
			productId:_index,
			type:'designer',
			isImage:_isImage,
			isAdditional:_isAdditional,
		},
		success:function(data){
			console.log(data);
			// $('.comment').empty();
			var lists = data.reviewsList;
			if(lists.length){
				$('.no-comment').addClass('tab')
				var _html = '';
				for(var i in lists){
					
					var img_url = lists[i].avatar?SERVER_CONTEXT_PATH+'/load/picture/'+lists[i].avatar:"/common/img/avatar_default.jpg"
					var _grade = lists[i].grade;
					_html += '<div class="user clear"><img src="'+img_url+'" alt="">'
					_html += '<span class="phone">'+lists[i].phone+'</span>'
					if(_grade == 1){
						_html += '<div class="star right"><ul class="grade clear"><li><img src="img/star.png" alt=""></li><li><img src="img/star1.png" alt=""></li><li><img src="img/star1.png" alt=""></li><li><img src="img/star1.png" alt=""></li><li><img src="img/star1.png" alt=""></li></ul></div>'
					}else if(_grade == 2){
						_html += '<div class="star right"><ul class="grade clear"><li><img src="img/star.png" alt=""></li><li><img src="img/star.png" alt=""></li><li><img src="img/star1.png" alt=""></li><li><img src="img/star1.png" alt=""></li><li><img src="img/star1.png" alt=""></li></ul></div>'
					}else if(_grade == 3){
						_html += '<div class="star right"><ul class="grade clear"><li><img src="img/star.png" alt=""></li><li><img src="img/star.png" alt=""></li><li><img src="img/star.png" alt=""></li><li><img src="img/star1.png" alt=""></li><li><img src="img/star1.png" alt=""></li></ul></div>'
					}else if(_grade == 4){
						_html += '<div class="star right"><ul class="grade clear"><li><img src="img/star.png" alt=""></li><li><img src="img/star.png" alt=""></li><li><img src="img/star.png" alt=""></li><li><img src="img/star.png" alt=""></li><li><img src="img/star1.png" alt=""></li></ul></div>'
					}else if(_grade == 5){
						_html += '<div class="star right"><ul class="grade clear"><li><img src="img/star.png" alt=""></li><li><img src="img/star.png" alt=""></li><li><img src="img/star.png" alt=""></li><li><img src="img/star.png" alt=""></li><li><img src="img/star.png" alt=""></li></ul></div>'
					}
					_html +='</div>'
					_html += '<p class="single"><span>'+getLocalTime(lists[i].createTime)+'</span>'+lists[i].productSpecification+'</p>'
					_html += '<div class="comment-detail">'+lists[i].description+'</div>'
					if(lists[i].imageUrl){
						var _img = lists[i].imageUrl.split(',');
						_html += '<div class="comment-img clear">'
						for(var k in _img){
							_html += '<img class="img_com" data-img="'+_img[k]+'" src="'+SERVER_CONTEXT_PATH+'/load/picture/'+_img[k]+'" alt="">'
						}
						_html += '</div>'
					}
					if(lists[i].additionalTime){
						var add_day = lists[i].additionalDay>0?lists[i].additionalDay+"天后":"当天"
						_html += '<div class="addition"><span>用户'+add_day+'追评</span>'
						_html += '<div class="comment-detail">'+lists[i].additionalDescription+'</div>'
						if(lists[i].additionalImageUrl){
							var img_add = lists[i].additionalImageUrl.split(',');
							_html += '<div class="comment-img clear">'
							for(var j in img_add){
								_html += '<img class="img_com" data-img="'+img_add[j]+'" src="'+SERVER_CONTEXT_PATH+'/load/picture/'+img_add[j]+'" alt="">'
							}
							_html += '</div>'
						}
						_html += '</div>'
					}
				}
				$('.comment').append(_html)
			}else{
				if(pageNum == 1){
					$('.no-comment').removeClass('tab')
				}else{
					$('.no-comment').addClass('tab')
				}
				
			}
		},
		error:function(msg){
			console.log(msg)
		}

	})
	}
		//图片放大
	
	imglist();
	
	function imglist(){

			$('.comment').delegate('.img_com','click',function(){
				var imgs = [$(this).data('img')]
				zoomImg($('.img_com'),imgs)
			})
	}
	//筛选
	function condition(){
		$('.condition li').click(function(){
			$(this).addClass('active').siblings().removeClass('active');
			$('.comment').empty();
		})
		$('.condition li:nth-child(1)').click(function(){
			_isImage = null;
			_isAdditional = null;
			pageNum = 1;
			commentList(pageNum);
		});
		$('.condition li:nth-child(2)').click(function(){
			_isImage = 1;
			_isAdditional = null;
			pageNum = 1;
			commentList(pageNum);
		});
		$('.condition li:nth-child(3)').click(function(){
			_isImage = null;
			_isAdditional = 1;
			pageNum = 1;
			commentList(pageNum);
		});
	}
	// 上拉刷新函数
    function iScroll() {
        function iScrollA() {
            var scrollTop = 0,
                bodyScrollTop = 0,
                documentScrollTop = 0;
            if (document.body) {
                bodyScrollTop = document.body.scrollTop;
            }
            if (document.documentElement) {
                documentScrollTop = document.documentElement.scrollTop;
            }
            scrollTop = (bodyScrollTop - documentScrollTop > 0) ? bodyScrollTop : documentScrollTop;
            return scrollTop;
        }

        function iScrollB() {
            var windowHeight = 0;
            if (document.compatMode == "CSS1Compat") {
                windowHeight = document.documentElement.clientHeight;
            } else {
                windowHeight = document.body.clientHeight;
            }
            return windowHeight;
        }

        function iScrollC() {
            var scrollHeight = 0,
                bodyScrollHeight = 0,
                documentScrollHeight = 0;
            if (document.body) {
                bodyScrollHeight = document.body.scrollHeight;
            }
            if (document.documentElement) {
                documentScrollHeight = document.documentElement.scrollHeight;
            }
            scrollHeight = (bodyScrollHeight - documentScrollHeight > 0) ? bodyScrollHeight : documentScrollHeight;
            return scrollHeight;
        }
        window.onscroll = function () {
            showGoTop($('body').scrollTop())
            if (iScrollC() == iScrollB() + iScrollA()) {
                pageNum += 1;
                commentList(pageNum);
                return pageNum;
            }
        }
    };
    // 时间轴处理
    function getLocalTime(nS) {   
    	Date.prototype.toLocaleString = function() {
          return this.getFullYear() + "/" + (this.getMonth() + 1) + "/" + this.getDate();
    	};  
       return new Date(parseInt(nS)).toLocaleString();      
    }     

})