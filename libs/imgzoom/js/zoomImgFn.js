/**
 * Created by Scoot on 2017/6/20.
 */

function zoomImg(dom,imgList){

    if(isWeiXin() == true){
        var imgsObj = $('.info img');
        var imgs = new Array();
        for(var i = 0; i < imgList.length; i++){
            imgs.push('http://'+window.location.host+'/load/picture/'+imgList[i]);
        }

        // dom.on('click',function(){
            WeixinJSBridge.invoke('imagePreview', {
                'current': $(this).attr('src'),
                'urls': imgs
            });
        // });
    }else{
        setCookie("imgzoom", imgList);
        // dom.click(function(){
            go("/libs/imgzoom/imgzoom.html");
        // });
    }

}

//判断是否是微信
function isWeiXin(){
    var ua = window.navigator.userAgent.toLowerCase();
    if(ua.match(/MicroMessenger/i) == 'micromessenger'){
        return true;
    }else{
        return false;
    }
}