$(function () {
    $(".go-back").on("click", function () {
        go("/index.html");
    })

    var _hash = window.location.hash.substring(1);
    console.log(_hash);

    var id = _hash;

    getArticle();

    function getArticle() {
        post({
            url : "/product/supermarket/list",
            success : function(data){
                console.log(data);
                var _html = "";
                for(var i = 0;i<data.length;i++){
                    _html += '<a class="list" href="/productlist/discountlist.html#'+data[i].id+'">';
                    _html += '<img src="'+SERVER_CONTEXT_PATH+'/load/picture/'+data[i].imageUrl+'" alt="">';
                    _html += '<div class="context">';
                    _html += '<p class="single">'+data[i].name+'<span>￥'+data[i].minPrice+'</span>元起</p>';
                    _html += '</div></a>';
                }
                $(".list-contain").append(_html);
            },
            error : function(msg){
                console.log("错误");
            }
        })        
    }
})