/**
 * Created by Scoot on 2017/4/14.
 */

var articleId = '';
var _pageNum = 1;
var login = false;
var uid = '';
var d_id = '';
var _this='';

var isApp = false;
var equipmentType = "common";

if (/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)) {
    equipmentType= "ios";
} else if (/(Android)/i.test(navigator.userAgent)) {
    equipmentType = "android";
} else {
    equipmentType = "common";
}

$(function(){

    articleId = $.getUrlParam("articleId");

    post({
        url : "/isLogin",
        success : function(data){
            if(!data){
                login = false;
                getListData();
            }else{
                login = true;
                post({
                    url:"/member/uid",
                    success:function(data){
                        uid = data;
                        getListData();
                    }
                });
            }
        }
    });

    //点击确认删除评论
    $(".no_delete .delete_affirm").click(function(){
        if(uid != ""){
            post({
                url:"/article/comment/"+d_id+"/delete",
                success:function(data){
                    _this.remove();
                    $(".no_delete").addClass("tab");
                    if($(".main").html() == ""){
                        $(".container .none").removeClass("tab");
                        $(".container .main").addClass("tab");
                    }
                }
            })
        }
    });
    $(".no_delete .delete_cancel").click(function(){
        $(".no_delete").addClass("tab");
    });

    $(".footer .btn").click(function(){
        publish();
    });

    $(".go-back").click(function(){
        go(getCookie("comment_back"));
    });

    //为了解决ios软键盘挡住输入框问题
    var bfscrolltop = document.body.scrollTop;//获取软键盘唤起前浏览器滚动部分的高度
    $(".f-input").focus(function(){//在这里‘input.inputframe’是我的底部输入栏的输入框，当它获取焦点时触发事件

        if(equipmentType == "ios"){
            try{
                ifApp();
                isApp = true;
            }catch (e){
                isApp = false;
            }
        }
        if(equipmentType == "ios" && isApp == true){
            $(".footer").css("position","static");
            $(".container").css("padding-bottom","0");
            $(".none").css("min-height","5rem");
            $(".main").css("min-height","5.2rem");
            interval = setInterval(function(){//设置一个计时器，时间设置与软键盘弹出所需时间相近
                document.body.scrollTop = document.body.scrollHeight;//获取焦点后将浏览器内所有内容高度赋给浏览器滚动部分高度
            },500);
        }else if(equipmentType == "ios" && isApp == false){
            interval = setInterval(function(){//设置一个计时器，时间设置与软键盘弹出所需时间相近
                document.body.scrollTop = document.body.scrollHeight;//获取焦点后将浏览器内所有内容高度赋给浏览器滚动部分高度
            },500);
        }
    }).blur(function(){//设定输入框失去焦点时的事件
        if(equipmentType == "ios" && isApp == true){
            clearInterval(interval);//清除计时器
            document.body.scrollTop = bfscrolltop;//将软键盘唤起前的浏览器滚动部分高度重新赋给改变后的高度
            $(".footer").css("position","fixed");
            $(".container").css("padding-bottom","0.72rem");
        }else if(equipmentType == "ios" && isApp == false){
            clearInterval(interval);//清除计时器
            document.body.scrollTop = bfscrolltop;//将软键盘唤起前的浏览器滚动部分高度重新赋给改变后的高度
        }
    });

    /*setInterval(function () {
        if (document.activeElement.className.indexOf('inputcontent') >= 0) {
            document.activeElement.scrollIntoViewIfNeeded();
        }
    }, 300);*/

});

function getListData(){//获取列表数据
    post({
        url:"/article/"+articleId+"/commentList",
        data:{
            pageNo: _pageNum,
            pageSize: "20",
            sorts: [{property: 'createTime', order: 'DESC'}]
        },
        success:function(data){
            //console.log(data)
            dealListData(data.list);
        },
        requestError:function(msg){
            console.log(msg)
        }
    })
}
function dealListData(list){    //处理列表数据
    var _html = '';
    for(var i=0;i<list.length;i++){
        _html += '<div class="list clear">';
        if(list[i]["avatar"]){
            _html += '<div class="left"><img src="'+SERVER_CONTEXT_PATH+'/load/picture/'+list[i]["avatar"]+'"></div>';
        }else{
            _html += '<div class="left"><img src="../common/img/avatar_default.jpg"></div>';
        }
        _html += '<div class="left  content">';
        _html += '<h5>'+list[i]["name"]+'</h5>';
        _html += '<time datetime="'+list[i]["createTime"]+'">'+list[i]["createTime"]+'</time>';
        _html += '<p>'+list[i]["content"]+'</p>';
        if(uid == list[i]["userId"]){
            _html += '<div class="delete" data-id="'+list[i]["id"]+'">删除</div>';
        }
        _html += '</div></div>';
        if(i<list.length-1){
            _html += '<div class="b-line"></div>';
        }
    }

    $(".main").append(_html);

    if(_pageNum == 1&&list.length==0){
        $(".container .none").removeClass("tab");
        $(".container .main").addClass("tab");
    }else{
        $(".container .none").addClass("tab");
        $(".container .main").removeClass("tab");
    }

    $(".content .delete").click(function(){
        d_id = $(this).data("id");
        _this = $(this).parent().parent();
        $(".no_delete").removeClass("tab");
    })
}

function publish(){ //发布评论
    if(login == true){
        var content = $(".f-input").val().trimSpace();
        if(content){
            post({
                url:"/article/"+uid+"/comment/save",
                data:{
                    referId:articleId,
                    content:content
                },
                success:function(data){
                    $(".main").html("");
                    _pageNum = 1;
                    $(".f-input").val("");
                    getListData();
                }
            })
        }
    }else{
        go("/login/index.html");
    }
}

isScroll();
function isScroll(){
    window.onscroll = function () {
        //监听事件内容
        if(getScrollHeight() == getWindowHeight() + getDocumentTop()){
            //当滚动条到底时,这里是触发内容
            //异步请求数据,局部刷新dom
            _pageNum++;
            setTimeout(function() {
                getListData();
            }, 500);
        }
    };
}
// 上拉加载
//文档高度
function getDocumentTop() {
    var scrollTop = 0, bodyScrollTop = 0, documentScrollTop = 0;
    if (document.body) {
        bodyScrollTop = document.body.scrollTop;
    }
    if (document.documentElement) {
        documentScrollTop = document.documentElement.scrollTop;
    }
    scrollTop = (bodyScrollTop - documentScrollTop > 0) ? bodyScrollTop : documentScrollTop;
    return scrollTop;
}

//可视窗口高度
function getWindowHeight() {
    var windowHeight = 0;
    if (document.compatMode == "CSS1Compat") {
        windowHeight = document.documentElement.clientHeight;
    } else {
        windowHeight = document.body.clientHeight;
    }
    return windowHeight;
}

//滚动条滚动高度
function getScrollHeight() {
    var scrollHeight = 0, bodyScrollHeight = 0, documentScrollHeight = 0;
    if (document.body) {
        bodyScrollHeight = document.body.scrollHeight;
    }
    if (document.documentElement) {
        documentScrollHeight = document.documentElement.scrollHeight;
    }
    scrollHeight = (bodyScrollHeight - documentScrollHeight > 0) ? bodyScrollHeight : documentScrollHeight;
    return scrollHeight;
}


function iosApp(str){
    isApp = true;
    return true;
}