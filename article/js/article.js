/**
 * Created by Scoot on 2017/3/11.
 */
var a_url = window.location.href;
var a_title = '';
var a_desc = '';
var a_img = '';
var a_summary = '火匣网';
var hash = window.location.href;
var _id = $.getUrlParam("id");
var inviter = $.getUrlParam("share");
// var member = '';

var equipmentType = "common";

if (/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)) {
    equipmentType= "ios";
} else if (/(Android)/i.test(navigator.userAgent)) {
    equipmentType = "android";
} else {
    equipmentType = "common";
}

setCookie("comment_back",a_url);
setCookie("productdetailgoback",encodeURIComponent(a_url));

$(function(){

    init();

    //是否登录
    // post({
    //      url: "/isLogin",
    //      success: function (data) {
    //          // console.log(data)
    //          if(data){
    //              uid()
    //          }
    //      }
    // });
    // function uid(){
    //      post({
    //          url:'/member/uid',
    //          success:function(data){
    //              //console.log(data);
    //              member = data;
    //          }
    //      })
    // }

    $(".go-back").click(function(){
        go(localStorage.getItem("articleGoBack"));
    });

    $(".menu").on("click",function(){
        $("#bottom").toggleClass("tab");
    });
    //分享生活
    if(inviter){
        post({
            url:'/isLogin',
            success:function(data){
                if(data){
                    $('.qr_code').addClass('tab');
                    $('.qr_code_2').removeClass('tab')
                }else{
                    $('.qr_code').removeClass('tab');
                    $('.qr_code_2').addClass('tab');   
                    useInfo(inviter);
                    $('.img_2').click(function(){
                        go('/register/index.html?inviter='+inviter)
                    });
                }
            }
        })
        
    }else{
        $('.qr_code').addClass('tab');
        $('.qr_code_2').removeClass('tab')
    }
});
function useInfo(id){
    
    post({
        url:'/invitee/info/'+id,
        success:function(data){
            console.log(data);
            if(data.avatar){
                $('.img_1').attr('src',''+SERVER_CONTEXT_PATH +'/load/picture/'+data.avatar+'');
            }
            $('.name_fx').html("“"+data.phone+"”")
        }
    })
}
function init(){

    //读取文章
    post({
        url:"/article/product/list/"+_id,
        success:function(data){
            dealData(data);
            $('.article>p>img').css({
                'display':'block',
                'max-width':'100%'
            });
        }
    });

    //读取用户二维码
    // if(inviter){
    //     if(hash.indexOf("&share=") != -1 && hash.indexOf("&share=&") == -1){
    //         inviter=hash.match(/&share=\d+/g)[0].replace(/&share=/g,"");
    //     }
    //     post({
    //         url:"/article/"+_id+"/inviter/"+inviter+"/qrcode",
    //         success:function(data){
    //             $(".qr_code img").attr("src",SERVER_CONTEXT_PATH + "/load/picture/"+data);
    //         }
    //     });
    // }else{
    //     $(".qr_code").css("display","none");
    // }

    //点击点赞
    $(".footer .vote").click(function(){
        var voteNum = parseInt($(this).text());
        $(this).text(voteNum+1);
        $.tipsBox({//点赞动画
            obj: $(this),
            str: "+1",
            callback: function () {
                post({
                    url:"/article/"+_id+"/vote",
                    success:function(data){
                        //voteCounts();
                    }
                })
            }
        });
        niceIn($(this));
    });

    //点击分享
    $(".footer .share,.img_3").click(function(){
        post({
            url:"/article/"+_id+"/share",
            success:function(data){
                shareCounts();
            }
        });

        if(equipmentType == "ios"){
            try{
                var message = {
                    'url' : window.location.href,
                    'title' : '火匣生活 定制食品倡导者',
                    'Imageurl' : a_img,
                    'text' : a_title
                };
                window.webkit.messageHandlers.share.postMessage(message);
                //share(window.location.href,'火匣生活',a_img,a_title);
            }catch (e){
                if(isWeiXin() == false){
                    $(".wrap_share,.share_cover").removeClass("tab");
                }else{
                    $(".share_cover,.pay_container").removeClass("tab");
                }
            }
        }else if(equipmentType == "android"){
            try{
                contact.share(window.location.href,'火匣生活 定制食品倡导者',a_img,a_title);
            }catch (e){
                if(isWeiXin() == false){
                    $(".wrap_share,.share_cover").removeClass("tab");
                }else{
                    $(".share_cover,.pay_container").removeClass("tab");
                }
            }
        }else{
            if(isWeiXin() == false){
                $(".wrap_share,.share_cover").removeClass("tab");
            }else{
                $(".share_cover,.pay_container").removeClass("tab");
            }
        }

    });

    //点击评论
    $(".footer .comment").click(function(){
        go("comment.html?articleId="+_id);
    });

    //点击底部分享
    $(".wrap_share .bottom").click(function(){
        $(".pay_container").removeClass("tab");
    });

    $(".share_cover,.pay_container").click(function(){
        $(".wrap_share,.share_cover,.pay_container").addClass("tab");
    });

    voteCounts();
    shareCounts();
    commentCounts();
}

function dealData(data){
    var _img = '';
    for(var j=0;j<data.imageList.length;j++){
        _img += '<div class="swiper-slide"><img src="'+SERVER_CONTEXT_PATH +'/load/picture/'+data.imageList[j]+'"></div>';
    }
    $(".swiper-wrapper").html(_img);
    var mySwiper = new Swiper ('.swiper-container', {
        pagination: '.swiper-pagination',
        loop: true//,//循环
        //autoplay: 3000//播放间隔
    });

    var _list = '<h2 class="title h_title">'+data.title+'</h2>';
    if(data.productList[0]["productId"] != null){
        for(var i=0;i<data.productList.length;i++){
            _list += '<div class="article">'+data.productList[i]["tweetsContent"]+'</div>';
            _list += '<div class="good"><h2 class="title">'+(i+1)+'.'+data.productList[i]["name"]+'</h2>';
            _list += '<img src="'+SERVER_CONTEXT_PATH +'/load/picture/'+data.productList[i]["productImageUrl"]+'"><div>';
            _list += '<p class="price left">￥'+returnFloat(data.productList[i]["minPrice"])+'起</p>';
            //_list += '<p class="price original-price left">￥119.99</p>';
            //_list += '<a class="look right add_cart" href="javascript:;" data-id="'+data.productList[i]["id"]+'">加入购物车</a>';
            if(inviter != ''){
                _list += '<a class="look right" href="/customize/newDesign.html?productid='+data.productList[i]["productId"]+'&inviter='+inviter+'">我要定制</a>';
            }else{
                _list += '<a class="look right" href="/customize/newDesign.html?productid='+data.productList[i]["productId"]+'">我要定制</a>';
            }
            _list += '</div></div>';
        }
    }else{
        _list += '<div>'+data.productList[0]["tweetsContent"]+'</div>';
    }

    $(".info").html(_list);
    $("head").append('<meta name="keywords" content="'+data.articleDigest+'"/><meta name="description" content="'+data.articleDigest+'"/>');
    //addCart();

    a_url = "https://"+window.location.host+'/article/index.html?id='+_id+"&share="+inviter;
    a_title = $(".h_title").text();
    a_img = "https://"+window.location.host+"/load/picture/"+data.imageList[0];
    a_summary = data["title"];
    $("title").text(a_title);
    var config = {
        url:a_url,
        title:a_title,
        desc:a_desc,
        img:a_img,
        img_title:"",
        from:"火匣网",
        summary:a_summary
    };
    var share_obj = new nativeShare('nativeShare',config);
}

function voteCounts(){
    post({//点赞数
        url:"/article/"+_id+"/voteCounts",
        success:function(data){
            $(".footer .vote").text(data);
        }
    })
}

function shareCounts(){
    post({//转发数
        url:"/article/"+_id+"/shareCounts",
        success:function(data){
            $(".footer .share").text(data);
        }
    })
}

function commentCounts(){
    post({//评论数
        url:"/article/"+_id+"/commentCounts",
        success:function(data){
            $(".footer .comment").text(data);
        }
    });
}


if(isWeiXin() == false){
    $(".wrap_share .bottom").remove();
}
//判断是否是微信
function isWeiXin(){
    var ua = window.navigator.userAgent.toLowerCase();
    if(ua.match(/MicroMessenger/i) == 'micromessenger'){
        return true;
    }else{
        return false;
    }
}


