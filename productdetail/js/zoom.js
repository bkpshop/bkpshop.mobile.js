/**
 * Created by Scoot on 2017/6/13.
 */

$(function(){
    var imgList = getCookie("imgzoom").split(',');
    console.log(imgList)
    var _img = '<div class="swiper-wrapper ">';
    for(var i=0;i<imgList.length;i++){
        _img += '<div class="swiper-slide "><img src="'+SERVER_CONTEXT_PATH+'/load/picture/'+imgList[i]+'"></div>';
    }
    _img += '</div><div class="swiper-pagination"></div>';

    $(".zoom-container").html(_img).css("left","0").click(function(){
        //$(this).css("left","20rem");
    });

    var mySwiper2 = new Swiper ('.zoom-container', {
        pagination: '.swiper-pagination',
        paginationElement : 'span',
        loop: true//,//循环
        //autoplay: 30000//播放间隔
    });

    $(".go-back,.zoom-container").click(function(){
        window.history.back();
    });
});