
$(function(){

    // var _hash=window.location.hash.substring(1);
    // console.log(_hash);
    // if(_hash == 1 || _hash == 2 || _hash == 4){
    //     $("header a").on("click",function(){
    //         go("/index.html");
    //     });
    // }else{
    //     $("header a").on("click",function(){
    //         go("/kinds/kinds.html");
    //     });
    // };

    setCookie("productdetailgoback",encodeURIComponent(window.location.href));
    $("header span").on("click",function(){
        $("section #bottom").toggleClass("tab");
    });

    // $("header a").on("click",function(){
    //     go("/index.html");
    // });
});

// 列表数据
$(function(){

    var pageNum = 1;
    getNewGoods();    
    function getNewGoods() { //新品推荐
        _data = {
            page: {
                pageNo: pageNum,
                pageSize: 20
            }
        }
        post({
            url: "/product/new/recommend",
            data: _data,
            success: function (data) {
                console.log(data)
                var _html = "";
                for (var i = 0; i < data.list.length; i++) {
                    _html += '<li><a href="/productdetail/index.html#'+data.list[i].productId+'">';
                    _html += '<img src="'+SERVER_CONTEXT_PATH+'/load/picture/'+data.list[i].imageUrl+'" alt="">';
                    _html += '<b class="single">'+data.list[i].keywords+'</b>';
                    _html += '<p class="single">'+data.list[i].name+'</p>';
                    //_html += '<span class="original">超市价￥'+returnFloat(data.list[i].originalPrice)+'</span>';
                    _html += '<p class="time-limit">￥'+returnFloat(data.list[i].retailPrice)+'</p>';                    
                    _html += '</a></li>';
                }
                $(".list>ul").append(_html);
            },
            error: function (msg) {
                console.log("错误");
            }
        })
    }

    // 上拉加载
    //文档高度 
    function getDocumentTop() { 
        var scrollTop = 0, bodyScrollTop = 0, documentScrollTop = 0; 
        if (document.body) { 
            bodyScrollTop = document.body.scrollTop; 
        } 
        if (document.documentElement) { 
            documentScrollTop = document.documentElement.scrollTop; 
        } 
        scrollTop = (bodyScrollTop - documentScrollTop > 0) ? bodyScrollTop : documentScrollTop; 
        return scrollTop; 
    } 
    
    //可视窗口高度 
    function getWindowHeight() { 
        var windowHeight = 0; 
        if (document.compatMode == "CSS1Compat") { 
            windowHeight = document.documentElement.clientHeight; 
        } else { 
            windowHeight = document.body.clientHeight; 
        } 
        return windowHeight; 
    } 
    
    //滚动条滚动高度 
    function getScrollHeight() { 
        var scrollHeight = 0, bodyScrollHeight = 0, documentScrollHeight = 0; 
        if (document.body) { 
            bodyScrollHeight = document.body.scrollHeight; 
        } 
        if (document.documentElement) { 
            documentScrollHeight = document.documentElement.scrollHeight; 
        } 
        scrollHeight = (bodyScrollHeight - documentScrollHeight > 0) ? bodyScrollHeight : documentScrollHeight; 
        return scrollHeight; 
    } 
    window.onscroll = function () {  
            showGoTop($('body').scrollTop())
        //监听事件内容 
        if(getScrollHeight() == getWindowHeight() + getDocumentTop()){ 
            //当滚动条到底时,这里是触发内容 
            //异步请求数据,局部刷新dom 
            //pageNum++;
            //console.log(pageNum);
            //getNewGoods();         
            
        } 
    }    

});