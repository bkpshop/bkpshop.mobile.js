
$(function(){

    var _hash=window.location.hash.substring(1);
    console.log(_hash);
    if(_hash == 1 || _hash == 2 || _hash == 4){
        $("header a").on("click",function(){
            go("/index.html");
        });
    }else{
        $("header a").on("click",function(){
            go("/kinds/kinds.html");
        });
    };

    setCookie("productdetailgoback",encodeURIComponent(window.location.href));
    $("header span").on("click",function(){
        $("section #bottom").toggleClass("tab");
    });

    // $("header a").on("click",function(){
    //     go("/kinds/kinds.html");
    // });
});

// 列表数据
$(function(){
    var pageNum = 1;
    function getData(flag){
        var hash = window.location.hash.substring(1);
        var _data = {
            page:pageNum,
            rows:8,
            categoryId:hash
        };
        //console.log(_data);
        post({
            url:"/product/list",
            data:_data,
            success:function(data){
                console.log(data);
                if(flag){
                    if(data.productList.length == 0){
                        $(".list-none").removeClass("tab");
                        $(".list").addClass("tab");
                    }else{
                        $(".list").removeClass("tab");
                        $(".list-none").addClass("tab");
                    }
                }
                var _list = '';
                for(var i = 0;i<data.productList.length;i++){
                    _list += '<li class="swiper-slide">';
                    _list += '<a href="/productdetail/index.html#' + data.productList[i].id + '">';
                    _list += '<img src="' + SERVER_CONTEXT_PATH + '/load/picture/' + data.productList[i].imageUrl + '" alt="">';
                    _list += '<p class="single">' + data.productList[i].keywords + '</p>';
                    _list += '<h4 class="single">' + data.productList[i].name + '</h4>';                    
                    _list += '<span>￥' + returnFloat(data.productList[i].retailPrice) + '</span>';
                    _list += '</a></li>';
                }
                $("header .head").text(data.cname);                
                $(".list ul").append(_list);

                $("img.lazy").lazyload({//图片懒加载
                    threshold : 300
                });
            }
        })
    }
    getData(true);

    // 上拉加载
    //文档高度 
    function getDocumentTop() { 
        var scrollTop = 0, bodyScrollTop = 0, documentScrollTop = 0; 
        if (document.body) { 
            bodyScrollTop = document.body.scrollTop; 
        } 
        if (document.documentElement) { 
            documentScrollTop = document.documentElement.scrollTop; 
        } 
        scrollTop = (bodyScrollTop - documentScrollTop > 0) ? bodyScrollTop : documentScrollTop; 
        return scrollTop; 
    } 
    
    //可视窗口高度 
    function getWindowHeight() { 
        var windowHeight = 0; 
        if (document.compatMode == "CSS1Compat") { 
            windowHeight = document.documentElement.clientHeight; 
        } else { 
            windowHeight = document.body.clientHeight; 
        } 
        return windowHeight; 
    } 
    
    //滚动条滚动高度 
    function getScrollHeight() { 
        var scrollHeight = 0, bodyScrollHeight = 0, documentScrollHeight = 0; 
        if (document.body) { 
            bodyScrollHeight = document.body.scrollHeight; 
        } 
        if (document.documentElement) { 
            documentScrollHeight = document.documentElement.scrollHeight; 
        } 
        scrollHeight = (bodyScrollHeight - documentScrollHeight > 0) ? bodyScrollHeight : documentScrollHeight; 
        return scrollHeight; 
    } 
    window.onscroll = function () {  
            showGoTop($('body').scrollTop())
        //监听事件内容 
        if(getScrollHeight() == getWindowHeight() + getDocumentTop()){ 
            //当滚动条到底时,这里是触发内容 
            //异步请求数据,局部刷新dom 
            pageNum++;
            //console.log(pageNum);
            getData(false);          
            
        } 
    }    

});