$(function () {
    setCookie("productdetailgoback", encodeURIComponent(window.location.href));

    //获取列表数据
    var _hash = window.location.hash.substring(1);
    console.log(_hash);

    var id = _hash;
    var pageSize = 10;
    getlistData();

    function getlistData() {
        var _data = {
            page: {
                pageNo: 1,
                pageSize: pageSize
            },
            id: id
        }
        post({
            url: "/product/supper/product/list",
            data: _data,
            success: function (data) {
                console.log(data);
                var _list = '';
                $("header>.banner>img").attr("src",SERVER_CONTEXT_PATH+'/load/picture/'+data.supermarket.imageUrl);
                $("section>aside>p").html(data.supermarket.introduce.replace(/-/g,'<br/>'));
                for(var i = 0;i<data.list.length;i++){
                    _list += '<li><a href="/productdetail/index.html#'+data.list[i].productId+'">';
                    _list += '<img src="'+SERVER_CONTEXT_PATH+'/load/picture/'+data.list[i].imageUrl+'" alt="">';
                    _list += '<b class="single">'+data.list[i].keywords+'</b>';
                    _list += '<p class="single">'+data.list[i].name+'</p>';
                    //_list += '<span class="original">超市价￥'+returnFloat(data.list[i].originalPrice)+'</span>';
                    _list += '<p class="time-limit">￥'+returnFloat(data.list[i].retailPrice)+'</p>';                    
                    _list += '</a></li>';
                }
                $(".list ul").append(_list);
                $("img.lazy").lazyload({
                    threshold: 300
                });
            }
        })
    }


    // 上拉加载
    //文档高度 
    function getDocumentTop() {
        var scrollTop = 0,
            bodyScrollTop = 0,
            documentScrollTop = 0;
        if (document.body) {
            bodyScrollTop = document.body.scrollTop;
        }
        if (document.documentElement) {
            documentScrollTop = document.documentElement.scrollTop;
        }
        scrollTop = (bodyScrollTop - documentScrollTop > 0) ? bodyScrollTop : documentScrollTop;
        return scrollTop;
    }

    //可视窗口高度 
    function getWindowHeight() {
        var windowHeight = 0;
        if (document.compatMode == "CSS1Compat") {
            windowHeight = document.documentElement.clientHeight;
        } else {
            windowHeight = document.body.clientHeight;
        }
        return windowHeight;
    }

    //滚动条滚动高度 
    function getScrollHeight() {
        var scrollHeight = 0,
            bodyScrollHeight = 0,
            documentScrollHeight = 0;
        if (document.body) {
            bodyScrollHeight = document.body.scrollHeight;
        }
        if (document.documentElement) {
            documentScrollHeight = document.documentElement.scrollHeight;
        }
        scrollHeight = (bodyScrollHeight - documentScrollHeight > 0) ? bodyScrollHeight : documentScrollHeight;
        return scrollHeight;
    }
    window.onscroll = function () {
        showGoTop($('body').scrollTop())
        //监听事件内容 
        if (getScrollHeight() == getWindowHeight() + getDocumentTop()) {
            //当滚动条到底时,这里是触发内容 
            //异步请求数据,局部刷新dom 
            pageNum++;
            getlistData();

        }
    }

});
