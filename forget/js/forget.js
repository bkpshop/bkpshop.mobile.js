$(function(){
	//用户名的失去焦点事件
    var _phone;
    $(".phone>input").blur(function() {
        if (!(/^1[34578]\d{9}$/.test($(this).val()))&&($(this).val().length !== 0)) {
            $('.phone').addClass("dl_error").siblings().removeClass('reg_error');
        } 
    });
    //用户名的获得焦点事件，去除警告状态
    $(".phone>input").focus(function() {
        $('.phone').removeClass("dl_error").next().removeClass('reg_error');
    });
    
    //验证码框
    $(".yzm>input").blur(function(){
        if($(this).val().length ==0){
            $(this).removeClass("reg_error").nextAll().removeClass("reg_error");
        }

    });

    $(".yzm>input").focus(function(){
        $(this).removeClass("reg_error").nextAll().removeClass("reg_error");

    })

    // 返回上一级
	$(".d_header>a").on("click",function(){
		window.history.back();
	});
    
    //验证码按钮的点击事件
    var flag = true;
    var authCode = '';
    $(".sendCode").on("click",function(){
        if(!(/^1[34578]\d{9}$/.test($(".phone>input").val()))){
            alertBox("请输入正确的手机号");
            return false;

        }
        else if($(".yzm>input").val().length == 0){
            alertBox("请输入验证码");
            return false;
        }
        // if ($(".information").children().hasClass("reg_error")||$(".phone>input").val().length == 0||$('.yzm>input').val().length == 0){
        //     return false;
        // };
        else{
        post({
            url:'/captcha/'+$('.yzm>input').val(),
            success:function(data){console.log(data)
                if(data == true){
                    if (flag) {
                        flag = false;
                        var count = 50;
                        $(".sendCode").html("<i>"+count+"</i>s重新获取");
                        $(".code button").attr("disabled",true);
						$(".code button").css({background:"#ddd",color:"#666",border:0});
                        //第三方接口
                        post({
                            url:"/forgetPassword/phone/"+$(".phone>input").val()+"/verificationCode",
                            success:function(data){
                                console.log(data)
                            }
                        });
                        //定时器，防止连续发送短信
                        var timer = setInterval(function(){
                            count--;
                            if(count == 0){
                                clearInterval(timer);
                                flag = true;
                                $(".sendCode").text("发送验证码");
                                $(".code button").attr("disabled",false);
								$(".code button").css({background:"#ffebd9",color:"#ec7a17",border:"1px solid #ec7a17"});
                                return;
                            }
                            $(".sendCode").text(count+"s重新获取");
                        },1000);
                    }
                }else{
                    // $('.alertBox,.alertBoxA').removeClass('tab');
                //  $('.alertBoxText').text("验证码错误");
                alertBox('验证码错误')

                }
            }
        })
        }
    });
    //点击下一步
    $('.next_first').click(function(e){
        e.preventDefault();
        if(!(/^1[34578]\d{9}$/.test($(".phone>input").val()))){
            alertBox("请输入正确的手机号");
            return false;

        }
        else if($(".yzm>input").val().length == 0){
            alertBox("请输入验证码");
            return false;
        }
        else if($('.code>input').val().length == 0){
            alertBox("请输入手机验证码");
            return false;
        }
        // if ($(".information").children().hasClass("reg_error")||$(".phone>input").val().length == 0||$('.yzm>input').val().length == 0||$('.code>input').val().length == 0){
        //     return false;
        // };
        else{ 
            post({
                url:"/phoneJudge",
                data:{phone:$(".phone>input").val(),code:$(".code>input").val()},
                success:function(data){
                    if(data){
                        console.log(data)
                        _phone = data;
                        go('indexTwo.html#'+_phone+'')
                        
                    }
                },
                error:function(msg){
                    alertBox(msg)
                    //alert(msg);
                }
            });
        }
    })
    //验证码
    $("#captchaImg").attr("src", ""+SERVER_CONTEXT_PATH+"/captcha.jpg?t="+new Date().getTime());
    // 换一换
    $(".getYZM").click(function() {
        $("#captchaImg").attr("src", ""+SERVER_CONTEXT_PATH+"/captcha.jpg?t="+new Date().getTime());
    });
     //弹窗函数
    function alertBox(msg){
        $('#alertBox,.alertBoxA').removeClass('tab');
        $('.alertBoxText').text(msg);
        $('#alertBox').off().on('click',function(){
            $('#alertBox,.alertBoxA').addClass('tab');
        });
        $('.alertBoxA').off().on('click',function(){
            $('#alertBox,.alertBoxA').addClass('tab');
        })
    }
    /*第一步结束*/

})