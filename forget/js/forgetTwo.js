$(function(){
	var _href = window.location.hash;
    var _phone = _href.split("#")[1];console.log(_phone);
    /*第二步开始*/
    $(".newPassword>input").blur(function() {
        if (!(/^(?![\d]+$)(?![a-zA-Z]+$)(?![^\da-zA-Z]+$).{6,20}$/.test($(this).val()))&&($(this).val().length !== 0)) {
            $('.newPassword').addClass("reg_error").siblings().removeClass('reg_error');
            // $('#banner .information .newPassword .err_t').css('display','block')
            }
    });
    //密码框的获得焦点事件，去除警告状态
    $(".newPassword>input").focus(function() {
        $('.newPassword').removeClass("reg_error").next().removeClass('reg_error');
        // $('#banner .information .newPassword .err_t').css('display','none')

    });
    //再次确认密码框失去焦点
    $(".rePassword>input").blur(function(){
        if ($(this).val() === $(".newPassword>input").val()) {
            $(".rePassword").removeClass("reg_error").siblings().removeClass("reg_error");
        } else {
            $(".rePassword").addClass("reg_error");
            // $('#banner .information .rePassword .err_r').css('display','block')
        }
    });
    //再次确认密码框获得焦点
    $(".rePassword>input").focus(function() {
        $('.rePassword').removeClass("reg_error").next().removeClass('reg_error');
        // $('#banner .information .rePassword .err_r').css('display','none')

    });

    // 返回上一级
	$("#banner>a").on("click",function(){
		window.history.back();
	});
    
    $(".next_two").on("click",function(e){
        e.preventDefault();
        if(!(/^(?![\d]+$)(?![a-zA-Z]+$)(?![^\da-zA-Z]+$).{6,20}$/.test($(".newPassword>input").val()))){
            alertBox("密码格式错误",'');
            return false;
        }else if($(".rePassword>input").val()!=$(".newPassword>input").val()){
            alertBox("密码不一致",'');
            return false;
        }
        // if ($(".information").children().hasClass("reg_error")||$(".newPassword>input").val().length == 0||$(".rePassword>input").val().length == 0){
        //     return false;
        // };
        // console.log(_phone);
        else{
            post({
                url:"/retrievePassword",
                data:{phone:_phone,password:Base64.encode($(".newPassword>input").val())},
                success:function(data){
                	console.log(data)
                	alertBox('密码修改成功！','/login/index.html');

                },
                error:function(msg){
                    console.log(msg)
                	alertBox(msg,'')
                
                }
            });
        } 
    });
    function alertBox(msg,url){
        $('#alertBox,.alertBoxA').removeClass('tab');
        $('.alertBoxText').text(msg);
        $('#alertBox').off().on('click',function(){
            $('#alertBox,.alertBoxA').addClass('tab');
            if(url){
                go(url);
            };
            
        });
        $('.alertBoxA').off().on('click',function(){
            $('#alertBox,.alertBoxA').addClass('tab');
            if(url){
                go(url);
            };
        })
    }
})