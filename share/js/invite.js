
var _title = "火匣生活 定制食品倡导者";
var _desc = "点我轻松找到你的专属生活";
var _link = window.location.href;
var _imgUrl = "https://"+window.location.host+"/common/img/share-img.png";

var equipmentType = "common";

$(function(){

    var userId = $.getUrlParam('id');
    post({
        url:'/inviter/'+userId+'/qrcode',
        success:function(data){
            $('.barcode>img').attr('src',SERVER_CONTEXT_PATH + "/load/picture/" + data);
            
        },
        requestError:function(msg){
            
        }
    });

    // var h=$(document).height();
    // var h2=$("header").height();
    // $("body").height(h-h2);

    if (/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)) {
        equipmentType= "ios";
    } else if (/(Android)/i.test(navigator.userAgent)) {
        equipmentType = "android";
    } else {
        equipmentType = "common";
    }

    //返回上一级
    $(".head-back").click(function(){
        var _url = decodeURIComponent(getCookie("productdetailgoback"));
        _url = _url?_url:"/index.html";
        go(_url);
    });
});

var config = {
    url:window.location.href,
    title:_title,
    desc:_desc,
    img:_imgUrl,
    img_title:"",
    from:"火匣生活",
    summary:"火匣生活 与众不同的生活方式"
};
var share_obj = new nativeShare('nativeShare',config);

$(".share_btn").click(function(){
    if(equipmentType == "ios"){
        try{
            calliOSShare();
            //share(window.location.href,_title,_imgUrl,_desc);
        }catch (e){
            if(isWeiXin() == false){
                $(".wrap_share,.share_cover").removeClass("tab");
            }else{
                $(".share_cover,.pay_container").removeClass("tab");
            }
        }
    }else if(equipmentType == "android"){
        try{
            contact.share(window.location.href,_title,_imgUrl,_desc);
        }catch (e){
            if(isWeiXin() == false){
                $(".wrap_share,.share_cover").removeClass("tab");
            }else{
                $(".share_cover,.pay_container").removeClass("tab");
            }
        }
    }else{
        if(isWeiXin() == false){
            $(".wrap_share,.share_cover").removeClass("tab");
        }else{
            $(".share_cover,.pay_container").removeClass("tab");
        }
    }

});

$(".share_cover,.pay_container").click(function(){
    $(".wrap_share,.share_cover,.pay_container").addClass("tab");
});

$(".wrap_share .bottom").click(function(){
    $(".pay_container").removeClass("tab");
});

if(isWeiXin() == false){
    $(".wrap_share .bottom").remove();
}else{
    $(".qzone").css("display","none");
}


function calliOSShare() {
    var message = {
        'url' : window.location.href,
        'title' : _title,
        'Imageurl' : _imgUrl,
        'text' : _desc
    };
    window.webkit.messageHandlers.share.postMessage(message);
}

//判断是否是微信
function isWeiXin(){
    var ua = window.navigator.userAgent.toLowerCase();
    if(ua.match(/MicroMessenger/i) == 'micromessenger'){
        return true;
    }else{
        return false;
    }
}

document.onreadystatechange = subSomething;//当页面加载状态改变的时候执行这个方法.
function subSomething()
{
    if(document.readyState == "complete" ){
        $(".share_btn").css("display","block");
    } //当页面加载状态

}