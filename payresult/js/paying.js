/**
 * Created by Scoot on 2017/7/17.
 */

$(function(){
    // 返回上一级
    $(".header .go-back").on("click",function(){

        if (document.referrer === '') { //当无上级页面时  返回首页。
            window.location.href = "/index.html";
        }else{
            window.history.back();
        }
    });

});