 $(function(){

	var _inviter = '',_target = '';
    var url = location.search; //获取url中"?"符后的字串  

    var theRequest = new Object();  
    if (url.indexOf("?") != -1) {  
        var str = url.substr(1);  
        strs = str.split("&");  
        for(var i = 0; i < strs.length; i ++) {  
           theRequest[strs[i].split("=")[0]]=unescape(strs[i].split("=")[1]);  
        }  
	    _inviter = theRequest.inviter != null?theRequest.inviter:'';
	    _target = theRequest.target != null?theRequest.target:'';
	}

     var psw = getCookie("register_password")||'';
     var rpsw = getCookie("register_repassword")||'';
     //$(".password>input").val(psw);

     //$(".repassword>input").val(rpsw);

// console.log(_inviter,_target,11)
	// 用户名的失去焦点事件
	$(".phone>input").blur(function() {
        if (!(/^1[34578]\d{9}$/.test($(this).val()))&&($(this).val().length !== 0)) {
            $('.phone').addClass("reg_error").siblings().removeClass('reg_error');
            alertBox('手机格式错误');
            // $('#banner .information .phone .err').css('display','block')
            };
        //判断用户是否已存在
            post({
                url:"/account/"+$(".phone>input").val()+"/isExist",
                error:function(result){
                    $('.phone').addClass("reg_error").siblings().removeClass('reg_error');
                    // $('#alertBox,.alertBoxA').removeClass('tab');
                // 	$('.alertBoxText').text("该用户已存在！");
                alertBox('该用户已存在！')
                }

            })
			
	});
	// 用户名的获得焦点事件，去除警告状态
	$(".phone>input").focus(function() {
		$('.phone').removeClass("reg_error").next().removeClass('reg_error');
		// $('#banner .information .phone .err').css('display','none');
		// $('#banner .information .phone .err_h').css('display','none')
	});
	//密码框失去焦点事件
	$(".password>input").blur(function() {
        setCookie("register_password",$(this).val().trimSpace());
		if (!(/^(?![\d]+$)(?![a-zA-Z]+$)(?![^\da-zA-Z]+$).{6,20}$/.test($(this).val()))&&($(this).val().length !== 0)) {
			$('.password').addClass("reg_error").siblings().removeClass('reg_error');
			// $('#banner .information .password .err_t').css('display','block')
			}
	}).val(psw);
	//密码框的获得焦点事件，去除警告状态
	$(".password>input").focus(function() {
		$('.password').removeClass("reg_error").next().removeClass('reg_error');
		// $('#banner .information .password .err_t').css('display','none')

	});
	//再次确认密码框失去焦点
	$(".repassword>input").blur(function(){
        setCookie("register_repassword",$(this).val().trimSpace());
		if ($(this).val() === $(".password>input").val()) {
			$(".repassword").removeClass("reg_error");
		} else {
			$(".repassword").addClass("reg_error");
			// $('#banner .information .repassword .err_r').css('display','block')
		}
	}).val(rpsw);
	//再次确认密码框获得焦点
	$(".repassword>input").focus(function() {
		$('.repassword').removeClass("reg_error").next().removeClass('reg_error');
		// $('#banner .information .repassword .err_r').css('display','none')

	});
	//验证码框
	$(".yzm>input").blur(function(){
		if($(this).val().length ==0){
			$(this).removeClass("reg_error").nextAll().removeClass("reg_error");
		}

	});

	$(".yzm>input").focus(function(){
		$(this).removeClass("reg_error").nextAll().removeClass("reg_error");

	})

	// 返回上一级
	$(".header .go-back").on("click",function(){

        if (document.referrer === '') { //当无上级页面时  返回首页。
            window.location.href = "/index.html";
        }else{
            window.history.back();
        }
	});
	
	//验证码按钮的点击事件
	var flag = true;
	// if(flag){$(".code button").css("background-color","#f0bf00");}else{$(".code button").css("background-color","#ededed");}
	var authCode = '';
	$('.sendCode').attr('disabled',true);
	$('.yzm>input').keyup(function(){
		post({
			url:'/captcha/' + $(this).val(),
			success:function(data){
				//console.log(data);
				if(data){
					$(".code button").css({background:"#ffebd9",color:"#ec7a17",border:"1px solid #ec7a17"});
				}else{
					$(".code button").css({background:"#ddd",color:"#666",border:0});
				}
			}
		})
		
	});

     $(".repassword>input").blur(function(){
         if($(".password>input").val() != $(this).val()){
             alertBox("密码不一致");
             return false;
         }
     });
	
	$(".code>button").on("click",function(e){
		e.preventDefault();
		if(!(/^1[34578]\d{9}$/.test($('.phone>input').val()))){
			alertBox("手机格式错误");
			return false;
		}
		else if(!(/^(?![\d]+$)(?![a-zA-Z]+$)(?![^\da-zA-Z]+$).{6,20}$/.test($('.password>input').val()))){
			alertBox("密码格式错误");
			return false;
		}
		else if($(".repassword>input").val() != $(".password>input").val()){
			alertBox("密码不一致");
			return false;
		}
		else if ($('.yzm>input').val().length == 0){
			alertBox("请输入验证码");
			return false;
		}
		else if($(".information").children().hasClass("reg_error")){
			return false;
		}
		else{
			// $(".code button").css("background-color","#f0bf00");
		post({
			url:'/captcha/'+$('.yzm>input').val(),
			success:function(data){
				if(data == true){
					if (flag) {
						flag = false;
						var count = 50;
						$(".sendCode").html("<i>"+count+"</i>s重新获取");
						$(".code button").attr("disabled",true);

						$(".code button").css({background:"#ddd",color:"#666",border:0});
						//第三方接口
						post({
							url:"/register/phone/"+$(".phone>input").val()+"/verificationCode",
							success:function(data){
								console.log(data)
							}
						});
						//定时器，防止连续发送短信
						var timer = setInterval(function(){
							count--;
							if(count == 0){
								clearInterval(timer);
								flag = true;
								$(".sendCode").text("发送验证码");
								$(".code button").attr("disabled",false);
								$(".code button").css({background:"#ffebd9",color:"#ec7a17",border:"1px solid #ec7a17"});
								return;
							}
							$(".sendCode").text(count+"s重新获取");
						},1000);
					}
				}else{
					// $('#alertBox,.alertBoxA').removeClass('tab');
			 	// 	$('.alertBoxText').text("验证码错误");
			 	alertBox('验证码错误')

				}
			}
		})
		}
	});

	//注册按钮点击事件
	$(".agree").on("click",function(e){
		e.preventDefault();
		$('.information input').blur();
		if(!(/^1[34578]\d{9}$/.test($('.phone>input').val()))){
			alertBox("手机格式错误");
			return false;
		}
		else if(!(/^(?![\d]+$)(?![a-zA-Z]+$)(?![^\da-zA-Z]+$).{6,20}$/.test($('.password>input').val()))){
			alertBox("密码格式错误");
			return false;
		}
		else if($(".repassword>input").val() != $(".password>input").val()){
			alertBox("密码不一致");
			return false;
		}
		else if ($('.yzm>input').val().length == 0){
			alertBox("请输入验证码");
			return false;
		}
		else if($(".hx-explain>label").hasClass("unselect")){
			alertBox("请同意注册协议");
			return false;
		}
		else if($(".information").children().hasClass("reg_error")){
			return false;
		}else{
			// if($('.phone>input').val().length !==0 &&$('.password>input').val().length !==0 &&$('.repassword>input').val().length !==0 &&$('.code>input').val().length !==0 &&$('.yzm>input').val().length !==0 && (/^(?![\d]+$)(?![a-zA-Z]+$)(?![^\da-zA-Z]+$).{6,20}$/.test($(".password>input").val()))&& ($('.repassword>input').val() == $('.password>input').val())){
				
				//向后台传入数据
				var username = $(".phone>input").val();
				var password = $(".password>input").val();
				var code = $(".code>input").val();
				var data = {
							username:username,
							password:Base64.encode(password),
							code:code,
							inviteUID:_inviter,
							target:_target
						};
 				console.log(data);
				post({
					url:"/doRegister",
					data:data,
					success:function(url){
						localStorage.setItem('logIn','success');
						// go(url)
						// $('#alertBox,.alertBoxA').removeClass('tab');
						// $('.alertBoxText').text("恭喜你，成为火匣创客的一员！");
						// alertBoxGo = url
						go(url)
						},
					error:function(msg){
						// $('#alertBox,.alertBoxA').removeClass('tab');
						// $('.alertBoxText').text(msg);
						// console.log(msg)
						alertBox(msg)
						}
				})
				
			// }else{
				// alert("请输入正确的注册信息")
				// $('#alertBox,.alertBoxA').removeClass('tab');
			 // 	$('.alertBoxText').text("请输入正确的注册信息");
			//  alertBox('请输入正确的注册信息')
			// }
		}	
		// }
	})
	
	$("#captchaImg").attr("src", ""+SERVER_CONTEXT_PATH+"/captcha.jpg?t="+new Date().getTime());
	// 换一换
	$(".getYZM").click(function() {
		$("#captchaImg").attr("src", ""+SERVER_CONTEXT_PATH+"/captcha.jpg?t="+new Date().getTime());
	});
 	
 })
 //弹窗函数
 function alertBox(msg){
	$('#alertBox,.alertBoxA').removeClass('tab');
 	$('.alertBoxText').text(msg);
 	$('#alertBox').off().on('click',function(){
 		$('#alertBox,.alertBoxA').addClass('tab');
 	});
 	$('.alertBoxA').off().on('click',function(){
 		$('#alertBox,.alertBoxA').addClass('tab');
 	})
}

	var explain_flag = true;

	$(".hx-explain>label").click(function(){
		var that = $(this)
		if(explain_flag){
			that.addClass("select").removeClass("unselect");
			explain_flag = false;
		}else{
			that.addClass("unselect").removeClass("select");
			explain_flag = true;
		}	
	});