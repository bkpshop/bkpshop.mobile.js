$(function(){
    // var pageNum = 1;
    // 判断是否有hash
    // var hash = window.location.hash;
    // if(hash){
    //     $('.recentSea').addClass('tab');
    //     $('.filtrate,.searchList').removeClass('tab');
    //     var hashId = hash.substring(1);
    //     $('.searchBorder').val(decodeURI(hashId));
    //     var searchData = decodeURI(hashId);
    //     dataRequestFn(searchData);
    // }else{
    //     $('.filtrate,.searchList').addClass('tab');
    //     $('.recentSea').removeClass('tab');
    // };

    $(".message_alert_affirm").click(function(){
        $(".message_alert").addClass("tab");
    });

    // 搜索
    $('.searchBtn').click(function(){
        var searchData = $('.searchBorder').val();
        if(searchData){
            // dataRequestFn(searchData);
            searchData = encodeURI(encodeURI(searchData));
            go('/customize/index.html?searchData='+searchData);
            // window.location.reload();
        }else{
            $(".adrees_alert p").text("搜索内容不能为空！");
            $(".message_alert").removeClass("tab");
            //alert('搜索内容不能为空！');
        }
    });

    // 数据请求函数
    // function dataRequestFn(searchData){
    //     var data = {
    //             productName:searchData,
    //             page:pageNum,
    //             rows:6
    //         };
    //     post({
    //         url:'/product/search',
    //         data:data,
    //         success:function(data){
    //             console.log(data);
    //             if(data["total"] == 0){
    //                 $('.recentSea,.filtrate,.searchList').addClass('tab');
    //                 $('.resultsEmpty').removeClass('tab');
    //             }else{
    //                 $('.recentSea,.resultsEmpty').addClass('tab');
    //                 $('.filtrate,.searchList').removeClass('tab');
    //                 dataListFn(data["productList"]);
    //                 iScroll();
    //             }
    //         }
    //     });
    // };

    // 数据列表
    // function dataListFn(data){
    //     for(var i=0; i<data.length; i++){
    //         $('.searchList').append(
    //             '<aside><a href="/productdetail/index.html#'+data[i]["id"]+'">'
    //             +'<img src="'+SERVER_CONTEXT_PATH+'/load/picture/'+data[i]["imageUrl"]+'" alt=""><span class="single">'+data[i]["name"]+'</span>'
    //             +'<b class="single"><i>￥</i>'+returnFloat(data[i]["retailPrice"])+'</b></a></aside>'
    //         )
    //     };
    // };

    // 最近搜索
    post({
        url:'/product/searchHistoryLogList',
        success:function(data){
            for(var i=0; i<data.length; i++){
                if(data[i]["name"]){
                    $('.recentSeaList').append(
                        '<li><a target="_self" href="/customize/index.html?searchData='+encodeURI(encodeURI(data[i]["name"]))+'">'+data[i]["name"]+'</a></li>'
                    );
                }
            };
            $('.recentSeaList>li>a').click(function(){
                go($(this).attr("href"));
                // window.location.reload();    
            });
        }
    });

    // 菜单键切换
    var i = 0;
    $('.header_menu').click(function () {
        if (i == 0) {
            $('#bottom').removeClass('tab');
            i = 1;
        } else {
            $('#bottom').addClass('tab');
            i = 0;
        };
    });

   //确认删除按钮（弹框）
    $('.recentSeaDel').click(function(){
        // $('.gat_background').removeClass('tab');
        $('.gat_background').removeClass('tab');
        $('.gat_alert').addClass('animated slideInDown');
    });
    $('.gat_alert_cancel').click(function(){
        $('.gat_background').addClass('tab');
    });
    $('.gat_alert_affirm').click(function(){
        post({
            url:'/product/deleteSearchHistoryLog',
            success:function(data){
                console.log(data);
                go('./search.html')
            }
        })
    });

    // 滚动条到底加载函数
    function iScroll(){
        function iScrollA() {
            var scrollTop = 0, bodyScrollTop = 0, documentScrollTop = 0;
            if (document.body) {
                bodyScrollTop = document.body.scrollTop;
            }
            if (document.documentElement) {
                documentScrollTop = document.documentElement.scrollTop;
            }
            scrollTop = (bodyScrollTop - documentScrollTop > 0) ? bodyScrollTop : documentScrollTop;
            return scrollTop;
        }

        function iScrollB() {
            var windowHeight = 0;
            if (document.compatMode == "CSS1Compat") {
                windowHeight = document.documentElement.clientHeight;
            } else {
                windowHeight = document.body.clientHeight;
            }
            return windowHeight;
        }

        function iScrollC() {
            var scrollHeight = 0, bodyScrollHeight = 0, documentScrollHeight = 0;
            if (document.body) {
                bodyScrollHeight = document.body.scrollHeight;
            }
            if (document.documentElement) {
                documentScrollHeight = document.documentElement.scrollHeight;
            }
            scrollHeight = (bodyScrollHeight - documentScrollHeight > 0) ? bodyScrollHeight : documentScrollHeight;
            return scrollHeight;
        }
        window.onscroll = function () {
            if(iScrollC() == iScrollB() + iScrollA()){
                pageNum +=1;
                var searchData = $('.searchBorder').val();
                dataRequestFn(searchData);
                return pageNum;
            }
        }
    }

})