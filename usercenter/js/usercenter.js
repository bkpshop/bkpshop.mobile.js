$(function () {
    // 是否登陆
    //goLogin();
    // 账户信息
    post({
        url: "/isLogin",
        success: function (data) {
            if (!!data) {
                init();
                $('.message').removeClass('tab');
            } else {
                $('.d_head>p').text("未登录");
                $('.sec_logout').addClass('tab');
                $('.message').addClass('tab');
            }
        }
    });

    function init() {
        post({
            url: '/member/avatar',
            success: function (data) {
                if (data) {
                    if (getImg(data)) {
                        $('.d_head>img').attr('src', SERVER_CONTEXT_PATH + "/load/picture/" + data);
                    } else {
                        $('.d_head>img').attr('src', data);
                    }
                };
            }
        });
        post({
            url: '/member/phone/blur',
            success: function (data) {
                $('.d_head>p').text(data);
            }
        });
        post({
            url : '/message/unRead/count',
            success : function (data) {
                console.log(data);
                if((data.memberMsgNum + data.systemMsgNum) == 0 ){
                    $('.message>i').addClass('tab');
                }else{
                    $('.message>i').removeClass('tab');
                    $('.message>i').html(data.memberMsgNum + data.systemMsgNum);
                }
            },
            error : function (msg) {
                console.log('错误');
            }
        })
        // orderStatusFn('WAIT_PAYING', '.sec_order_numA');
        // orderStatusFn('WAIT_DELIVERY', '.sec_order_numB');
    }
    orderStatusFn();
    // 订单状态
    function orderStatusFn() {
        post({
            url: '/customizeOrder/statusCount',
            
            success: function (data) {
                console.log(data);
                if(data[0].total != 0){$('.sec_order_numA').removeClass('tab').html(data[0].total)}
                if(data[1].total != 0){$('.sec_order_numB').removeClass('tab').html(data[1].total)}
                if(data[2].total != 0){$('.sec_order_numC').removeClass('tab').html(data[2].total)}
                if(data[3].total != 0){$('.sec_order_numD').removeClass('tab').html(data[3].total)}  
                    
                   
                // if (data == 0) {
                //     $(dom).addClass('tab');
                // } else {
                //     $(dom).removeClass('tab');
                //     $(dom).text(data);
                // }
            }
        });
    }

    // $('.sec_logout').append(
    //     `
    //     <form class='tab' action="${SERVER_CONTEXT_PATH}/logout" method="post">
    //         <input type="submit" value="退出登录">
    //     </form>
    //     `
    // )
    // $('.sec_logout>span').click(function(){
    //     $('.sec_logout>form').submit()
    // })

    //退出登录
    $('.sec_logout').click(function () {
        $('.gat_alert>p').text('亲，真的要退出吗？');
        $('.gat_background').removeClass('tab');
        $('.gat_alert').addClass('animated slideInDown');
        $('.gat_alert_cancel').click(function () {
            $.post({
                url: SERVER_CONTEXT_PATH + '/logout',
                success: function () {
                    if (/(Android)/i.test(navigator.userAgent)) {
                        try{
                            // alert('up')
                            contact.logOut();
                        }catch (e){
                            // alert('down')
                        }
                    }
                    // alert(1111)
                    deleteAllCookies();
                    go('/index.html');
                }
            });
        });
        $('.gat_alert_affirm').click(function () {
            $('.gat_background').addClass('tab');
        });

    });


    // 页面跳转
    $('.d_head>img').click(function () {
        go('/member/information/information.html');
    });

    $(".d_head>span").on("click",function(){
        post({
            url: "/isLogin",
            success: function (data) {
                if (!!data) {
                    go("/message/index.html");
                } else {
                    go("/login/index.html");
                }
            }
        });
    })

    function deleteAllCookies() {
        var cookies = document.cookie.split(";");
        for (var i = 0; i <cookies.length; i++) {
            var cookie = cookies[i];
            var eqPos = cookie.indexOf("=");
            var name = eqPos> -1? cookie.substr(0, eqPos) : cookie;
            document.cookie = name +"=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
        }
    }
    /* $('.sec_realorder').click(function () {
         go('./myorders/allorder.html');
     });*/
    /*$('.sec_share').click(function () {
        go('./share/share.html');
    });
    $('.sec_address').click(function () {
        go('./addressmanagement/addresslist.html');
    });
    $('.sec_account').click(function () {
        go('./myaccount/myaccount.html');
    });
    $('.sec_aboutus').click(function () {
        go('/aboutus/index.html');
    });*/
});