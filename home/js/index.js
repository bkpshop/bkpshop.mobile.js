

var isLogin = false;
var categoryId = 1;
var pageNo = 1;
var inviter = '';

$(function () {
    // invit();
    // custom();
    // newCustom();
    // hot();
    // articleType();
    // articleList();

    setCookie("productdetailgoback", encodeURIComponent(window.location.href));
    localStorage.setItem("articleGoBack",window.location.href);

    $(".search").on("click", function () {
        go("./search/search.html");
    });

    //获取用户uid
    post({
        url: "/isLogin",
        success: function (data) {
            // console.log(data)
            if(data){
                post({
                    url:'/member/uid',
                    success:function(data){
                        console.log(data);
                        inviter = data;
                        custom();
                        newCustom();
                        hot();
                        articleType();
                        articleList();
                    }
                })
            }else{
                custom();
                newCustom();
                hot();
                articleType();
                articleList();
            }

        }
    });
    // post({
    //     url: "/isLogin",
    //     success: function (data) {

    //         if(!!data){
    //             isLogin = true;
    //             post({ //获取用户uid
    //                 url: "/member/uid",
    //                 success: function (data) {
    //                     shareId = data;
    //                 },
    //                 error:function(msg){
    //                     console.log(msg)
    //                 }
    //             });
    //         }
    //     }
    // });

    //跳转到分享
    $(".share").on("click", function () {
        post({
            url: "/isLogin",
            success: function(data){
                if (data) {
                    go("/share/invite.html?id="+inviter);
                } else {
                    go("/login/index.html");
                }
            },
            error:function(msg){
                console.log(msg)
            }
        })

    });

    /*//跳转消息
   
    $(".message").on("click", function () {
        post({
            url: "/isLogin",
            success: function(data){
                if (data) {
                    go("/message/index.html");
                } else {
                    go("/login/index.html");
                }
            },
            error:function(msg){
                console.log(msg)
            }
        })

    });*/
    //跳转购物车
   
    /*$(".cart").on("click", function () {
        post({
            url: "/isLogin",
            success: function(data){
                if (data) {
                    go("/customize/cart/index.html");
                } else {
                    go("/login/index.html");
                }
            },
            error:function(msg){
                console.log(msg)
            }
        })

    });*/
    //banner轮播图
    var bannerSwiper = new Swiper('.banner-swiper', {
        //loop: true, //环路
        //pagination: '.swiper-pagination',
        //autoplay: 5000,
        //autoplayDisableOnInteraction: false //,  //用户操作后是否进行循环播放
    });

    //文章
    function articleType(){
        post({
            url : '/main/category/list',
            success : function(data){
                //console.log(data);
                var _html = "";
                for(var i = 0;i<data.length;i++){
                    _html += '<li class="swiper-slide" name="'+data[i].id+'" data-img="'+data[i]['imageUrl']+'" data-checkimg="'+data[i]['checkImageUrl']+'">';

                    if(i == 0){
                        _html += '<img src="'+SERVER_CONTEXT_PATH+'/load/picture/'+data[i].checkImageUrl+'" alt=""></li>';
                    }else{
                        _html += '<img src="'+SERVER_CONTEXT_PATH+'/load/picture/'+data[i].imageUrl+'" alt=""></li>';
                    }
                    // _html += '<span><i>'+data[i].name+'</i></span></li>';
                }                
                $('.life-list>ul').append(_html);
                $('.life-list>ul>li:first-child').addClass('active');
                var mySwiper = new Swiper('.life-list', {
                    slidesPerView: 6,
                    spaceBetween : 30,
                    slidesOffsetAfter : 20
                })
                $('.life-list li').click(function(){
                    categoryId = $(this).attr("name");

                    $(".life-list li").each(function(){
                        var _src = $(this).data("img");
                        $(this).find("img").attr("src",SERVER_CONTEXT_PATH+'/load/picture/'+_src);
                    });
                    var _srcCheck = $(this).data("checkimg");
                    $(this).find("img").attr("src",SERVER_CONTEXT_PATH+'/load/picture/'+_srcCheck);
                    //$(this).addClass('active').siblings().removeClass('active');
                    articleList();
                })
            },
            error : function (msg){
                console.log("错误");
            }
        })
    }
    function articleList(){
        var _data = {
            page:{
                pageNo:1,
                pageSize:3
            },
            categoryId:categoryId
        }
        post({
            data : _data,
            url : '/main/article/list',
            success : function (data){
                //console.log(data);
                $('.article-list>ul').empty();
                var _html = "";
                for(var i = 0;i<data.list.length;i++){
                    _html += '<li><a href="/article/index.html?id='+data.list[i].id+'&share='+inviter+'" class="clear">';
                    _html += '<span class="single left">'+data.list[i].title+'</span>';
                    _html += '<img src="'+SERVER_CONTEXT_PATH+'/load/picture/'+data.list[i].thumbUrl+'" alt="">';
                    _html += '</a></li>';
                }
                $('.article-list>ul').append(_html);
            },
            error: function(msg){
                console.log("错误");
            }
        })
    }


    //定制推荐
    function custom(){
        post({
            url:'/main/customize/recommend',
            data:{pageNo:1,pageSize:3},
            success:function(data){
                console.log(data);
                var _html = '';
                for(var i=0;i<data.list.length;i++){
                    _html += '<li data-id="'+data.list[i]['id']+'"><a href="javascript:;">';
                    _html += '<img src="home/img/c'+(i+1)+'.png"/><p class="explain single">'+data.list[i]['name']+'</p>';
                    _html += '<span class="explainAgain">'+digitUppercase(data.list[i]['period'])+'天发货<br/>'+data.list[i]['minPrice']+'元/'+data.list[i].minUnit+''+data.list[i].unitName+'/箱起</span></a>';
                    _html += '</li>';
                }
                $(".chain .classify ul").html(_html);
                $(".chain .classify li").click(function(){
                    go("/customize/newDesign.html?productid="+$(this).data("id")+"&inviter="+inviter);
                });
            },
            error:function(msg){
                console.log(msg);
            }
        })
    }

    //新品上架
    function newCustom() {
        var _data = {
            pageNo : 1,
            pageSize : 3
        }
        post({
            data : _data,
            url : '/main/new/putAway',
            success : function (data) {
                //console.log(data);
                var _html = "";
                for(var i = 0;i<data.list.length;i++){
                    _html += '<li><a href="/customize/newDesign.html?productid='+data.list[i].id+'&inviter='+inviter+'">';
                    _html += '<img src="'+SERVER_CONTEXT_PATH+'/load/picture/'+data.list[i].imageUrl+'" alt="">';
                    _html += '<p class="single">'+data.list[i].name+'</p></a></li>';
                }
                $('.new>ul').append(_html);
            },
            error : function (msg) {
                console.log("错误");
            }
        })
    }

    //热门定制
    function hot(){
        var _data = {
            pageNo : pageNo,
            pageSize : 5
        }
        post({
            data : _data,
            url : '/main/hot/sell',
            success : function (data){
                console.log(data);
                var _html = "";
                for(var i = 0;i<data.list.length;i++){
                    _html += '<div class="hot-list"><div class="list-detail"><div class="list-top clear" name="'+data.list[i].id+'&inviter='+inviter+'">';
                    _html += '<img src="'+SERVER_CONTEXT_PATH+'/load/picture/'+data.list[i].imageUrl+'" alt="">';
                    _html += '<div class="t-right right"><p>「</p>';
                    _html += '<p class="three-single">'+data.list[i].keywords+'</p>';
                    _html += '<p>」</p></div></div><div class="list-bottom clear"><div class="b-left left">';
                    _html += '<h3 class="single">'+data.list[i].name+'</h3><i>'+digitUppercase(data.list[i].period)+'天发货</i></div>';
                    _html += '<div class="b-right right">';
                    _html += '<h3 class="single">'+data.list[i].minPrice+'元/'+data.list[i].minUnit+''+data.list[i].unitName+'/箱起</h4>';
                    _html += '<a href="/customize/newDesign.html?productid='+data.list[i].id+'&inviter='+inviter+'">我要定制</a>';
                    _html += '</div></div></div></div>';
                }
                $('.hot-data').append(_html);
                $('.list-top').click(function(){
                    go('/customize/newDesign.html?productid='+$(this).attr("name"));
                })
            },
            error : function (msg) {
                console.log("错误")
            }
        })
    }

    // 上拉加载
    //文档高度
    function getDocumentTop() {
        var scrollTop = 0,
            bodyScrollTop = 0,
            documentScrollTop = 0;
        if (document.body) {
            bodyScrollTop = document.body.scrollTop;
        }
        if (document.documentElement) {
            documentScrollTop = document.documentElement.scrollTop;
        }
        scrollTop = (bodyScrollTop - documentScrollTop > 0) ? bodyScrollTop : documentScrollTop;
        return scrollTop;
    }

    //可视窗口高度
    function getWindowHeight() {
        var windowHeight = 0;
        if (document.compatMode == "CSS1Compat") {
            windowHeight = document.documentElement.clientHeight;
        } else {
            windowHeight = document.body.clientHeight;
        }
        return windowHeight;
    }

    //滚动条滚动高度
    function getScrollHeight() {
        var scrollHeight = 0,
            bodyScrollHeight = 0,
            documentScrollHeight = 0;
        if (document.body) {
            bodyScrollHeight = document.body.scrollHeight;
        }
        if (document.documentElement) {
            documentScrollHeight = document.documentElement.scrollHeight;
        }
        scrollHeight = (bodyScrollHeight - documentScrollHeight > 0) ? bodyScrollHeight : documentScrollHeight;
        return scrollHeight;
    }
    window.onscroll = function () {
        showGoTop($('body').scrollTop())
        //监听事件内容
        if (getScrollHeight() == getWindowHeight() + getDocumentTop()) {
            //当滚动条到底时,这里是触发内容
            //异步请求数据,局部刷新dom
            pageNo++;
            hot();          
        }
    }
});
