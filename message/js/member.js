$(function(){
    var pageNo = 1;

        // 时间轴处理
    function getLocalTime(nS) {   
        Date.prototype.toLocaleString = function() {
        return this.getFullYear() + "年" + (this.getMonth() + 1) + "月" + this.getDate() + "日";
        };  
        return new Date(parseInt(nS)).toLocaleString();      
    }

    getMsgList();
    function getMsgList(){
        var _data = {
            pageNo :  pageNo,
            pageSize : 10
        }
        post({
            data : _data,
            url : "/message/member/list",
            success : function(data){
                console.log(data);
                var _html = "";
                if(data.list.length !== 0){
                    for(var i = 0;i<data.list.length;i++){
                        _html += '<aside><p class="time">'+getLocalTime(data.list[i].createTime)+'</p>';
                        _html += '<div class="msg-list">';
                        _html += '<p>'+data.list[i].content+'</p>';
                        _html += '</div></aside>';
                    }
                    $("section").append(_html)
                }
                if(pageNo == 1 && data.list.length == 0){                    
                    _html += '<aside><img style="width: 50%;margin: .6rem auto 0;display: block;" src="img/kong.png" alt=""/>';
                    _html += '<p style="text-align: center;color: #999;margin-top: .1rem;">暂无新内容</p>';
                    _html += '<span style="display: block;text-align: center;font-size: .13rem;margin-top: .05rem;color: #999;">去别处转转，稍后再来吧！</span></aside>';                
                    $("section").html(_html);
                }                 
            },
            error : function(msg){
                console.log("错误");
            }
        })
    }


        // 上拉加载
    //文档高度
    function getDocumentTop() {
        var scrollTop = 0,
            bodyScrollTop = 0,
            documentScrollTop = 0;
        if (document.body) {
            bodyScrollTop = document.body.scrollTop;
        }
        if (document.documentElement) {
            documentScrollTop = document.documentElement.scrollTop;
        }
        scrollTop = (bodyScrollTop - documentScrollTop > 0) ? bodyScrollTop : documentScrollTop;
        return scrollTop;
    }

    //可视窗口高度
    function getWindowHeight() {
        var windowHeight = 0;
        if (document.compatMode == "CSS1Compat") {
            windowHeight = document.documentElement.clientHeight;
        } else {
            windowHeight = document.body.clientHeight;
        }
        return windowHeight;
    }

    //滚动条滚动高度
    function getScrollHeight() {
        var scrollHeight = 0,
            bodyScrollHeight = 0,
            documentScrollHeight = 0;
        if (document.body) {
            bodyScrollHeight = document.body.scrollHeight;
        }
        if (document.documentElement) {
            documentScrollHeight = document.documentElement.scrollHeight;
        }
        scrollHeight = (bodyScrollHeight - documentScrollHeight > 0) ? bodyScrollHeight : documentScrollHeight;
        return scrollHeight;
    }
    window.onscroll = function () {
        showGoTop($('body').scrollTop())
        //监听事件内容
        if (getScrollHeight() == getWindowHeight() + getDocumentTop()) {
            //当滚动条到底时,这里是触发内容
            //异步请求数据,局部刷新dom
            pageNo++;          
            getMsgList();                       
        }
    }

})