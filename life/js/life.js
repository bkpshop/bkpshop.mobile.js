/**
 * Created by Scoot on 2017/4/22.
 */

var typeName = ["全部"];
var customType = $.getUrlParam("customType");
var login='';
var shareId='';
var tipNum =0;
var _id=$.getUrlParam("customType")||0;

$(function(){

    articleList(true);

    $(".go-back").click(function(){
        window.history.back();
    });

    post({
        url : "/isLogin",
        success : function(data){
            if(!data){
                login = false;
            }else{
                login = true;
                post({
                    url:"/member/uid",
                    success:function(data){
                        shareId = data;
                    }
                });
            }
        }
    });

    var total = 0;
    post({
        url:"/main/category/list",
        success:function(data){
            console.log(data);
            var _slider = '';
            for(var n=0;n<data.length;n++){
                _slider += '<li class="swiper-slide" data-id="'+data[n]['id']+'" data-img="'+data[n]['imageUrl']+'" data-checkimg="'+data[n]['checkImageUrl']+'" data-name="'+data[n]['name']+'">';
                _slider +='<img src="'+SERVER_CONTEXT_PATH+'/load/picture/'+data[n]['imageUrl']+'" alt="">';
                //_slider +='<p class="custom-name red-border-'+(n+1)+'">'+data[n]['name']+'</p>';
                _slider +='</li>';
                typeName.push(data[n]['name']);
            }
            _slider += '';
            $(".custom_list .slider-swiper .swiper-wrapper").append(_slider);
            $(".custom_list .swiper-slide").css("display","block");
            total = data.length+0;
            //_id = data[0]['id'];
            slide();
            /*if(customType){
                setTimeout(function(){
                    pageNum = 1;
                    _id = customType;
                    $(".custom-name").removeClass("red-border");
                    articleList(true);
                },50);
            }*/
        }
    });



    function slide(){
        var sliderSwiper = new Swiper ('.slider-swiper', {
            slidesPerView: 6,
            spaceBetween : 10,
            slidesOffsetAfter : 43
            //slidesPerView: 5,
            //centeredSlides: false,
            //paginationClickable: true,
            //loop:false,
            //spaceBetween: 8,
            /*onTransitionEnd: function(swiper){

                var i = swiper.activeIndex;
                if(i>=3&&i<=total+2){
                    i=i-3;
                }else if(i==1){
                    i=total-2
                }
                else if(i==2){
                    i=total-1
                }else if(i==total+3){
                    i=0;
                }else if(i==0){
                    i=total-3
                }
                _id = i;
                $(".custom-name").removeClass("red-border");
                $(".red-border-"+i).addClass("red-border");
                pageNum = 1;
                articleList(true)
                tipNum ++;
                if(tipNum>0){
                    $(".slider-tip").css("display","none");
                }
            }*/
        });

        $(".slider-swiper").height($(".swiper-slide").width());

        $(".slider-swiper .swiper-slide:eq(0) .custom-name").addClass("red-border");
        $(".slider-swiper .swiper-slide").click(function(){
            //$(".custom-name").removeClass("red-border");
            //$(this).find(".custom-name").addClass("red-border");
            $(".slider-swiper .swiper-slide").each(function(){
                var _src = $(this).data("img");
                if($(this).index() == 0){
                    $(this).find("img").attr("src",_src);
                }else{
                    $(this).find("img").attr("src",SERVER_CONTEXT_PATH+'/load/picture/'+_src);
                }

            });
            var _name = $(this).data("name");
            var _srcCheck = $(this).data("checkimg");
            $(".header span").text(_name);
            if($(this).index() == 0){
                $(this).find("img").attr("src",_srcCheck);
            }else{
                $(this).find("img").attr("src",SERVER_CONTEXT_PATH+'/load/picture/'+_srcCheck);
            }

            _id = $(this).data("id");
            pageNum = 1;
            articleList(true);
        });
    }

});

var pageNum = 1;
function articleList(flag){

    var _page = '';
    if(_id == 0){
        _page =  {
            page:{
                pageNo:pageNum,
                pageSize:10
            } ,
            categoryId:''
        };
    }else{
        _page =  {
            page:{
                pageNo:pageNum,
                pageSize:10
            } ,
            categoryId:_id
        };
    }

    post({
        url:"/main/article/list",
        data:_page,
        success:function(data){
            //console.log(data)
            var _list = '';
            _list += '<div class="list-contain">';
            for(var i=0;i<data.list.length;i++){
                _list += '<div class="list" data-id="'+data.list[i]["id"]+'">';
                _list += '<img src="'+SERVER_CONTEXT_PATH+'/load/picture/'+data.list[i]['thumbUrl']+'" alt=""/>';
                _list += '<div class="context"><h4>'+data.list[i]['title']+'</h4>';
                _list += '<p class="single">'+data.list[i]['articleDigest']+'</p>';
                _list += '<div class="footer"><div class="vote">'+data.list[i]['voteNum']+'</div>';
                _list += '<div class="share">'+data.list[i]['shareNum']+'</div>';
                _list += '<div class="comment">'+data.list[i]['commentNum']+'</div></div>';
                _list += '</div></div>'
            }
            _list += '</div>';
            if(flag){
                if(pageNum == 1 && data.total == 0){
                    var _n = '<div class="none "><img src="../common/img/none.png" alt=""/>';
                    _n += '<p>暂无新内容</p><p>去别处转转，稍后再来吧！</p></div>';

                    $(".switchover .area").html(_n);
                }else{
                    $(".switchover .area").html(_list);
                }

            }else{
                $(".switchover .area").append(_list);
            }

            $(".list-contain .list").click(function(){
                //setCookie("articleGoBack",window.location.href);
                localStorage.setItem("articleGoBack",window.location.href);
                go("/article/index.html?id="+$(this).data("id")+"&share="+shareId );
            });
        }
    })

}

// 上拉加载
//文档高度
function getDocumentTop() {
    var scrollTop = 0, bodyScrollTop = 0, documentScrollTop = 0;
    if (document.body) {
        bodyScrollTop = document.body.scrollTop;
    }
    if (document.documentElement) {
        documentScrollTop = document.documentElement.scrollTop;
    }
    scrollTop = (bodyScrollTop - documentScrollTop > 0) ? bodyScrollTop : documentScrollTop;
    return scrollTop;
}

//可视窗口高度
function getWindowHeight() {
    var windowHeight = 0;
    if (document.compatMode == "CSS1Compat") {
        windowHeight = document.documentElement.clientHeight;
    } else {
        windowHeight = document.body.clientHeight;
    }
    return windowHeight;
}

//滚动条滚动高度
function getScrollHeight() {
    var scrollHeight = 0, bodyScrollHeight = 0, documentScrollHeight = 0;
    if (document.body) {
        bodyScrollHeight = document.body.scrollHeight;
    }
    if (document.documentElement) {
        documentScrollHeight = document.documentElement.scrollHeight;
    }
    scrollHeight = (bodyScrollHeight - documentScrollHeight > 0) ? bodyScrollHeight : documentScrollHeight;
    return scrollHeight;
}
window.onscroll = function () {
            showGoTop($('body').scrollTop())
    //监听事件内容
    if(getScrollHeight() == getWindowHeight() + getDocumentTop()){
        //当滚动条到底时,这里是触发内容
        //异步请求数据,局部刷新dom
        pageNum++;
        articleList(false);
    }
};