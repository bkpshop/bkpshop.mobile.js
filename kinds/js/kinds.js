$(function () {
    var height = $(window).height() - $("header").height() - $("#bottom").height() - 8;
    $("#kinds").height(height);

    //导航栏动画
    function navAnimate(data){
        $(".kinds_list li").on("click",function(){
            $(this).addClass("selected").siblings().removeClass("selected");
            var index = ($(this).index())*0.5;
            $(".kinds_list>span").animate({"top":index+0.2+"rem"},100);
            dealDetail(data[$(this).index()]['children'])
            $(".kinds_detail>h4").text(data[$(this).index()]['name']+'分类')
        })
    }

    function getData() {
        post({
            url: "/product/category/4/child",
            success: function (data) {
                console.log(data)
                var _list = "";
                
                for(var i = 0;i<data.length;i++){
                    if(i == 0){
                        _list += '<li class="selected">'+data[i].name+'</li>'
                    }else{
                        _list += '<li>'+data[i].name+'</li>'
                    }                                     
                }
                $(".kinds_list>ul").append(_list);
                dealDetail(data[0]['children']);
                $(".kinds_detail>h4").text(data[0]['name']+'分类');
                navAnimate(data);
            }
        })
    }
    getData();

    function dealDetail(list){
        var _detail = "";
        for(var j = 0;j<list.length;j++){
            _detail += '<li><a href="/productlist/list.html#'+list[j].id+'">';
            _detail += '<img src="'+SERVER_CONTEXT_PATH+'/load/picture/'+list[j].imageUrl+'" alt="">';
            _detail += '<p class="single">'+list[j].name+'</p>';
            _detail += '</a></li>';
        }   
        //console.log(list);
        $(".foodlist>ul").html(_detail);
    }

})