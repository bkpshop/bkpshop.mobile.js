$(function () {

	init();

	//用户名的失去焦点事件
	$(".phone>input").blur(function () {
		if (!(/^1[34578]\d{9}$/.test($(this).val())) && ($(this).val().length !== 0)) {
			$('.phone').addClass("dl_error").siblings().removeClass('dl_error');
			// $('#banner .information .phone .err').css('display','block')
		}
	});
	//用户名的获得焦点事件，去除警告状态
	$(".phone>input").focus(function () {
		$('.phone').removeClass("dl_error").next().removeClass('dl_error');
		// $('#banner .information .phone .err').css('display','none')
	});
	//密码框失去焦点事件
	$(".password>input").blur(function () {
		if (!(/^(?![\d]+$)(?![a-zA-Z]+$)(?![^\da-zA-Z]+$).{6,20}$/.test($(this).val())) && ($(this).val().length !== 0)) {
			$('.password').addClass("dl_error").siblings().removeClass('dl_error');
			// $('#banner .information .password .err_t').css('display','block')
		}
	});
	//密码框的获得焦点事件，去除警告状态
	$(".password>input").focus(function () {
		$('.password').removeClass("dl_error").next().removeClass('dl_error');
		// $('#banner .information .password .err_t').css('display','none')
	});
	//登录按钮点击事件

	$(".login").on('click', function (e) {
		e.preventDefault();
		if(!(/^1[34578]\d{9}$/.test($('.phone>input').val()))){
			alertBox('请输入正确的手机号');
			return false;
		}else if(!(/^(?![\d]+$)(?![a-zA-Z]+$)(?![^\da-zA-Z]+$).{6,20}$/.test($('.password>input').val()))){
			alertBox('密码错误');
			return false;
		}else if($(".yzm>input").val().length == 0){
			alertBox('验证码有误')
			return false
		}else{
		// if ($(".information").children().hasClass("dl_error") == false) {
			// if ($('.phone>input').val().length !== 0 && $('.password>input').val().length !== 0 && $(".yzm>input").val().length !== 0) {
				var username = $(".phone>input").val();
				var password = $(".password>input").val();
				var data = {
					username: Base64.encode(username),
					password: Base64.encode(password),
					captcha: $(".yzm>input").val()
				};
				post({
					url: "/login/check",
					data: data,
					success: function (url) {
						localStorage.setItem('logIn','success');
                        var backUrl = decodeURIComponent(getCookie("loginBack"));
                        if(backUrl){
                            if(backUrl.indexOf("member")>-1){
                                go("/memberindex.html");
                            }else{
                                go(backUrl);
                            }

                        }else{
                            go(url);
                        }
                        /*if(url == "/"){
                            var backUrl = decodeURIComponent(getCookie("loginBack"));
							// alert(backUrl)
                            if(backUrl){
                                if(backUrl.indexOf("member")>-1){
                                    go("/memberindex.html");
                                }else{
                                    go(backUrl);
                                }

                            }else{
                                go(url);
                            }
                        }else{
                            go(url);
                        }*/
					},
					error: function (msg) {
						alertBox(msg);
					}
				});
			// }
		// }
		}
	});

	// 换一换
	$(".getYZM").click(function () {
		getCaptcha();
	});

	// 返回上一级
	$(".header .go-back").on("click", function () {
        if (document.referrer === '') { //当无上级页面时  返回首页。
            window.location.href = "/index.html";
        }else{
            window.history.back();
        }
	});

	isWeiXin();
	//判断是否是微信
	function isWeiXin() {
		var ua = window.navigator.userAgent.toLowerCase();
		if (ua.match(/MicroMessenger/i) == 'micromessenger') {
			// return true;
			$('.third_party').removeClass('tab');
			$('footer').css('top', '0');
		} else {
			// return false;
			$('.third_party').addClass('tab');
			$('footer').css('top', '1.5rem');

		}
	}


	// 第三方登录
	weChatFu();

	function weChatFu() {
		$('.third_party>img').click(function () {
			go('/weixin/pub/login');
		});
	};


});

function init() {
	getCaptcha();
}

function getCaptcha() {
	$("#captchaImg").attr("src", "" + SERVER_CONTEXT_PATH + "/captcha.jpg?t=" + new Date().getTime());
}

function alertBox(msg) {
	$('#alertBox,.alertBoxA').removeClass('tab');
	$('.alertBoxText').text(msg);
	$('#alertBox').off().on('click', function () {
		$('#alertBox,.alertBoxA').addClass('tab');
	});
	$('.alertBoxA').off().on('click', function () {
		$('#alertBox,.alertBoxA').addClass('tab');
	})
}