
$(function(){

    var designerId = $.getUrlParam('designerId')
    $('.d_header>.head-back').click(function(){
        window.history.go(-1)
    })

    designer();
    function designer() {
        post({
            url : '/customizeProduct/designer/detail/'+designerId,
            success : function (data) {
                console.log(data);
                var _html = "";
                for(var i = 0;i<data.picList.length;i++){
                    _html += '<li class="swiper-slide">';
                    _html += '<img src="'+SERVER_CONTEXT_PATH+'/load/picture/'+data.picList[i].imageUrl+'" alt=""></li>';
                }
                $('.ban-swiper>ul').append(_html);
                $('.design-name').text(data.name);
                $('.introduct').append(data.remark);
                var bannerSwiper = new Swiper('.ban-swiper', {
                    loop: true, //环路
                    pagination: '.swiper-pagination',
                    autoplay: 5000,
                    autoplayDisableOnInteraction: false //,  //用户操作后是否进行循环播放
                });
            },
            error : function (msg) {
                console.log("错误");
            }
        })
    }

})