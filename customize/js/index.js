var inviter = '';
$(function(){
    setCookie("productdetailgoback",encodeURIComponent(window.location.href));
    var pageNum = 1, _id=null;
    var searchData = decodeURI($.getUrlParam('searchData'));
    if(searchData != 'null'){
        $('._seachBar>input').val(searchData);
    }else{
        searchData = null;
    }
    console.log(searchData)
    iScroll();
	goodList(pageNum,null,searchData);
    
    //是否登录
    invit();


    // 搜索框
    $('._seachBar>span').click(function(){
        // window.history.pushState({},0,'http://'+window.location.host+'/customize/index.html');
        searchData = $('._seachBar>input').val();
        $('.datalist').empty();
        pageNum = 1;
        goodList(pageNum,_id,searchData);
    })
    $('._more').click(function(){
        $('.msg').removeClass('tab');
    })
    $('.msg').click(function(){
        $(this).addClass('tab');
    })

	function nextSession(flag){
		$(".datalist>li").on("click",function(){
			var id = $(this).attr("name");

            go("/customize/newDesign.html?productid="+id+"&inviter="+inviter)
		})
	}

    //顶部nav
    post({
        url:'/main/lifeWay/list',
        success:function(data){
            console.log(data)
            for(var i=0; i<data.length; i++){
                $('.navList').append(
                    '<li data-id="'+data[i].id+'" class="swiper-slide">'
                    +'<a href="javascript:;">'+data[i].name+'</a>'
                    +'</li>'
                )
            }
            var mySwiper = new Swiper('.nav-swiper', {
                slidesPerView: 5,
                centeredSlides: false
            })
        }
    })

    // var mySwiper = new Swiper('.nav-swiper', {
    //     slidesPerView: 5,
    //     centeredSlides: false
    // })
    $(".navList").delegate("li","click", function () {
        $(this).addClass("selected").siblings().removeClass("selected");
        console.log($(this).text());
        // $('header').text($(this).text())
        $('.datalist').empty();
        _id = $(this).data('id');
        pageNum = 1;
        goodList(pageNum,_id,searchData)
    })
	//商品列表
	function goodList(pageNum,_id,searchData){
		post({
			url:'/main/lifeWay/product',
			data:{page:{pageNo:pageNum,pageSize:10},lifeWayId:_id,productName:searchData},
			success:function(data){
				console.log(data)
				var lists = data.list;
                if(data.total == 0){
                    console.log(11111)
                    $('.datalist').append(
                        '<img style="width: 50%;margin: .6rem auto 0;display: block;" src="./img/no_data.png" alt=""/>'
                        +'<p style="text-align: center;color: #999;margin-top: .1rem;">暂无新内容</p>'
                        +'<span style="display: block;text-align: center;font-size: .13rem;margin-top: .05rem;color: #999;">去别处转转，稍后再来吧！</span>'
                    )
                }else{
                    var html_list = '';
                    for(var i in lists){
                        html_list += '<li name="'+lists[i].id+'">';
                        html_list += '<img src="'+SERVER_CONTEXT_PATH+'/load/picture/'+lists[i].imageUrl+'" alt="">';
                        html_list += '<div class="custom-detail">';
                        html_list += '<h4 class="single">'+lists[i].name+'</h4><p>'+digitUppercase(lists[i].period)+'天发货</p><span>'+lists[i].minPrice+'元/'+lists[i].minUnit+''+data.list[i].unitName+'/箱起</span>';
                        html_list += '<input type="button" value="我要定制">';
                        html_list += '</div></li>'
                    };						
				    $('.datalist').append(html_list);
                    //var mySwiper = new Swiper('.nav-swiper', {
                    //    slidesPerView: 5,
                    //    centeredSlides: false
                    //})
                }
				nextSession();
			}
		})
	 
	}
	// 上拉刷新函数
    function iScroll() {
        function iScrollA() {
            var scrollTop = 0,
                bodyScrollTop = 0,
                documentScrollTop = 0;
            if (document.body) {
                bodyScrollTop = document.body.scrollTop;
            }
            if (document.documentElement) {
                documentScrollTop = document.documentElement.scrollTop;
            }
            scrollTop = (bodyScrollTop - documentScrollTop > 0) ? bodyScrollTop : documentScrollTop;
            return scrollTop;
        }

        function iScrollB() {
            var windowHeight = 0;
            if (document.compatMode == "CSS1Compat") {
                windowHeight = document.documentElement.clientHeight;
            } else {
                windowHeight = document.body.clientHeight;
            }
            return windowHeight;
        }

        function iScrollC() {
            var scrollHeight = 0,
                bodyScrollHeight = 0,
                documentScrollHeight = 0;
            if (document.body) {
                bodyScrollHeight = document.body.scrollHeight;
            }
            if (document.documentElement) {
                documentScrollHeight = document.documentElement.scrollHeight;
            }
            scrollHeight = (bodyScrollHeight - documentScrollHeight > 0) ? bodyScrollHeight : documentScrollHeight;
            return scrollHeight;
        }
        window.onscroll = function () {
            showGoTop($('body').scrollTop())
            if (iScrollC() == iScrollB() + iScrollA()) {
                pageNum += 1;
                goodList(pageNum,_id,searchData);
                return pageNum;
            }
        }
    };

        
})