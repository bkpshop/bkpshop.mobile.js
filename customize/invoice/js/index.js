
$(function(){

    var userId = "";
    var title = "";
    var type = "addedValueNormalTax";
    var invoiceCategory = "PERSON";
    var contentId = "";
    var urlId = "";
    var kind = "no-need";
    var identifyNumber = "";
    var banckAccountNo = "";
    var banckAccount = "";
    var tel = "";
    var address = "";
    needInvoice();
    getInvoiceList();
    invoiceObj();
    invoiceType();
    getUserId();
    invoiceContent();
    submit();
    
    $(".btns>input").on("click",function(){
        $(this).addClass("active").siblings().removeClass("active");
    })    

    //点击是否需要发票
    function needInvoice(){
        $(".no-need").on("click",function(){ 
            kind = "no-need";                  
            $(".invoice-detail").addClass("tab");
        })
        $(".need").on("click",function(){ 
            kind = "need";
            $(".invoice-detail").removeClass("tab");
        })
    }

    //获取用户个人发票列表
    function getInvoiceList(){
        post({
            url : "/invoice/list",
            success : function(data){
                //console.log(data)
                var _html = "";
                for(var i = 0;i<data.length;i++){
                    _html += '<div class="select-invoice clear"><span class="select list left">';
                    _html += '<img src="img/yuan.png" alt="">';
                    _html += '</span><i class="single left">'+data[i].title+'</i>';
                    _html += '<b class="single left">'+data[i].invoiceContent+'</b>';
                    _html += '<span class="delete right" name="'+data[i].id+'"><img src="img/ljt.png" alt=""></span></div>';
                }
                $(".invoice-list").append(_html);
                $(".select>img").on("click",function(){
                    $(".select>img").attr("src","img/yuan.png");
                    $(this).attr("src","img/xuanzhong.png");
                    urlId = $(this).parent().siblings("span").attr("name");
                })
                clickDel();
            },
            error : function(msg){
                console.log("错误");
            }
        })
    }

    // 获取userId
    function getUserId(){
        post({
            url: "/member/uid",
            success: function (data) {
                console.log(data);
                userId = data;
            },
            error : function(msg){
                console.log("错误");
            }
        });
    }

    //获取开票对象分类
    function invoiceObj(){
        post({
            url: "/invoice/category",
            success: function (data) {
                console.log(data);
                var _html = '';
                for(var i = 0;i<data.length;i++){
                    _html += '<option value="'+data[i].name+'">'+data[i].text+'</option>';
                }                
                $(".invoice-obj>select").append(_html);
                $(".invoice-obj>select").on("change",function(){
                    invoiceCategory = $(this).val();
                    if(invoiceCategory == "UNIT"){
                        $(".identify-num").removeClass("tab"); 
                    }else{
                        $(".identify-num").addClass("tab");
                    }
                    invoiceType();
                })
                
            },
            error : function(msg){
                console.log("错误");
            }
        })
    }

    //获取发票类型(是否是增值税发票)
    function invoiceType(){
        var _data = {
            invoiceCategory : invoiceCategory
        };
        post({
            data: _data,
            url: "/invoice/type",
            success: function(data){
                console.log(data);
                var _html = "";
                for(var i = 0;i<data.length;i++){
                    if(invoiceCategory == "UNIT"){
                        _html += '<option value="'+data[i].name+'">'+data[i].text+'</option>';
                    }                   
                }
                if(invoiceCategory == "PERSON"){
                    _html += '<option value="'+data.name+'">'+data.text+'</option>';
                }
                $(".invoice-type>select").empty();
                $(".invoice-type>select").append(_html);
                $(".invoice-type>select").on("change",function(){
                    type = $(this).val();
                    if(type == "addedValueSpecialTax"){
                        $(".else-invoice").removeClass("tab");
                    }else{
                        $(".else-invoice").addClass("tab");
                    }
                    invoiceContent();
                })
            },
            error: function(msg){
                console.log(msg);
            }
        })
    }


    //发票内容
    function invoiceContent(){
        var _data = {
            invoiceCategory : invoiceCategory,
            invoiceType: type
        };
        post({
            data : _data,
            url : "/invoice/content/",
            success : function(data){
                $(".invoice-content>select").empty();
                console.log(data);
                var _html = "";
                for(var i = 0;i<data.length;i++){
                    _html += '<option value="'+data[i].id+'">'+data[i].name+'</option>';
                }
                if(data.length !== 0){
                    contentId = data[0].id;
                }
                $(".invoice-content>select").append(_html);
                $(".invoice-content>select").on("change",function(){
                    contentId = $(this).val();
                })
            },
            error : function(msg){
                console.log("错误");
            }
        })
    }

    //新增发票
    function addInvoice(){
        var _data = {
            title : title,
            userId : userId,
            invoiceCategory :invoiceCategory,
            invoiceContentId :contentId,
            identifyNumber: identifyNumber,
            adress: address,
            tel: tel,
            banckAccount : banckAccount,
            banckAccountNo : banckAccountNo
        }
        console.log(_data)
        post({
            url : "/invoice/save",
            data : _data,
            success : function(data){
                console.log(data);
                localStorage.setItem("invoice_id",data);
                go("/mobileorder/order_confirm.html");
            },
            error: function(msg){
                console.log("错误");
            }
        })
    }

    //删除个人发票
    function deleteInvoice(){
        post({
            url : "/invoice/delete/"+delId,
            success : function(data){
                $(".title_alert").html("删除成功!");
                $(".title_alert").removeClass("tab");
                setTimeout(function(){
                    $(".title_alert").addClass("tab");
                },3000);
            },
            error : function(msg){
                console.log("错误")
            }
        })
    }

    //点击删除按钮
    function clickDel(){
        $(".delete").on("click",function(){
            $(this).parent().addClass("tab");
            delId = $(this).attr("name");
            deleteInvoice();
        })
    }    

    //点击确认保存,分为三种操作 1、不需要发票 2、新增发票 3、选择现有发票
    function submit(){
        $(".invoiceSubmit").on("click",function(){                        
            if(kind == "no-need"){//不需要发票
                localStorage.setItem("invoice_id","");
                go("/mobileorder/order_confirm.html");
            }else{//需要发票新增
                var imageUrl = $(".selected-invoice>span>img").attr("src");
                title = $(".invoice-head>input").val().trimSpace();
                selectContent = $(".invoice-content>select").html();
                identifyNumber = $(".identify-num>input").val().trimSpace();
                banckAccountNo = $(".invoice-bink>input").val().trimSpace();
                banckAccount = $(".invoice-startbink>input").val().trimSpace();
                tel = $(".invoice-tel>input").val().trimSpace();
                address = $(".invoice-address>input").val().trimSpace();
                if(imageUrl == "img/xuanzhong.png"){ //是否选中                    
                    //console.log(title)
                    if(type == "addedValueSpecialTax"){
                        if(title == ""){
                            $(".title_alert").html("请输入发票抬头!");
                            $(".title_alert").removeClass("tab");
                            setTimeout(function(){
                                $(".title_alert").addClass("tab");
                            },3000);
                        }else if(identifyNumber == ""){
                            $(".title_alert").html("请输入纳税人识别号!");
                            $(".title_alert").removeClass("tab");
                            setTimeout(function(){
                                $(".title_alert").addClass("tab");
                            },3000);
                        }else if(!((/[0-9a-zA-Z_]{18}/).test(identifyNumber))){
                            $(".title_alert").html("请输入正确的识别号!");
                            $(".title_alert").removeClass("tab");
                            setTimeout(function(){
                                $(".title_alert").addClass("tab");
                            },3000);
                        }else if(banckAccountNo == ""){
                            $(".title_alert").html("请输入银行账号!");
                            $(".title_alert").removeClass("tab");
                            setTimeout(function(){
                                $(".title_alert").addClass("tab");
                            },3000);
                        }else if(banckAccount == ""){
                            $(".title_alert").html("请输入开户行!");
                            $(".title_alert").removeClass("tab");
                            setTimeout(function(){
                                $(".title_alert").addClass("tab");
                            },3000);
                        }else if(address == ""){
                            $(".title_alert").html("请输入地址!");
                            $(".title_alert").removeClass("tab");
                            setTimeout(function(){
                                $(".title_alert").addClass("tab");
                            },3000);
                        }else{
                            addInvoice();
                            $(".title_alert").html("添加发票成功!");
                            $(".title_alert").removeClass("tab");
                            setTimeout(function(){
                                $(".title_alert").addClass("tab");
                            },3000);
                        }
                    }else if(invoiceCategory == "UNIT" && type == "addedValueNormalTax"){
                        if(title == ""){
                            $(".title_alert").html("请输入发票抬头!");
                            $(".title_alert").removeClass("tab");
                            setTimeout(function(){
                                $(".title_alert").addClass("tab");
                            },3000);
                        }else if(identifyNumber == ""){
                            $(".title_alert").html("请输入纳税人识别号!");
                            $(".title_alert").removeClass("tab");
                            setTimeout(function(){
                                $(".title_alert").addClass("tab");
                            },3000);
                        }else if(!((/[0-9a-zA-Z_]{18}/).test(identifyNumber))){
                            $(".title_alert").html("请输入正确的识别号!");
                            $(".title_alert").removeClass("tab");
                            setTimeout(function(){
                                $(".title_alert").addClass("tab");
                            },3000);
                        }else if(selectContent == ""){
                            $(".title_alert").html("无法添加发票!");
                            $(".title_alert").removeClass("tab");
                            setTimeout(function(){
                                $(".title_alert").addClass("tab");
                            },3000);
                        }else{
                            addInvoice();
                            $(".title_alert").html("添加发票成功!");
                            $(".title_alert").removeClass("tab");
                            setTimeout(function(){
                                $(".title_alert").addClass("tab");
                            },3000);  
                        }
                    }else{
                        if(title == ""){
                            $(".title_alert").html("请输入发票抬头!");
                            $(".title_alert").removeClass("tab");
                            setTimeout(function(){
                                $(".title_alert").addClass("tab");
                            },3000);
                        }else if(selectContent == ""){
                            $(".title_alert").html("无法添加发票!");
                            $(".title_alert").removeClass("tab");
                            setTimeout(function(){
                                $(".title_alert").addClass("tab");
                            },3000);
                        }else{
                            addInvoice();
                            $(".title_alert").html("添加发票成功!");
                            $(".title_alert").removeClass("tab");
                            setTimeout(function(){
                                $(".title_alert").addClass("tab");
                            },3000);  
                        }
                    }
                } else{//选择现有发票
                    localStorage.setItem("invoice_id",urlId);
                    go("/mobileorder/order_confirm.html");
                }
            }                  
        })
    }  

})