$(function(){
    goLogin();
});
// setCookie("productdetailgoback",encodeURIComponent(window.location.href));
var num = 0;
var codes = [];
// 订单内容
// function orderListFn(listData, orderList) {
function orderListFn(listData) {
    if(listData["designType"] == 'DESIGN'){
        var designer = '<span>定制</span>';
        var designerFee = '<li><span>设计费：</span><i>￥'+returnFloat(listData["designFee"])+'</i></li>';
        var html_makeFee = '<li>'
                                +'<span>制作单价：</span>'
                                +'<i>￥'+returnFloat(listData["makeFee"])+'</i>'
                                +'<b class="right" style="color:#999">x'+listData["num"]+'</b>'
                            '</li>'

    }else{
        var designer = '<span>原厂</span>';
        var designerFee = '';
        var html_makeFee ='';

    }
    
    $('section').append(
        '<aside>'
            +'<div class="aside_top clear">'
                +'<p class="orderNum left">订单编号：<span>'+listData["code"]+'</span></p>' 
                +'<p class="orderDate right">'+getLocalTime(listData["createTime"])+'</p>'
            +'</div>'
            +'<div class="aside_center">'
                +'<a href="../orderdetail/detail.html?orderId='+listData["code"]+'" class="clear">'
                    +'<img src="'+SERVER_CONTEXT_PATH+'/load/picture/'+listData["productUrl"]+'" class="aside_center_pic left">'
                    +''+designer+''
                        +'<ul class="aside_center_text left">'
                            +'<li class="clear">'
                                +'<span class="left">'+listData["productName"]+'</span>'
                            
                        +'</li>'
                        +'<li>'
                           +' <span>商品单价：</span>'
                            +'<i>￥'+returnFloat(listData["price"])+'</i>'
                            +'<b class="right" style="color:#999">x'+listData["num"]+'</b>'
                        +'</li>'       
                        +''+designerFee+''
                        +''+html_makeFee+''
                        
                    +'</ul>'           
                +'</a>'
            +'</div>' 
            +'<div class="aside_bottom">'
                +'<p class="clear">'
                    +'<span>应付总额：<i>￥'+returnFloat(listData["totalPrice"])+'</i><b>（包邮）</b></span>'
                    +'<b class="right">'+statusName+'</b>'
                +'</p>'
                +'<div class="btns clear">'
                    +''+btn_1+''
                    +''+btn_2+''
                    +''+btn_3+''
                                    
                +'</div>'
            +'</div>'
        +'</aside>'
    )
};
    // <input type="button" class="cancel left" value="取消">
    // <input type="button" class="pay right" value="在线支付">
    // <input type="button" class="design right" value="设计编辑">    
// var codes=[];
function statusFn(data){
    // var codes = []
    console.log(data);
    switch (data["orderStep"]) {
        //已删除的状态
        // case 'WAIT_PAYMENT':
        //     statusName = '待付预付款'
        //     btn_1 = ''
        //     btn_2 = ''
        //     btn_3 = ''  
        // case 'WAIT_FINAL_PAYMENT':
        //     statusName = '待付尾款'
        //     btn_1 = ''
        //     btn_2 = ''
        //     btn_3 = ''
        //.......

        case 'WAIT_PAYING':
            statusName = '待付款'
            btn_1 = '<input type="button" data-code="'+data["code"]+'" class="cancel del_order left" value="取消">'
            btn_2 = '<input type="button" data-code="'+data["code"]+'" data-totalPrice="'+data["totalPrice"]+'" class="pay pay_money right" value="在线支付">'
            if(data.designType == 'DESIGN'){
                btn_3 = '<input type="button" data-code="'+data["code"]+'" class="design design_edit right" value="设计编辑"> '
            }else{
                btn_3 = ''
            }
            break;
        case 'WAIT_CONFIRM':
            statusName = '已付款'
            btn_1 = ''
            btn_2 = '<input type="button" data-code="'+data["code"]+'" class="pay design_confirm right" value="设计确认">'
            btn_3 = ' '
            break;    
        case 'WAIT_DELIVERY':
            statusName = '待收货'
            btn_1 = ''
            btn_2 = '<input type="button" data-code="'+data["code"]+'" class="pay confirm_receiving right" value="确认收货">'
            btn_3 = '<input type="button" data-code="'+data["code"]+'" class="design find_stream right" value="查看物流">'
            break;
        case 'WAIT_SEND':
            statusName = '待发货'
            btn_1 = ''
            btn_2 = ''
            btn_3 = ''
            break;
        case 'COMPLETE':
            statusName = '订单完成'
            btn_1 = '<a class="tel left" href="tel:400-825-8521"><input type="button" style="border-radius: 3px" class="cancel after_sale" value="售后"></a>'
            btn_2 = '<input type="button" data-code="'+data["code"]+'" class="pay add_com tab right" value="我要追评">'
            btn_3 = ''
            break;
         case 'CLOSED':
            statusName = '交易关闭'
            btn_1 = ''
            btn_2 = ''
            btn_3 = ''
            break;
        case 'WAIT_REVIEW':
            statusName = '待评论';
            btn_1 = '<a class="tel left" href="tel:400-825-8521"><input type="button" style="border-radius: 3px" class="cancel after_sale" value="售后"></a>'
            btn_2 = '<input type="button" data-code="'+data["code"]+'" class="pay com right" value="我要评论">'
            btn_3 = ''
            break;
        default:
            break;
    }
    
};

//已完成追评
function add_com(){
    
    //制造商
    $('.add_com').each(function(){
        var _this_add_m = $(this);
        var _code_add_m = $(this).data('code');
        post({
            url:'/customizeProduct/findReviews/',
            data:{
                code:_code_add_m,
                type:'manufacturer'
            },
            success:function(obj){
                console.log(obj)
                if(obj.isAdditional == true){
                    _this_add_m.removeClass('tab');
                    $(_this_add_m).click(function(){
                        go('/customized/manufacturerComment/addition.html#'+obj.id+'')
                    })
                }

            },
            error:function(msg){
                console.log(msg)
            }
        })
    })
}
//待评价评价
// function com(){
  
//  //制造商
//     $('.com').each(function(){
//         var _this_m = $(this);
//         var _code_m = $(this).data('code');
//         post({
//             url:'/customizeProduct/findReviews/',
//             data:{
//                 code:_code_m,
//                 type:'manufacturer'
//             },
//             success:function(obj){
//                 // console.log(obj)
//                 if(obj.isAdditional == null){
//                     _this_m.removeClass('tab');
//                     $(_this_m).click(function(){
//                         go('/customized/manufacturerComment/publish.html#'+_code_m+'')
//                     })
//                 }else{
//                     _this_m.addClass('tab')
//                 }
                
//             },
//             error:function(msg){
//                 console.log(msg)
//             }
//         })
//     })
// }

function timeFn(timedata,code){
    if(timedata<=0){
        post({
            url:'customizeOrder/timeout',
            data:{code:code},
            success:function(){
                window.location.reload();
            }
        })
    }else{
        var date = new Date(timedata);
        h = date.getHours() + ':';
        m = date.getMinutes();  
       var timeD = h + m;  
    }
};

function operationFn(){
    //删除订单
    $('section').delegate('.del_order','click',function(){
        var code = $(this).data('code');
        var _this = $(this);
        $('.gat_alert>p').text('是否确定关闭该订单？');
        $('.gat_background').removeClass('tab');
        $('.gat_alert').addClass('animated slideInDown');
        $('.gat_alert_cancel').click(function () {
            $('.gat_background').addClass('tab');
        });
        $('.gat_alert_affirm').click(function () {
            post({
                url:'/customizeOrder/cancel',
                data:{"code":code},
                success:function(data){
                    window.location.reload();
                    
                }
            })
        });
    });
    //确认收货
    $('section').delegate('.confirm_receiving','click',function(){
        var code = $(this).data('code');
        $('.gat_alert>p').text('请收到货后，再确认收货！');
        $('.gat_background').removeClass('tab');
        $('.gat_alert').addClass('animated slideInDown');
        $('.gat_alert_cancel').click(function () {
            $('.gat_background').addClass('tab');
        });
        $('.gat_alert_affirm').click(function () {
            post({
                url:'/customizeOrder/confirmationReceipt',
                data:{"code":code},
                success:function(data){
                    window.location.reload();
                }
            })
        });
    });

    //设计确认
    $('section').delegate('.design_confirm','click',function(){
        var code = $(this).data('code');
        $('.gat_alert>p').text('设计商为您设计的作品您是否满意！');
        $('.gat_background').removeClass('tab');
        $('.gat_alert').addClass('animated slideInDown');
        $('.gat_alert_cancel').click(function () {
            $('.gat_background').addClass('tab');
        });
        $('.gat_alert_affirm').click(function () {
            post({
                url:'/customizeOrder/designerConfirm',
                data:{"code":code},
                success:function(data){
                    window.location.reload();
                }
            })
        });
    });

    //查看物流
    $('section').delegate('.find_stream','click',function(){
        var code = $(this).data('code');
        go('/customized/customizedorder/logistics/index.html?orderId='+code);
    })

    //在线支付
    $('section').delegate('.pay_money','click',function(){
        var code = $(this).data('code');
        var totalprice = $(this).data('totalprice')
        //console.log(code,totalprice)
        setCookie("pay-content","customimprest"); //支付预付款、原厂包装
        setCookie("wait-code",code);
        setCookie("wait-amount",totalprice);
        setCookie("step", "1");
        setCookie("productdetailgoback",encodeURIComponent(window.location.href));
        go('/mobileorder/selectpaytype.html');
       
    })
    //在线编辑
    $('section').delegate('.design_edit','click',function(){
        var code = $(this).data('code');
        go('/customize/gocustomize/designcontain.html?orderId='+code);
    })
    //去评论
    $('section').delegate('.com','click',function(){
        var code = $(this).data('code');
        go('/customized/manufacturerComment/publish.html#'+code);
    })
}

// 时间轴处理
function getLocalTime(nS) {   
    Date.prototype.toLocaleString = function() {
      return this.getFullYear() + "." + (this.getMonth() + 1) + "." + this.getDate();
    };  
   return new Date(parseInt(nS)).toLocaleString();      
} 
$('section').delegate('.aside_center>a','click',function(){
    localStorage.setItem('custOrderUrl',window.location.href)
})
