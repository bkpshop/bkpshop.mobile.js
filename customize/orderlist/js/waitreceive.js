$(function () {
    var pageNum = 0;
    allorderFn(pageNum);
    iScroll();
    operationFn();


    // 订单数据函数
    function allorderFn(pageNum) {
        var orderList = {
            "orderStep": 'WAIT_DELIVERY',
            "page": pageNum,
            "rows": "3"
        };
        post({
            url: '/customizeOrder/list',
            data: orderList,
            success: function (data) {
                var list = data.orderList
                if (data.total == 0) {
                    $('.noOrder').removeClass('tab');
                } else {
                    $('.noOrder').addClass('tab');
                    for (var i = 0; i < list.length; i++) {
                        var listData = list[i];
                        var statusName,orderBtn,countdown;
                        statusFn(listData);
                        orderListFn(listData);
                    };
                    // orderBtnFn();
                };
            }
        });
    };
    // 上拉刷新函数
    function iScroll() {
        function iScrollA() {
            var scrollTop = 0,
                bodyScrollTop = 0,
                documentScrollTop = 0;
            if (document.body) {
                bodyScrollTop = document.body.scrollTop;
            }
            if (document.documentElement) {
                documentScrollTop = document.documentElement.scrollTop;
            }
            scrollTop = (bodyScrollTop - documentScrollTop > 0) ? bodyScrollTop : documentScrollTop;
            return scrollTop;
        }

        function iScrollB() {
            var windowHeight = 0;
            if (document.compatMode == "CSS1Compat") {
                windowHeight = document.documentElement.clientHeight;
            } else {
                windowHeight = document.body.clientHeight;
            }
            return windowHeight;
        }

        function iScrollC() {
            var scrollHeight = 0,
                bodyScrollHeight = 0,
                documentScrollHeight = 0;
            if (document.body) {
                bodyScrollHeight = document.body.scrollHeight;
            }
            if (document.documentElement) {
                documentScrollHeight = document.documentElement.scrollHeight;
            }
            scrollHeight = (bodyScrollHeight - documentScrollHeight > 0) ? bodyScrollHeight : documentScrollHeight;
            return scrollHeight;
        }
        window.onscroll = function () {
            showGoTop($('body').scrollTop())
            if (iScrollC() == iScrollB() + iScrollA()) {
                pageNum += 1;
                allorderFn(pageNum);
                return pageNum;
            }
        }
    };
});