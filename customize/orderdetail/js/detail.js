
var orderId = $.getUrlParam('orderId');
var addressId = '';
var amount = '0.00';
var orderStep = '';
var _totalPrice;
var inviter = '';
$(function(){

    // $('.add_cart').attr('name',orderId);
    post({
        url:'/member/uid',
        success:function(data){
            console.log(data);
            inviter = data
        }
    })
    getDataFn(orderId);

    operationFn();
    //返回
    $('.head-back-2').click(function(){
        go(localStorage.getItem('custOrderUrl'))
    })


    // FUN
    function getDataFn(code){
        post({
            url:'/customizeOrder/detail',
            data:{'code':code},
            success:function(data){
                console.log(data);
                _totalPrice = data.totalPrice;
                orderStep = data.orderStep;
                dealwithDataFn(data);
                chooseBtnFn(data["orderStep"],data.designType);

            },
            error:function(msg){
                console.log(msg)
            }
        })
    };

    function dealwithDataFn(data){
        //产品信息
        $('._productName').html(data["productName"]);
        $('._productUrl').attr('src',SERVER_CONTEXT_PATH+"/load/picture/"+data["productUrl"]);
        $('._manufacturer').text(data["manufacturer"]);
        var arry = data["manufacturerSpecification"].split('  ');
        for(var i=0; i<arry.length; i++){
            $('._manufacturerSpecification').append('<li class="single">'+arry[i]+'</li>')
        }
        $('.go_detail').click(function(){
            go('/customize/newDesign.html?productid='+data.productId+'&inviter='+inviter+'')
        });
        //发票

        if(data.productInvoice){
            $('._invoiceContent').html(data.productInvoice.invoiceContent);
            $('.title_invoice').removeClass('tab');
            $('.title_invoice').html(data.productInvoice.invoiceCategory.text+'：<i>'+data.productInvoice.title+'</i>')
        }else{
            $('._invoiceContent').html('不要发票')
        }
        //地址信息
        $('._consignee').html(data.orderAddress.consignee)
        $('._mobile').html(data.orderAddress.mobile)
        $('._address').html(data.orderAddress.address)

        // 金额
        // $('.totalPrice').html('合计：￥'+returnFloat(data.totalPrice));
        if(data.designType == 'DESIGN'){
            $('.pack-kind').removeClass('tab');
            $('.designType').text('定制');
            $('._productUrl').attr('src',SERVER_CONTEXT_PATH+"/load/picture/"+data["productUrl"])
            $('._designerProductUrl').attr('src',SERVER_CONTEXT_PATH+"/load/picture/"+data["designerProductUrl"])
            $('._designer').html(data.designer);
            $('._packName').html(data.packName);
            $('._designerSpecification').html(data.designerSpecification);
            $('._packImageUrl').attr('src',SERVER_CONTEXT_PATH+"/load/picture/"+data["packImageUrl"])
            if(data.designerList){
                for(var i in data.designerList){
                    if(data.designerList[i].imageUrl){
                        $('._designerList').append('<img src="'+SERVER_CONTEXT_PATH+'/load/picture/'+data.designerList[i].imageUrl+'">')
                    }
                }
            }
            $('.Fee').html(
                '<li class="clear">'
                    +'<span class="left">商品金额：</span>'
                    +'<i class="left">'+data.price+'x'+data.num+'箱</i>'
                    +'<b class="right">￥'+returnFloat((data.price)*(data.num))+'</b>'
                +'</li>'
                +'<li class="clear">'
                    +'<span class="left">设计费：</span>'
                    +'<b class="right">￥'+returnFloat(data.designFee)+'</b>'
                +'</li>'
                +'<li class="clear">'
                    +'<span class="left">制作费：</span>'
                    +'<i class="left">'+data.makeFee+'x'+data.num+'箱</i>'
                   +'<b class="right">￥'+returnFloat((data.makeFee)*(data.num))+'</b>'
                +'</li>'
                +'<li class="clear">'
                    +'<span class="left" style="font-weight:700">合计：</span>'
                   +'<b class="right" style="font-weight:700">￥'+returnFloat(data.totalPrice)+'</b>'
                +'</li>'
            )

        }else{
            $('.designType').text('原厂');
            $('.Fee').html(
                '<li class="clear">'
                    +'<span class="left">商品金额：</span>'
                    +'<i class="left">'+data.price+'x'+data.num+'箱</i>'
                    +'<b class="right">￥'+returnFloat((data.price)*(data.num))+'</b>'
                +'</li>'
                +'<li class="clear">'
                    +'<span class="left" style="font-weight:700">合计：</span>'
                   +'<b class="right" style="font-weight:700">￥'+returnFloat(data.totalPrice)+'</b>'
                +'</li>'
            )
        }
    
       
        

       
        //$(".order_detail .period").text((data.period?data.period:7));
        // $('._productPrice').text(returnFloat(data["productPrice"]));
        // if(data["packPrice"] == null){
        //     $('._packPrice').parent().addClass('tab');
        //     $('._packPriceName').addClass('tab');
        // }else{
        //     $('._packPrice').text(returnFloat(data["packPrice"]));
        // }
        // $('._shippingFee').text(returnFloat(data["shippingFee"]));
        // $('._totalPrice').text(returnFloat(data["totalPrice"]));
        // if(data["advanceMoney"] == null){
        //     $('._advanceMoney').parent().addClass('tab');
        //     $('._advanceMoneyName').addClass('tab');
        // }else{
        //     $('._advanceMoney').text(returnFloat(data["advanceMoney"]));
        // }
    };

    function chooseBtnFn(status,designType){
        var orderText;
        switch (status) {

            case 'WAIT_PAYING':
                if(designType == "DESIGN"){
                    $('.btns').html('<input type="button" class="design_edit" value="设计编辑"><span>|</span><input  class="pay_money" type="button" value="在线支付">')
                }else{
                    $('.btns').html('<input  class="pay_money" type="button" value="在线支付">')
                }
                break;
            case 'WAIT_CONFIRM':
                $('.btns').html('<input type="button" class="design_confirm" name="" value="设计确认"><span>')
                break;
            case 'WAIT_DELIVERY':
                $('.btns').html('<input type="button" class="find_stream" name="" value="查看物流"><span>|</span><input class="confirm_receiving" type="button" name="" value="确认收货">')
                break;
            case 'WAIT_REVIEW':
                $('.btns').html('<a class="after_sale" href="tel:400-825-8521"><input type="button" class="after_sale" name="" value="售后"></a><span>|</span><input class="com" type="button" name="" value="我要评论">')
                break;
            case 'COMPLETE':
                post({
                url:'/customizeProduct/findReviews/',
                data:{
                    code:orderId,
                    type:'manufacturer'
                },
                success:function(obj){
                    console.log(obj)
                    if(obj.isAdditional == true){
                        $('.btns').html('<a class="after_sale" href="tel:400-825-8521"><input type="button" class="after_sale" name="" value="售后"></a><span>|</span><input data-id="'+obj.id+'" class="add_com" type="button" name="" value="我要追评">')
                        
                    }else{
                        $('.btns').html('<a class="after_sale" href="tel:400-825-8521"><input type="button" class="after_sale" name="" value="售后"></a>')
                    }

                },
                error:function(msg){
                    console.log(msg)
                }
            })
                // $('.btns').html('<a class="after_sale" href="tel:400-825-8521"><input type="text" class="after_sale" name="" value="售后"></a><span>|</span><input class="add_com tab" type="button" name="" value="我要追评">')
            case 'CLOSED':
                break;
            default:
                break;
        }
    };
    
    function operationFn(){
       
        //确认收货
        $('.btns').delegate('.confirm_receiving','click',function(){
            $('.gat_alert>p').text('请收到货后，再确认收货！');
            $('.gat_background').removeClass('tab');
            $('.gat_alert').addClass('animated slideInDown');
            $('.gat_alert_cancel').click(function () {
                $('.gat_background').addClass('tab');
            });
            $('.gat_alert_affirm').click(function () {
                post({
                    url:'/customizeOrder/confirmationReceipt',
                    data:{"code":orderId},
                    success:function(data){
                        window.location.reload();
                    }
                });
            });
        });

        //设计确认
        $('.btns').delegate('.design_confirm','click',function(){
            $('.gat_alert>p').text('设计商为您设计的作品您是否满意！');
            $('.gat_background').removeClass('tab');
            $('.gat_alert').addClass('animated slideInDown');
            $('.gat_alert_cancel').click(function () {
                $('.gat_background').addClass('tab');
            });
            $('.gat_alert_affirm').click(function () {
                post({
                    url:'/customizeOrder/designerConfirm',
                    data:{"code":orderId},
                    success:function(data){
                        window.location.reload();
                    }
                })
            });
        });
        //查看物流
        $('.btns').delegate('.find_stream','click',function(){
            go('/customized/customizedorder/logistics/index.html?orderId='+orderId);
        });
     
        //在线支付
        $('.btns').delegate('.pay_money','click',function(){
            setCookie("pay-content","customimprest"); //支付预付款、原厂包装
            setCookie("wait-code",orderId);
            setCookie("wait-amount",_totalPrice);
            setCookie("step", "1");
            setCookie("productdetailgoback",encodeURIComponent(window.location.href));
            go('/mobileorder/selectpaytype.html');
        });
        //在线编辑
        $('.btns').delegate('.design_edit','click',function(){
            go('/customize/gocustomize/designcontain.html?orderId='+orderId);
        })
        //去评论
        $('.btns').delegate('.com','click',function(){
            go('/customized/manufacturerComment/publish.html#'+orderId);
        })
        //去追评
        $('.btns').delegate('.add_com','click',function(){
            var id = $(this).data('id');
            go('/customized/manufacturerComment/addition.html#'+id);
        })


    }
    //已完成追评
    function add_com(){
        
        //制造商
        $('.add_com').each(function(){
            var _this_add_m = $(this);
            var _code_add_m = $(this).data('code');
            post({
                url:'/customizeProduct/findReviews/',
                data:{
                    code:_code_add_m,
                    type:'manufacturer'
                },
                success:function(obj){
                    console.log(obj)
                    if(obj.isAdditional == true){
                        _this_add_m.removeClass('tab');
                        $(_this_add_m).click(function(){
                            go('/customized/manufacturerComment/addition.html#'+obj.id+'')
                        })
                    }

                },
                error:function(msg){
                    console.log(msg)
                }
            })
        })
    }
    //待评价评价
    // function com(){
      
    //  //制造商
    //     $('.com').each(function(){
    //         var _this_m = $(this);
    //         var _code_m = $(this).data('code');
    //         post({
    //             url:'/customizeProduct/findReviews/',
    //             data:{
    //                 code:_code_m,
    //                 type:'manufacturer'
    //             },
    //             success:function(obj){
    //                 // console.log(obj)
    //                 if(obj.isAdditional == null){
    //                     _this_m.removeClass('tab');
    //                     $(_this_m).click(function(){
    //                         go('/customized/manufacturerComment/publish.html#'+_code_m+'')
    //                     })
    //                 }else{
    //                     _this_m.addClass('tab')
    //                 }
                    
    //             },
    //             error:function(msg){
    //                 console.log(msg)
    //             }
    //         })
    //     })
    // }

});